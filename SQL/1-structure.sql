CREATE DATABASE  IF NOT EXISTS `nomads` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `nomads`;
-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: localhost    Database: nomads
-- ------------------------------------------------------
-- Server version	8.0.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `action`
--

DROP TABLE IF EXISTS `action`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `action` (
  `action_id` int NOT NULL AUTO_INCREMENT,
  `action_tournee_id` int NOT NULL,
  `action_debut` datetime NOT NULL,
  `action_fin` datetime NOT NULL,
  `action_date_satisfaction` datetime DEFAULT NULL,
  `action_satisfaction_id` int DEFAULT NULL,
  `action_date_annulation` datetime DEFAULT NULL,
  `action_motif_annulation_action_id` int DEFAULT NULL,
  PRIMARY KEY (`action_id`),
  UNIQUE KEY `tournee_id_debut_fin_UNIQUE` (`action_tournee_id`,`action_debut`,`action_fin`),
  KEY `FK_ACTION_TOURNEE_idx` (`action_tournee_id`),
  KEY `FK_ACTION_MOTIF_ANNULATION_ACTION_idx` (`action_motif_annulation_action_id`),
  KEY `FK_ACTION_SATISFACTION_idx` (`action_satisfaction_id`),
  CONSTRAINT `FK_ACTION_MOTIF_ANNULATION_ACTION` FOREIGN KEY (`action_motif_annulation_action_id`) REFERENCES `motif_annulation_action` (`motif_annulation_action_id`),
  CONSTRAINT `FK_ACTION_SATISFACTION` FOREIGN KEY (`action_satisfaction_id`) REFERENCES `satisfaction` (`satisfaction_id`),
  CONSTRAINT `FK_ACTION_TOURNEE` FOREIGN KEY (`action_tournee_id`) REFERENCES `tournee` (`tournee_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2229 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `affectation_action`
--

DROP TABLE IF EXISTS `affectation_action`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `affectation_action` (
  `affectation_action_id` int NOT NULL AUTO_INCREMENT,
  `affectation_action_action_id` int NOT NULL,
  `affectation_action_masseur_id` int NOT NULL,
  `affectation_action_date_affectation` datetime NOT NULL,
  `affectation_action_gains_euros` float DEFAULT NULL,
  `affectation_action_nombre_massages` int DEFAULT NULL,
  `affectation_action_date_satisfaction` datetime DEFAULT NULL,
  `affectation_action_satisfaction_id` int DEFAULT NULL,
  `affectation_action_date_desistement` datetime DEFAULT NULL,
  `affectation_action_motif_desistement_action_id` int DEFAULT NULL,
  `affectation_action_remboursement_mads` int DEFAULT NULL,
  PRIMARY KEY (`affectation_action_id`),
  KEY `FK_AFFECTATION_ACTION_ACTION_idx` (`affectation_action_action_id`),
  KEY `FK_AFFECTATION_ACTION_MASSEUR_idx` (`affectation_action_masseur_id`),
  KEY `FK_AFFECTATION_ACTION_SATISFACTION_idx` (`affectation_action_satisfaction_id`),
  KEY `FK_AFFECTATION_ACTION_MOTIF_DESISTEMENT_ACTION_idx` (`affectation_action_motif_desistement_action_id`),
  CONSTRAINT `FK_AFFECTATION_ACTION_ACTION` FOREIGN KEY (`affectation_action_action_id`) REFERENCES `action` (`action_id`),
  CONSTRAINT `FK_AFFECTATION_ACTION_MASSEUR` FOREIGN KEY (`affectation_action_masseur_id`) REFERENCES `masseur` (`masseur_id`),
  CONSTRAINT `FK_AFFECTATION_ACTION_MOTIF_DESISTEMENT_ACTION` FOREIGN KEY (`affectation_action_motif_desistement_action_id`) REFERENCES `motif_desistement_action` (`motif_desistement_action_id`),
  CONSTRAINT `FK_AFFECTATION_ACTION_SATISFACTION` FOREIGN KEY (`affectation_action_satisfaction_id`) REFERENCES `satisfaction` (`satisfaction_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `competence`
--

DROP TABLE IF EXISTS `competence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `competence` (
  `competence_id` int NOT NULL AUTO_INCREMENT,
  `competence_type_intervention_id` int NOT NULL,
  `competence_ordre` int NOT NULL,
  `competence_libelle` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`competence_id`),
  UNIQUE KEY `competence_type_intervention_id_libelle_UNIQUE` (`competence_libelle`,`competence_type_intervention_id`),
  KEY `FK_COMPETENCE_TYPE_INTERVENTION_idx` (`competence_type_intervention_id`),
  CONSTRAINT `FK_COMPETENCE_TYPE_INTERVENTION` FOREIGN KEY (`competence_type_intervention_id`) REFERENCES `type_intervention` (`type_intervention_id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cp_ville`
--

DROP TABLE IF EXISTS `cp_ville`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cp_ville` (
  `cp_ville_id` int NOT NULL AUTO_INCREMENT,
  `cp_ville_code_postal` varchar(5) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `cp_ville_ville` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`cp_ville_id`),
  UNIQUE KEY `code_postal_ville_UNIQUE` (`cp_ville_code_postal`,`cp_ville_ville`)
) ENGINE=InnoDB AUTO_INCREMENT=1299 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `depart_masseur`
--

DROP TABLE IF EXISTS `depart_masseur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `depart_masseur` (
  `depart_masseur_id` int NOT NULL AUTO_INCREMENT,
  `depart_masseur_masseur_id` int NOT NULL,
  `depart_masseur_motif_depart_masseur_id` int NOT NULL,
  `depart_masseur_date_depart` datetime NOT NULL,
  `depart_masseur_date_retour` datetime DEFAULT NULL,
  PRIMARY KEY (`depart_masseur_id`),
  KEY `FK_DEPART_MASSEUR_MOTIF_DEPART_MASSEUR_idx` (`depart_masseur_motif_depart_masseur_id`),
  KEY `FK_DEPART_MASSEUR_MASSEUR_idx` (`depart_masseur_masseur_id`),
  CONSTRAINT `FK_DEPART_MASSEUR_MASSEUR` FOREIGN KEY (`depart_masseur_masseur_id`) REFERENCES `masseur` (`masseur_id`),
  CONSTRAINT `FK_DEPART_MASSEUR_MOTIF_DEPART_MASSEUR` FOREIGN KEY (`depart_masseur_motif_depart_masseur_id`) REFERENCES `motif_depart_masseur` (`motif_depart_masseur_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `depart_partenaire`
--

DROP TABLE IF EXISTS `depart_partenaire`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `depart_partenaire` (
  `depart_partenaire_id` int NOT NULL AUTO_INCREMENT,
  `depart_partenaire_partenaire_id` int NOT NULL,
  `depart_partenaire_motif_depart_partenaire_id` int NOT NULL,
  `depart_partenaire_date_depart` datetime NOT NULL,
  `depart_partenaire_date_retour` datetime DEFAULT NULL,
  PRIMARY KEY (`depart_partenaire_id`),
  KEY `FK_DEPART_PARTENAIRE_MOTIF_DEPART_PARTENAIRE_idx` (`depart_partenaire_motif_depart_partenaire_id`),
  KEY `FK_DEPART_PARTENAIRE_PARTENAIRE_idx` (`depart_partenaire_partenaire_id`),
  CONSTRAINT `FK_DEPART_PARTENAIRE_MOTIF_DEPART_PARTENAIRE` FOREIGN KEY (`depart_partenaire_motif_depart_partenaire_id`) REFERENCES `motif_depart_partenaire` (`motif_depart_partenaire_id`),
  CONSTRAINT `FK_DEPART_PARTENAIRE_PARTENAIRE` FOREIGN KEY (`depart_partenaire_partenaire_id`) REFERENCES `partenaire` (`partenaire_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `echelle_remboursement`
--

DROP TABLE IF EXISTS `echelle_remboursement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `echelle_remboursement` (
  `echelle_remboursement_id` int NOT NULL AUTO_INCREMENT,
  `echelle_remboursement_tournee_id` int NOT NULL,
  `echelle_remboursement_heures_restantes` time NOT NULL,
  `echelle_remboursement_pourcentage` int NOT NULL,
  `echelle_remboursement_date_creation` datetime NOT NULL,
  `echelle_remboursement_date_annulation` datetime DEFAULT NULL,
  `echelle_remboursement_motif_annulation_echelle_remboursement_id` int DEFAULT NULL,
  PRIMARY KEY (`echelle_remboursement_id`),
  KEY `FK_ECHELLE_REMBOURSEMENT_TOURNEE_idx` (`echelle_remboursement_tournee_id`),
  KEY `FK_ECHELLE_REMBOURSEMENT_MOTIF_ANNULATION_ECHELLE_REMBOURSE_idx` (`echelle_remboursement_motif_annulation_echelle_remboursement_id`),
  CONSTRAINT `FK_ECHELLE_REMBOURSEMENT_MOTIF_ANNULATION_ECHELLE_REMBOURSEMENT` FOREIGN KEY (`echelle_remboursement_motif_annulation_echelle_remboursement_id`) REFERENCES `motif_annulation_echelle_remboursement` (`motif_annulation_echelle_remboursement_id`),
  CONSTRAINT `FK_ECHELLE_REMBOURSEMENT_TOURNEE` FOREIGN KEY (`echelle_remboursement_tournee_id`) REFERENCES `tournee` (`tournee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `emprunt_materiel`
--

DROP TABLE IF EXISTS `emprunt_materiel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `emprunt_materiel` (
  `emprunt_materiel_id` int NOT NULL AUTO_INCREMENT,
  `emprunt_materiel_materiel_nomads_id` int NOT NULL,
  `emprunt_materiel_inscription_intervention_id` int NOT NULL,
  `emprunt_materiel_date_emprunt` datetime NOT NULL,
  `emprunt_materiel_date_retour` datetime NOT NULL,
  PRIMARY KEY (`emprunt_materiel_id`),
  UNIQUE KEY `materiel_nomads_id_inscription_intervention_id` (`emprunt_materiel_materiel_nomads_id`,`emprunt_materiel_inscription_intervention_id`),
  KEY `FK_EMPRUNT_MATERIEL_INSCRIPTION_INTERVENTION_idx` (`emprunt_materiel_inscription_intervention_id`),
  KEY `FK_EMPRUNT_MATERIEL_MATERIEL_NOMADS_idx` (`emprunt_materiel_materiel_nomads_id`),
  CONSTRAINT `FK_EMPRUNT_MATERIEL_INSCRIPTION_INTERVENTION` FOREIGN KEY (`emprunt_materiel_inscription_intervention_id`) REFERENCES `inscription_intervention` (`inscription_intervention_id`),
  CONSTRAINT `FK_EMPRUNT_MATERIEL_MATERIEL_NOMADS` FOREIGN KEY (`emprunt_materiel_materiel_nomads_id`) REFERENCES `materiel_nomads` (`materiel_nomads_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `etat_materiel`
--

DROP TABLE IF EXISTS `etat_materiel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `etat_materiel` (
  `etat_materiel_id` int NOT NULL AUTO_INCREMENT,
  `etat_materiel_libelle` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`etat_materiel_id`),
  UNIQUE KEY `etat_materiel_libelle_UNIQUE` (`etat_materiel_libelle`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `evolution_masseur`
--

DROP TABLE IF EXISTS `evolution_masseur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `evolution_masseur` (
  `evolution_masseur_id` int NOT NULL AUTO_INCREMENT,
  `evolution_masseur_masseur_id` int NOT NULL,
  `evolution_masseur_competence_id` int NOT NULL,
  `evolution_masseur_date_evolution` datetime NOT NULL,
  PRIMARY KEY (`evolution_masseur_id`),
  UNIQUE KEY `masseur_id_competence_id_UNIQUE` (`evolution_masseur_masseur_id`,`evolution_masseur_competence_id`),
  KEY `FK_EVOLUTION_MASSEUR_MASSEUR_idx` (`evolution_masseur_masseur_id`),
  KEY `FK_EVOLUTION_MASSEUR_COMPETENCE_idx` (`evolution_masseur_competence_id`),
  CONSTRAINT `FK_EVOLUTION_MASSEUR_COMPETENCE` FOREIGN KEY (`evolution_masseur_competence_id`) REFERENCES `competence` (`competence_id`),
  CONSTRAINT `FK_EVOLUTION_MASSEUR_MASSEUR` FOREIGN KEY (`evolution_masseur_masseur_id`) REFERENCES `masseur` (`masseur_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `evolution_materiel`
--

DROP TABLE IF EXISTS `evolution_materiel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `evolution_materiel` (
  `evolution_materiel_id` int NOT NULL AUTO_INCREMENT,
  `evolution_materiel_materiel_nomads_id` int NOT NULL,
  `evolution_materiel_etat_materiel_id` int NOT NULL,
  `evolution_materiel_date_evolution` datetime NOT NULL,
  PRIMARY KEY (`evolution_materiel_id`),
  KEY `FK_EVOLUTION_MATERIEL_MATERIEL_NOMADS_idx` (`evolution_materiel_materiel_nomads_id`),
  KEY `FK_EVOLUTION_MATERIEL_ETAT_MATERIEL_idx` (`evolution_materiel_etat_materiel_id`),
  CONSTRAINT `FK_EVOLUTION_MATERIEL_ETAT_MATERIEL` FOREIGN KEY (`evolution_materiel_etat_materiel_id`) REFERENCES `etat_materiel` (`etat_materiel_id`),
  CONSTRAINT `FK_EVOLUTION_MATERIEL_MATERIEL_NOMADS` FOREIGN KEY (`evolution_materiel_materiel_nomads_id`) REFERENCES `materiel_nomads` (`materiel_nomads_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `evolution_partenaire`
--

DROP TABLE IF EXISTS `evolution_partenaire`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `evolution_partenaire` (
  `evolution_partenaire_id` int NOT NULL AUTO_INCREMENT,
  `evolution_partenaire_partenaire_id` int NOT NULL,
  `evolution_partenaire_type_partenaire_id` int NOT NULL,
  `evolution_partenaire_date_evolution` datetime NOT NULL,
  PRIMARY KEY (`evolution_partenaire_id`),
  KEY `FK_EVOLUTION_PARTENAIRE_PARTENAIRE_idx` (`evolution_partenaire_partenaire_id`),
  KEY `FK_EVOLUTION_PARTENAIRE_TYPE_PARTENAIRE_idx` (`evolution_partenaire_type_partenaire_id`),
  CONSTRAINT `FK_EVOLUTION_PARTENAIRE_PARTENAIRE` FOREIGN KEY (`evolution_partenaire_partenaire_id`) REFERENCES `partenaire` (`partenaire_id`),
  CONSTRAINT `FK_EVOLUTION_PARTENAIRE_TYPE_PARTENAIRE` FOREIGN KEY (`evolution_partenaire_type_partenaire_id`) REFERENCES `type_partenaire` (`type_partenaire_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `formation`
--

DROP TABLE IF EXISTS `formation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `formation` (
  `formation_id` int NOT NULL AUTO_INCREMENT,
  `formation_titre` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `formation_description` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `formation_competence_id` int NOT NULL,
  `formation_duree` time NOT NULL,
  `formation_adresse_postale` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `formation_cp_ville_id` int NOT NULL,
  `formation_min_masseurs` int NOT NULL,
  `formation_max_masseurs` int NOT NULL,
  `formation_date_creation` datetime NOT NULL,
  `formation_date_annulation` datetime DEFAULT NULL,
  `motif_annulation_formation_id` int DEFAULT NULL,
  PRIMARY KEY (`formation_id`),
  KEY `FK_FORMATION_COMPETENCE_idx` (`formation_competence_id`),
  KEY `FK_FORMATION_CP_VILLE_idx` (`formation_cp_ville_id`),
  KEY `FK_MOTIF_ANNULATION_FORMATION_idx` (`motif_annulation_formation_id`),
  CONSTRAINT `FK_FORMATION_COMPETENCE` FOREIGN KEY (`formation_competence_id`) REFERENCES `competence` (`competence_id`),
  CONSTRAINT `FK_FORMATION_CP_VILLE` FOREIGN KEY (`formation_cp_ville_id`) REFERENCES `cp_ville` (`cp_ville_id`),
  CONSTRAINT `FK_MOTIF_ANNULATION_FORMATION` FOREIGN KEY (`motif_annulation_formation_id`) REFERENCES `motif_annulation_formation` (`motif_annulation_formation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `genre`
--

DROP TABLE IF EXISTS `genre`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `genre` (
  `genre_id` int NOT NULL AUTO_INCREMENT,
  `genre_libelle` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `genre_abreviation` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`genre_id`),
  UNIQUE KEY `genre_libelle_UNIQUE` (`genre_libelle`),
  UNIQUE KEY `genre_abreviation_UNIQUE` (`genre_abreviation`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `indisponibilite`
--

DROP TABLE IF EXISTS `indisponibilite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `indisponibilite` (
  `indisponibilite_id` int NOT NULL AUTO_INCREMENT,
  `indisponibilite_utilisateur` tinyint(1) NOT NULL,
  `indisponibilite_masseur_id` int NOT NULL,
  `indisponibilite_motif_indisponibilite_id` int DEFAULT NULL,
  `indisponibilite_debut` datetime NOT NULL,
  `indisponibilite_fin` datetime DEFAULT NULL,
  PRIMARY KEY (`indisponibilite_id`),
  KEY `FK_INDISPONIBILITE_MASSEUR_idx` (`indisponibilite_masseur_id`),
  KEY `FK_INDISPONIBILITE_MOTIF_INDISPONIBILITE_idx` (`indisponibilite_motif_indisponibilite_id`),
  CONSTRAINT `FK_INDISPONIBILITE_MASSEUR` FOREIGN KEY (`indisponibilite_masseur_id`) REFERENCES `masseur` (`masseur_id`),
  CONSTRAINT `FK_INDISPONIBILITE_MOTIF_INDISPONIBILITE` FOREIGN KEY (`indisponibilite_motif_indisponibilite_id`) REFERENCES `motif_indisponibilite` (`motif_indisponibilite_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `inscription_intervention`
--

DROP TABLE IF EXISTS `inscription_intervention`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `inscription_intervention` (
  `inscription_intervention_id` int NOT NULL AUTO_INCREMENT,
  `inscription_intervention_intervention_id` int NOT NULL,
  `inscription_intervention_masseur_id` int NOT NULL,
  `inscription_intervention_date_creation` datetime NOT NULL,
  `inscription_intervention_date_satisfaction` datetime DEFAULT NULL,
  `inscription_intervention_satisfaction_id` int DEFAULT NULL,
  `inscription_intervention_date_desistement` datetime DEFAULT NULL,
  `inscription_intervention_motif_desistement_intervention_id` int DEFAULT NULL,
  PRIMARY KEY (`inscription_intervention_id`),
  UNIQUE KEY `intervention_id_masseur_id_UNIQUE` (`inscription_intervention_intervention_id`,`inscription_intervention_masseur_id`) /*!80000 INVISIBLE */,
  KEY `FK_INSCRIPTION_INTERVENTION_INTERVENTION_idx` (`inscription_intervention_intervention_id`),
  KEY `FK_INSCRIPTION_INTERVENTION_MASSEUR_idx` (`inscription_intervention_masseur_id`),
  KEY `FK_INSCRIPTION_INTERVENTION_MOTIF_DESISTEMENT_INTERVENTION_idx` (`inscription_intervention_motif_desistement_intervention_id`),
  KEY `FK_INSCRIPTION_INTERVENTION_SATISFACTION_idx` (`inscription_intervention_satisfaction_id`),
  CONSTRAINT `FK_INSCRIPTION_INTERVENTION_INTERVENTION` FOREIGN KEY (`inscription_intervention_intervention_id`) REFERENCES `intervention` (`intervention_id`),
  CONSTRAINT `FK_INSCRIPTION_INTERVENTION_MASSEUR` FOREIGN KEY (`inscription_intervention_masseur_id`) REFERENCES `masseur` (`masseur_id`),
  CONSTRAINT `FK_INSCRIPTION_INTERVENTION_MOTIF_DESISTEMENT_INTERVENTION` FOREIGN KEY (`inscription_intervention_motif_desistement_intervention_id`) REFERENCES `motif_desistement_intervention` (`motif_desistement_intervention_id`),
  CONSTRAINT `FK_INSCRIPTION_INTERVENTION_SATISFACTION` FOREIGN KEY (`inscription_intervention_satisfaction_id`) REFERENCES `satisfaction` (`satisfaction_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `inscription_session`
--

DROP TABLE IF EXISTS `inscription_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `inscription_session` (
  `inscription_session_id` int NOT NULL AUTO_INCREMENT,
  `inscription_session_session_formation_id` int NOT NULL,
  `inscription_session_masseur_id` int NOT NULL,
  `inscription_session_date_creation` datetime NOT NULL,
  `inscription_session_formation_validee` tinyint(1) DEFAULT NULL,
  `inscription_session_date_satisfaction` datetime DEFAULT NULL,
  `inscription_session_satisfaction_id` int DEFAULT NULL,
  `inscription_session_date_desistement` datetime DEFAULT NULL,
  `inscription_session_motif_desistement_session_id` int DEFAULT NULL,
  PRIMARY KEY (`inscription_session_id`),
  UNIQUE KEY `formation_id_masseur_id_UNIQUE` (`inscription_session_session_formation_id`,`inscription_session_masseur_id`),
  KEY `FK_INSCRIPTION_SESSION_SESSION_FORMATION_idx` (`inscription_session_session_formation_id`),
  KEY `FK_INSCRIPTION_SESSION_MASSEUR_idx` (`inscription_session_masseur_id`),
  KEY `FK_INSCRIPTION_SESSION_SATISFACTION_idx` (`inscription_session_satisfaction_id`),
  KEY `FK_INSCRIPTION_SESSION_MOTIF_DESISTEMENT_SESSION_idx` (`inscription_session_motif_desistement_session_id`),
  CONSTRAINT `FK_INSCRIPTION_SESSION_MASSEUR` FOREIGN KEY (`inscription_session_masseur_id`) REFERENCES `masseur` (`masseur_id`),
  CONSTRAINT `FK_INSCRIPTION_SESSION_MOTIF_DESISTEMENT_SESSION` FOREIGN KEY (`inscription_session_motif_desistement_session_id`) REFERENCES `motif_desistement_session` (`motif_desistement_session_id`),
  CONSTRAINT `FK_INSCRIPTION_SESSION_SATISFACTION` FOREIGN KEY (`inscription_session_satisfaction_id`) REFERENCES `satisfaction` (`satisfaction_id`),
  CONSTRAINT `FK_INSCRIPTION_SESSION_SESSION_FORMATION` FOREIGN KEY (`inscription_session_session_formation_id`) REFERENCES `session_formation` (`session_formation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `intervention`
--

DROP TABLE IF EXISTS `intervention`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `intervention` (
  `intervention_id` int NOT NULL AUTO_INCREMENT,
  `intervention_partenaire_id` int NOT NULL,
  `intervention_type_intervention_id` int NOT NULL,
  `intervention_nom_lieu` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `intervention_adresse_lieu` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `intervention_cp_ville_id` int NOT NULL,
  `intervention_competence_id` int NOT NULL,
  `intervention_telephone_lieu` varchar(10) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `intervention_mail_lieu` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `intervention_date_creation` datetime NOT NULL,
  `intervention_cout_total_prestation` float DEFAULT NULL,
  `intervention_valide_bool` tinyint(1) DEFAULT NULL,
  `intervention_valide_date` datetime DEFAULT NULL,
  `intervention_date_satisfaction` datetime DEFAULT NULL,
  `intervention_satisfaction_id` int DEFAULT NULL,
  `intervention_date_annulation` datetime DEFAULT NULL,
  `intervention_motif_annulation_intervention_id` int DEFAULT NULL,
  `intervention_date_paiement` datetime DEFAULT NULL,
  `intervention_montant_facture` float DEFAULT NULL,
  PRIMARY KEY (`intervention_id`),
  KEY `INTERVENTION_TYPE_INTERVENTION_idx` (`intervention_type_intervention_id`),
  KEY `FK_INTERVENTION_CP_VILLE_idx` (`intervention_cp_ville_id`),
  KEY `FK_INTERVENTION_SATISFACTION_idx` (`intervention_satisfaction_id`),
  KEY `FK_INTERVENTION_MOTIF_ANNULATION_INTERVENTION_idx` (`intervention_motif_annulation_intervention_id`),
  KEY `FK_INTERVENTION_PARTENAIRE_idx` (`intervention_partenaire_id`),
  KEY `FK_INTERVENTION_COMPETENCE_idx` (`intervention_competence_id`),
  CONSTRAINT `FK_INTERVENTION_COMPETENCE` FOREIGN KEY (`intervention_competence_id`) REFERENCES `competence` (`competence_id`),
  CONSTRAINT `FK_INTERVENTION_CP_VILLE` FOREIGN KEY (`intervention_cp_ville_id`) REFERENCES `cp_ville` (`cp_ville_id`),
  CONSTRAINT `FK_INTERVENTION_MOTIF_ANNULATION_INTERVENTION` FOREIGN KEY (`intervention_motif_annulation_intervention_id`) REFERENCES `motif_annulation_intervention` (`motif_annulation_intervention_id`),
  CONSTRAINT `FK_INTERVENTION_PARTENAIRE` FOREIGN KEY (`intervention_partenaire_id`) REFERENCES `partenaire` (`partenaire_id`),
  CONSTRAINT `FK_INTERVENTION_SATISFACTION` FOREIGN KEY (`intervention_satisfaction_id`) REFERENCES `satisfaction` (`satisfaction_id`),
  CONSTRAINT `FK_INTERVENTION_TYPE_INTERVENTION` FOREIGN KEY (`intervention_type_intervention_id`) REFERENCES `type_intervention` (`type_intervention_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `jour_semaine`
--

DROP TABLE IF EXISTS `jour_semaine`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `jour_semaine` (
  `jour_semaine_id` int NOT NULL AUTO_INCREMENT,
  `jour_semaine_ordre` int NOT NULL,
  `jour_semaine_mysql_value` int NOT NULL,
  `jour_semaine_csharp_value` int NOT NULL,
  `jour_semaine_libelle` varchar(10) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`jour_semaine_id`),
  UNIQUE KEY `jour_semaine_ordre_UNIQUE` (`jour_semaine_ordre`),
  UNIQUE KEY `jour_semaine_mysql_value_UNIQUE` (`jour_semaine_mysql_value`),
  UNIQUE KEY `jour_semaine_csharp_value_UNIQUE` (`jour_semaine_csharp_value`),
  UNIQUE KEY `jour_semaine_libelle_UNIQUE` (`jour_semaine_libelle`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `massage_intervention`
--

DROP TABLE IF EXISTS `massage_intervention`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `massage_intervention` (
  `massage_intervention_id` int NOT NULL AUTO_INCREMENT,
  `massage_intervention_intervention_id` int NOT NULL,
  `massage_intervention_type_massage_id` int NOT NULL,
  `massage_intervention_nombre_masseurs` int NOT NULL,
  PRIMARY KEY (`massage_intervention_id`),
  UNIQUE KEY `intervention_id_type_massage_id_UNIQUE` (`massage_intervention_intervention_id`,`massage_intervention_type_massage_id`) /*!80000 INVISIBLE */,
  KEY `FK_MASSAGE_INTERVENTION_INTERVENTION_idx` (`massage_intervention_intervention_id`),
  KEY `FK_MASSAGE_INTERVENTION_TYPE_MASSAGE_idx` (`massage_intervention_type_massage_id`),
  CONSTRAINT `FK_MASSAGE_INTERVENTION_INTERVENTION` FOREIGN KEY (`massage_intervention_intervention_id`) REFERENCES `intervention` (`intervention_id`),
  CONSTRAINT `FK_MASSAGE_INTERVENTION_TYPE_MASSAGE` FOREIGN KEY (`massage_intervention_type_massage_id`) REFERENCES `type_massage` (`type_massage_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `masseur`
--

DROP TABLE IF EXISTS `masseur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `masseur` (
  `masseur_id` int NOT NULL AUTO_INCREMENT,
  `masseur_nom` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `masseur_prenom` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `masseur_genre_id` int NOT NULL,
  `masseur_telephone` varchar(10) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `masseur_email` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `masseur_adresse_postale` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `masseur_cp_ville_id` int NOT NULL,
  `masseur_date_naissance` date NOT NULL,
  `masseur_distance_max_km` int NOT NULL,
  `masseur_date_creation` datetime NOT NULL,
  PRIMARY KEY (`masseur_id`),
  KEY `FK_MASSEUR_GENRE_idx` (`masseur_genre_id`),
  KEY `FK_MASSEUR_CP_VILLE_idx` (`masseur_cp_ville_id`),
  CONSTRAINT `FK_MASSEUR_CP_VILLE` FOREIGN KEY (`masseur_cp_ville_id`) REFERENCES `cp_ville` (`cp_ville_id`),
  CONSTRAINT `FK_MASSEUR_GENRE` FOREIGN KEY (`masseur_genre_id`) REFERENCES `genre` (`genre_id`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `materiel_intervention`
--

DROP TABLE IF EXISTS `materiel_intervention`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `materiel_intervention` (
  `materiel_intervention_id` int NOT NULL AUTO_INCREMENT,
  `materiel_intervention_intervention_id` int NOT NULL,
  `materiel_intervention_type_materiel_id` int NOT NULL,
  `materiel_intervention_quantite` int NOT NULL,
  PRIMARY KEY (`materiel_intervention_id`),
  UNIQUE KEY `intervention_id_type_materiel_id_UNIQUE` (`materiel_intervention_intervention_id`,`materiel_intervention_type_materiel_id`) /*!80000 INVISIBLE */,
  KEY `FK_MATERIEL_INTERVENTION_INTERVENTION_idx` (`materiel_intervention_intervention_id`),
  KEY `FK_MATERIEL_INTERVENTION_TYPE_MATERIEL_idx` (`materiel_intervention_type_materiel_id`),
  CONSTRAINT `FK_MATERIEL_INTERVENTION_INTERVENTION` FOREIGN KEY (`materiel_intervention_intervention_id`) REFERENCES `intervention` (`intervention_id`),
  CONSTRAINT `FK_MATERIEL_INTERVENTION_TYPE_MATERIEL` FOREIGN KEY (`materiel_intervention_type_materiel_id`) REFERENCES `type_materiel` (`type_materiel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `materiel_massage`
--

DROP TABLE IF EXISTS `materiel_massage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `materiel_massage` (
  `materiel_massage_id` int NOT NULL AUTO_INCREMENT,
  `materiel_massage_type_massage_id` int NOT NULL,
  `materiel_massage_type_materiel_id` int NOT NULL,
  `materiel_massage_quantite` int NOT NULL,
  PRIMARY KEY (`materiel_massage_id`),
  KEY `MATERIEL_MASSAGE_TYPE_MATERIEL_idx` (`materiel_massage_type_materiel_id`),
  KEY `MATERIEL_MASSAGE_TYPE_MASSAGE_idx` (`materiel_massage_type_massage_id`),
  CONSTRAINT `FK_MATERIEL_MASSAGE_TYPE_MASSAGE` FOREIGN KEY (`materiel_massage_type_massage_id`) REFERENCES `type_massage` (`type_massage_id`),
  CONSTRAINT `FK_MATERIEL_MASSAGE_TYPE_MATERIEL` FOREIGN KEY (`materiel_massage_type_materiel_id`) REFERENCES `type_materiel` (`type_materiel_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `materiel_masseur`
--

DROP TABLE IF EXISTS `materiel_masseur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `materiel_masseur` (
  `materiel_masseur_id` int NOT NULL AUTO_INCREMENT,
  `materiel_masseur_type_materiel_id` int NOT NULL,
  `materiel_masseur_masseur_id` int NOT NULL,
  `materiel_masseur_quantite` int NOT NULL,
  PRIMARY KEY (`materiel_masseur_id`),
  UNIQUE KEY `type_materiel_id_masseur_id_UNIQUE` (`materiel_masseur_type_materiel_id`,`materiel_masseur_masseur_id`),
  KEY `FK_MATERIEL_MASSEUR_TYPE_MATERIEL_idx` (`materiel_masseur_type_materiel_id`),
  KEY `FK_MATERIEL_MASSEUR_MASSEUR_idx` (`materiel_masseur_masseur_id`),
  CONSTRAINT `FK_MATERIEL_MASSEUR_MASSEUR` FOREIGN KEY (`materiel_masseur_masseur_id`) REFERENCES `masseur` (`masseur_id`),
  CONSTRAINT `FK_MATERIEL_MASSEUR_TYPE_MATERIEL` FOREIGN KEY (`materiel_masseur_type_materiel_id`) REFERENCES `type_materiel` (`type_materiel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `materiel_nomads`
--

DROP TABLE IF EXISTS `materiel_nomads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `materiel_nomads` (
  `materiel_nomads_id` int NOT NULL AUTO_INCREMENT,
  `materiel_nomads_type_materiel_id` int NOT NULL,
  `materiel_nomads_ajout` datetime NOT NULL,
  PRIMARY KEY (`materiel_nomads_id`),
  KEY `FK_MATERIEL_NOMADS_TYPE_MATERIEL_idx` (`materiel_nomads_type_materiel_id`),
  CONSTRAINT `FK_MATERIEL_NOMADS_TYPE_MATERIEL` FOREIGN KEY (`materiel_nomads_type_materiel_id`) REFERENCES `type_materiel` (`type_materiel_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `motif_annulation_action`
--

DROP TABLE IF EXISTS `motif_annulation_action`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `motif_annulation_action` (
  `motif_annulation_action_id` int NOT NULL AUTO_INCREMENT,
  `motif_annulation_action_libelle` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`motif_annulation_action_id`),
  UNIQUE KEY `motif_annulation_action_libelle_UNIQUE` (`motif_annulation_action_libelle`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `motif_annulation_echelle_remboursement`
--

DROP TABLE IF EXISTS `motif_annulation_echelle_remboursement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `motif_annulation_echelle_remboursement` (
  `motif_annulation_echelle_remboursement_id` int NOT NULL AUTO_INCREMENT,
  `motif_annulation_echelle_remboursement_libelle` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`motif_annulation_echelle_remboursement_id`),
  UNIQUE KEY `motif_annulation_echelle_remboursement_libelle_UNIQUE` (`motif_annulation_echelle_remboursement_libelle`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `motif_annulation_formation`
--

DROP TABLE IF EXISTS `motif_annulation_formation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `motif_annulation_formation` (
  `motif_annulation_formation_id` int NOT NULL AUTO_INCREMENT,
  `motif_annulation_formation_libelle` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`motif_annulation_formation_id`),
  UNIQUE KEY `motif_annulation_formation_libelle_UNIQUE` (`motif_annulation_formation_libelle`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `motif_annulation_intervention`
--

DROP TABLE IF EXISTS `motif_annulation_intervention`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `motif_annulation_intervention` (
  `motif_annulation_intervention_id` int NOT NULL AUTO_INCREMENT,
  `motif_annulation_intervention_libelle` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`motif_annulation_intervention_id`),
  UNIQUE KEY `motif_annulation_intervention_libelle_UNIQUE` (`motif_annulation_intervention_libelle`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `motif_annulation_session`
--

DROP TABLE IF EXISTS `motif_annulation_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `motif_annulation_session` (
  `motif_annulation_session_id` int NOT NULL AUTO_INCREMENT,
  `motif_annulation_session_libelle` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`motif_annulation_session_id`),
  UNIQUE KEY `motif_annulation_session_UNIQUE` (`motif_annulation_session_libelle`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `motif_annulation_tournee`
--

DROP TABLE IF EXISTS `motif_annulation_tournee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `motif_annulation_tournee` (
  `motif_annulation_tournee_id` int NOT NULL AUTO_INCREMENT,
  `motif_annulation_tournee_libelle` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`motif_annulation_tournee_id`),
  UNIQUE KEY `motif_annulation_tournee_libelle_UNIQUE` (`motif_annulation_tournee_libelle`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `motif_depart_masseur`
--

DROP TABLE IF EXISTS `motif_depart_masseur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `motif_depart_masseur` (
  `motif_depart_masseur_id` int NOT NULL AUTO_INCREMENT,
  `motif_depart_masseur_libelle` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`motif_depart_masseur_id`),
  UNIQUE KEY `motif_depart_masseur_libelle_UNIQUE` (`motif_depart_masseur_libelle`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `motif_depart_partenaire`
--

DROP TABLE IF EXISTS `motif_depart_partenaire`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `motif_depart_partenaire` (
  `motif_depart_partenaire_id` int NOT NULL AUTO_INCREMENT,
  `motif_depart_partenaire_libelle` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`motif_depart_partenaire_id`),
  UNIQUE KEY `motif_depart_partenaire_UNIQUE` (`motif_depart_partenaire_libelle`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `motif_desistement_action`
--

DROP TABLE IF EXISTS `motif_desistement_action`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `motif_desistement_action` (
  `motif_desistement_action_id` int NOT NULL AUTO_INCREMENT,
  `motif_desistement_action_libelle` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`motif_desistement_action_id`),
  UNIQUE KEY `motif_desistement_action_libelle_UNIQUE` (`motif_desistement_action_libelle`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `motif_desistement_formateur`
--

DROP TABLE IF EXISTS `motif_desistement_formateur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `motif_desistement_formateur` (
  `motif_desistement_formateur_id` int NOT NULL AUTO_INCREMENT,
  `motif_desistement_formateur_libelle` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`motif_desistement_formateur_id`),
  UNIQUE KEY `motif_desistement_formateur_libelle_UNIQUE` (`motif_desistement_formateur_libelle`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `motif_desistement_intervention`
--

DROP TABLE IF EXISTS `motif_desistement_intervention`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `motif_desistement_intervention` (
  `motif_desistement_intervention_id` int NOT NULL AUTO_INCREMENT,
  `motif_desistement_intervention_libelle` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`motif_desistement_intervention_id`),
  UNIQUE KEY `motif_desistement_intervention_libelle_UNIQUE` (`motif_desistement_intervention_libelle`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `motif_desistement_session`
--

DROP TABLE IF EXISTS `motif_desistement_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `motif_desistement_session` (
  `motif_desistement_session_id` int NOT NULL AUTO_INCREMENT,
  `motif_desistement_session_libelle` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`motif_desistement_session_id`),
  UNIQUE KEY `motif_desistement_session_libelle_UNIQUE` (`motif_desistement_session_libelle`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `motif_indisponibilite`
--

DROP TABLE IF EXISTS `motif_indisponibilite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `motif_indisponibilite` (
  `motif_indisponibilite_id` int NOT NULL AUTO_INCREMENT,
  `motif_indisponibilite_libelle` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`motif_indisponibilite_id`),
  UNIQUE KEY `motif_indisponibilite_libelle_UNIQUE` (`motif_indisponibilite_libelle`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pack_mads`
--

DROP TABLE IF EXISTS `pack_mads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pack_mads` (
  `pack_mads_id` int NOT NULL AUTO_INCREMENT,
  `pack_mads_quantite_mads` int NOT NULL,
  `pack_mads_prix_unitaire` float NOT NULL,
  PRIMARY KEY (`pack_mads_id`),
  UNIQUE KEY `pack_mads_quantite_mads_UNIQUE` (`pack_mads_quantite_mads`),
  UNIQUE KEY `pack_mads_prix_unitaire_UNIQUE` (`pack_mads_prix_unitaire`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `partenaire`
--

DROP TABLE IF EXISTS `partenaire`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `partenaire` (
  `partenaire_id` int NOT NULL AUTO_INCREMENT,
  `partenaire_nom` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `partenaire_telephone` varchar(10) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `partenaire_mail` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `partenaire_adresse_postale` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `partenaire_cp_ville_id` int NOT NULL,
  `partenaire_nom_referent` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `partenaire_prenom_referent` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `partenaire_date_creation` datetime NOT NULL,
  PRIMARY KEY (`partenaire_id`),
  UNIQUE KEY `partenaire_mail_UNIQUE` (`partenaire_mail`),
  KEY `FK_PARTENAIRE_CP_VILLE_idx` (`partenaire_cp_ville_id`),
  CONSTRAINT `FK_PARTENAIRE_CP_VILLE` FOREIGN KEY (`partenaire_cp_ville_id`) REFERENCES `cp_ville` (`cp_ville_id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `preference_masseur`
--

DROP TABLE IF EXISTS `preference_masseur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `preference_masseur` (
  `preference_masseur_id` int NOT NULL AUTO_INCREMENT,
  `preference_masseur_masseur_id` int NOT NULL,
  `preference_masseur_jour_semaine_id` int NOT NULL,
  `preference_masseur_heure_debut` time NOT NULL,
  `preference_masseur_duree` time NOT NULL,
  `preference_masseur_date_creation` datetime NOT NULL,
  `preference_masseur_date_fin` datetime DEFAULT NULL,
  PRIMARY KEY (`preference_masseur_id`),
  KEY `FK_PREFERENCE_MASSEUR_MASSEUR_idx` (`preference_masseur_masseur_id`),
  KEY `FK_PREFERENCE_MASSEUR_JOUR_SEMAINE_idx` (`preference_masseur_jour_semaine_id`),
  CONSTRAINT `FK_PREFERENCE_MASSEUR_JOUR_SEMAINE` FOREIGN KEY (`preference_masseur_jour_semaine_id`) REFERENCES `jour_semaine` (`jour_semaine_id`),
  CONSTRAINT `FK_PREFERENCE_MASSEUR_MASSEUR` FOREIGN KEY (`preference_masseur_masseur_id`) REFERENCES `masseur` (`masseur_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `satisfaction`
--

DROP TABLE IF EXISTS `satisfaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `satisfaction` (
  `satisfaction_id` int NOT NULL AUTO_INCREMENT,
  `satisfaction_ordre` int NOT NULL,
  `satisfaction_libelle` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`satisfaction_id`),
  UNIQUE KEY `satisfaction_libelle_UNIQUE` (`satisfaction_libelle`),
  UNIQUE KEY `satisfaction_ordre_UNIQUE` (`satisfaction_ordre`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `session_formation`
--

DROP TABLE IF EXISTS `session_formation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `session_formation` (
  `session_formation_id` int NOT NULL AUTO_INCREMENT,
  `session_formation_formation_id` int NOT NULL,
  `session_formation_debut` datetime NOT NULL,
  `session_formation_fin` datetime NOT NULL,
  `session_formation_formateur_masseur_id` int DEFAULT NULL,
  `session_formation_affectation_formateur_date` datetime DEFAULT NULL,
  `session_formation_date_desistement_formateur` datetime DEFAULT NULL,
  `session_formation_motif_desistement_formateur_id` int DEFAULT NULL,
  `session_formation_date_annulation` datetime DEFAULT NULL,
  `session_formation_motif_annulation_session_id` int DEFAULT NULL,
  PRIMARY KEY (`session_formation_id`),
  KEY `FK_SESSION_FORMATION_FORMATION_idx` (`session_formation_formation_id`),
  KEY `FK_SESSION_FORMATION_MOTIF_DESISTEMENT_FORMATEUR_idx` (`session_formation_motif_desistement_formateur_id`),
  KEY `FK_SESSION_FORMATION_MOTIF_ANNULATION_SESSION_idx` (`session_formation_motif_annulation_session_id`),
  KEY `FK_SESSION_FORMATION_FORMATEUR_MASSEUR_idx` (`session_formation_formateur_masseur_id`),
  CONSTRAINT `FK_SESSION_FORMATION_FORMATEUR_MASSEUR` FOREIGN KEY (`session_formation_formateur_masseur_id`) REFERENCES `masseur` (`masseur_id`),
  CONSTRAINT `FK_SESSION_FORMATION_FORMATION` FOREIGN KEY (`session_formation_formation_id`) REFERENCES `formation` (`formation_id`),
  CONSTRAINT `FK_SESSION_FORMATION_MOTIF_ANNULATION_SESSION` FOREIGN KEY (`session_formation_motif_annulation_session_id`) REFERENCES `motif_annulation_session` (`motif_annulation_session_id`),
  CONSTRAINT `FK_SESSION_FORMATION_MOTIF_DESISTEMENT_FORMATEUR` FOREIGN KEY (`session_formation_motif_desistement_formateur_id`) REFERENCES `motif_desistement_formateur` (`motif_desistement_formateur_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tournee`
--

DROP TABLE IF EXISTS `tournee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tournee` (
  `tournee_id` int NOT NULL AUTO_INCREMENT,
  `tournee_partenaire_id` int NOT NULL,
  `tournee_nom` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `tournee_jour_semaine_id` int NOT NULL,
  `tournee_type_intervention_id` int NOT NULL,
  `tournee_competence_id` int NOT NULL,
  `tournee_heure_debut` time NOT NULL,
  `tournee_duree` time NOT NULL,
  `tournee_min_binomes` int NOT NULL,
  `tournee_max_binomes` int NOT NULL,
  `tournee_cout_mads` int NOT NULL,
  `tournee_date_fin` datetime DEFAULT NULL,
  `tournee_date_creation` datetime NOT NULL,
  `tournee_date_annulation` datetime DEFAULT NULL,
  `tournee_motif_annulation_tournee_id` int DEFAULT NULL,
  PRIMARY KEY (`tournee_id`),
  UNIQUE KEY `partenaire_id_tournee_nom_UNIQUE` (`tournee_partenaire_id`,`tournee_nom`) /*!80000 INVISIBLE */,
  KEY `FK_TOURNEE_PARTENAIRE_idx` (`tournee_partenaire_id`),
  KEY `FK_TOURNEE_MOTIF_ANNULATION_TOURNEE_idx` (`tournee_motif_annulation_tournee_id`),
  KEY `FK_TOURNEE_JOUR_SEMAINE_idx` (`tournee_jour_semaine_id`),
  KEY `FK_TOURNEE_TYPE_INTERVENTION_idx` (`tournee_type_intervention_id`),
  KEY `FK_TOURNEE_COMPETENCE_idx` (`tournee_competence_id`),
  CONSTRAINT `FK_TOURNEE_COMPETENCE` FOREIGN KEY (`tournee_competence_id`) REFERENCES `competence` (`competence_id`),
  CONSTRAINT `FK_TOURNEE_JOUR_SEMAINE` FOREIGN KEY (`tournee_jour_semaine_id`) REFERENCES `jour_semaine` (`jour_semaine_id`),
  CONSTRAINT `FK_TOURNEE_MOTIF_ANNULATION_TOURNEE` FOREIGN KEY (`tournee_motif_annulation_tournee_id`) REFERENCES `motif_annulation_tournee` (`motif_annulation_tournee_id`),
  CONSTRAINT `FK_TOURNEE_PARTENAIRE` FOREIGN KEY (`tournee_partenaire_id`) REFERENCES `partenaire` (`partenaire_id`),
  CONSTRAINT `FK_TOURNEE_TYPE_INTERVENTION` FOREIGN KEY (`tournee_type_intervention_id`) REFERENCES `type_intervention` (`type_intervention_id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `transaction_pack_mads`
--

DROP TABLE IF EXISTS `transaction_pack_mads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transaction_pack_mads` (
  `transaction_pack_mads_id` int NOT NULL AUTO_INCREMENT,
  `transaction_pack_mads_pack_mads_id` int NOT NULL,
  `transaction_pack_mads_masseur_id` int NOT NULL,
  `transaction_pack_mads_quantite` int NOT NULL,
  `transaction_pack_mads_date_transaction` datetime NOT NULL,
  PRIMARY KEY (`transaction_pack_mads_id`),
  KEY `FK_TRANSACTION_PACK_MADS_PACK_MADS_idx` (`transaction_pack_mads_pack_mads_id`),
  KEY `FK_TRANSACTION_PACK_MADS_MASSEUR_idx` (`transaction_pack_mads_masseur_id`),
  CONSTRAINT `FK_TRANSACTION_PACK_MADS_MASSEUR` FOREIGN KEY (`transaction_pack_mads_masseur_id`) REFERENCES `masseur` (`masseur_id`),
  CONSTRAINT `FK_TRANSACTION_PACK_MADS_PACK_MADS` FOREIGN KEY (`transaction_pack_mads_pack_mads_id`) REFERENCES `pack_mads` (`pack_mads_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `type_intervention`
--

DROP TABLE IF EXISTS `type_intervention`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `type_intervention` (
  `type_intervention_id` int NOT NULL AUTO_INCREMENT,
  `type_intervention_libelle` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`type_intervention_id`),
  UNIQUE KEY `type_intervention_libelle_UNIQUE` (`type_intervention_libelle`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `type_massage`
--

DROP TABLE IF EXISTS `type_massage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `type_massage` (
  `type_massage_id` int NOT NULL AUTO_INCREMENT,
  `type_massage_libelle` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`type_massage_id`),
  UNIQUE KEY `type_massage_libelle_UNIQUE` (`type_massage_libelle`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `type_materiel`
--

DROP TABLE IF EXISTS `type_materiel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `type_materiel` (
  `type_materiel_id` int NOT NULL AUTO_INCREMENT,
  `type_materiel_libelle` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`type_materiel_id`),
  UNIQUE KEY `type_materiel_libelle_UNIQUE` (`type_materiel_libelle`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `type_partenaire`
--

DROP TABLE IF EXISTS `type_partenaire`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `type_partenaire` (
  `type_partenaire_id` int NOT NULL AUTO_INCREMENT,
  `type_partenaire_libelle` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`type_partenaire_id`),
  UNIQUE KEY `type_partenaire_libelle_UNIQUE` (`type_partenaire_libelle`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-03-28 15:42:02
