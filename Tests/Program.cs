﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.DataAccess;

namespace Tests {
    class Program {
        static void Main(string[] args) {

            Console.WriteLine(@"SELECT * FROM " +
                BDD.Tournee.TableName + ", " +
                BDD.Partenaire.TableName + ", " +
                BDD.JourSemaine.TableName + ", " +
                BDD.MotifAnnulationTournee.TableName +
            " WHERE " + BDD.Tournee.Fields.PartenaireId + " = " + BDD.Partenaire.Fields.Id +
            " AND " + BDD.Tournee.Fields.JourSemaineId + " = " + BDD.JourSemaine.Fields.Id +
            " AND (" + BDD.Tournee.Fields.MotifAnnulationTourneeId + " = " + BDD.MotifAnnulationTournee.Fields.Id +
            " OR " + BDD.Tournee.Fields.MotifAnnulationTourneeId + " = NULL)" +
            " ORDER BY " + BDD.Partenaire.Fields.Nom + ";");
            Console.ReadLine();

        }
    }
}
