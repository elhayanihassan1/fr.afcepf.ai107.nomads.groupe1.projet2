﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Presentation
{
    public partial class Accueil : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
          

            lbltxt1.Text = @"
                         Il n'est pas souvent possible de se déplacer 
                         pour aller se faire masser. Cela ne fait rien,
                         ce sont nos masseurs qui se déplacent  et vous
                         proposent un réel moment de détente, là où vous êtes.  
                         Dans nos bars et restaurants partenaires,
                        en entreprise, lors de vos événements, en
                        festival, nous sommes là pour vous apporter
                        un moment de détente magique :)                        
                        Notre massage a spécialement été développé
                        pour être réalisé en position assise, et détendre 
                        un maximum les zones les plus tendues : trapèzes, nuque,
                        épaules et bras.Nous proposons également du Amma Assis,
                        un massage effectué sur chaise ergonomique pour une
                        vraie bulle de détente.
                        Nos massages durent entre 10 et 20 minutes,
                        la pause parfaite au quotidien !";

        

            lbltxt2.Text = @"
                         Nous sommes les experts du massage Bien-être 
                         assis tout-terrain: sur chaise normale,
                         sur un fauteuil, un banc... n'importe !
                         Notre massage a spécialement été développé 
                         pour être réalisé en position assise, et
                         détendre un maximum les zones les plus
                        tendues : trapèzes, nuque, épaules et bras.
                        Nous proposons également du Amma                       
                        Assis, un massage effectué sur chaise
                        ergonomique pour une vraie bulle de détente.
                        Nos massages durent entre 10 et 20 minutes,
                        la pause parfaite au quotidien !";

            Label1.Text = @"
                         Avec votre accès direct et gratuit à
                        notre plateforme, vous pouvez
                        décider en toute liberté des jours
                        et des horaires auxquels vous souhaitez
                        profiter de nos services. Vous gardez
                        une visibilité en continu sur la 
                        venue de nos masseurs!";

            Label2.Text = @"
                            Nous communiquons sur les lieux 
                            et les horaires de nos prestations
                            massage, et sur nos établissements partenaires !
                            Ainsi, nous vous apportons en plus notre
                            clientèle d'habitués friande de massages, 
                            ainsi que ceux qui sont à la recherche 
                            d'une expérience hors du commun";
            Label3.Text = @"
                            Notre prestation est entièrement gratuite
                            pour nos partenaires ! Nous proposons
                            des massages gratuits au staff en bonus :) 
                            C'est à chaque partenaire de décider de 
                            la récurrence de notre venue, nous sommes
                            totalement flexibles et à votre écoute !";
            Label4.Text = @"
                            Les clients qui profitent d'un massage
                            dans votre établissement garderont un 
                            souvenir inoubliable de cette expérience, 
                            et en parleront autour d'eux ! Et même ceux 
                            qui ne souhaitent pas profiter de ce service 
                            ne l'oublieront pas, effet bouche à oreille garanti !";
        }
    }
}