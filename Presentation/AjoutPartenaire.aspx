﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Page.Master" AutoEventWireup="true" CodeBehind="AjoutPartenaire.aspx.cs" Inherits="FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Presentation.AjoutPartenaire" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>




<asp:Content ID="Content2" ContentPlaceHolderID="principal" runat="server">

                <h1>  Ajout Partenaire   </h1>
    <hr />
            <table  class="AjoutPartenaire">
                <tr>
                    <td>
                        <asp:Label ID="lblNom" runat="server" Text=" Nom : "></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtNom" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblTypePartenaire" runat="server" Text=" Type partenaire :"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlTypePartenaire" runat="server">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblTelephone" runat="server" Text="Téléphone : "></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtTelephone" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblEmail" runat="server" Text="Email : "></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtEmail" runat="server" > exemple@hotmail.com   </asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblAdressePostal" runat="server" Text="Adresse postal : "></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtAdressePostal" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblNomReferent" runat="server" Text="Nom référent :"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtNomReferent" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblPrenomReferent" runat="server" Text="Prenom référent  :"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtPrenomReferent" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblCodePostal" runat="server" Text="Code postal :"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlCodePostal"  AutoPostBack="true" runat="server"  OnSelectedIndexChanged="ddlCodePostal_SelectedIndexChanged" >
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblVille" runat="server" Text=" Ville :"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlVille" AutoPostBack="true" runat="server"  OnSelectedIndexChanged="ddlVille_SelectedIndexChanged" >
                        </asp:DropDownList>
                    </td>
                </tr>
            
  
            </table>
          <br />

     
            <asp:Button ID="btnEnregistrer" runat="server" OnClick="btnEnregistrer_Click" Text="Enregistrer" BackColor="#009933" BorderStyle="Outset" BorderWidth="5px" Font-Bold="True" Font-Overline="False" Font-Size="Medium" Font-Strikeout="False" Font-Underline="False" ForeColor="White"  />  
    
      
</asp:Content>
