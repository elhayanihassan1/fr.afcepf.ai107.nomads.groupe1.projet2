﻿using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Business;
using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Presentation {
    public partial class AjoutPartenaire : System.Web.UI.Page {
        CpVilleBU buCp = new CpVilleBU();
        PartenaireBU bu = new PartenaireBU();
        protected void Page_Load(object sender, EventArgs e) {


            List<CpVilleEntity> listCp = new List<CpVilleEntity>();
            DataTable dtCp = buCp.Liste;

            for (int i = 0; i < dtCp.Rows.Count; i++) {
                CpVilleEntity cp = new CpVilleEntity();
                cp.Id = Convert.ToInt32(dtCp.Rows[i]["cp_ville_id"]);
                cp.CodePostal = dtCp.Rows[i]["cp_ville_code_postal"].ToString();
                cp.Ville = dtCp.Rows[i]["cp_ville_ville"].ToString();
                listCp.Add(cp);
            }


            List<TypePartenaireEntity> listTp = new List<TypePartenaireEntity>();
            DataTable dtTp = bu.GetListTypePartenaire();

            for (int i = 0; i < dtTp.Rows.Count; i++) {
                TypePartenaireEntity tp = new TypePartenaireEntity();
                tp.Id = Convert.ToInt32(dtTp.Rows[i]["type_partenaire_id"]);
                tp.Libelle = dtTp.Rows[i]["type_partenaire_libelle"].ToString();
                listTp.Add(tp);
            }

            if (!IsPostBack) {
                listCp.Insert(0, new CpVilleEntity() {
                    Id = -1,
                    CodePostal = "Aucun",
                    Ville = "Aucun"
                });
                ddlCodePostal.DataTextField = "CodePostal";
                ddlCodePostal.DataValueField = "Id";
                ddlCodePostal.DataSource = listCp;
                ddlCodePostal.DataBind();

                ddlVille.DataTextField = "Ville";
                ddlVille.DataValueField = "Id";
                ddlVille.DataSource = listCp;
                ddlVille.DataBind();

                listTp.Insert(0, new TypePartenaireEntity() {
                    Id = -1,
                    Libelle = "Type partenaire"
                });
                ddlTypePartenaire.DataTextField = "Libelle";
                ddlTypePartenaire.DataValueField = "Id";
                ddlTypePartenaire.DataSource = listTp;
                ddlTypePartenaire.DataBind();

            }

        }

        protected void btnEnregistrer_Click(object sender, EventArgs e) {

            #region  (Bars & Restaurants)  et ( Entreprise )

            PartenaireEntity partenaire = new PartenaireEntity();
            partenaire.Nom = txtNom.Text;
            partenaire.Telephone = txtTelephone.Text;
            partenaire.Mail = txtEmail.Text;
            partenaire.AdressePostale = txtAdressePostal.Text;
            partenaire.NomReferent = txtNomReferent.Text;
            partenaire.PrenomReferent = txtPrenomReferent.Text;

            partenaire.DateCreation = DateTime.Now;


            if (ddlCodePostal.SelectedIndex > 0) {
                partenaire.CpVilleId = int.Parse(ddlCodePostal.SelectedValue);
            }

            if (ddlVille.SelectedIndex > 0) {
                partenaire.CpVilleId = int.Parse(ddlVille.SelectedValue);
            }


            bu.AjouterPartenaire(partenaire);

            #endregion


            #region EVOLUTION PARTENAIRE

            EvolutionPartenaireEntity evolution = new EvolutionPartenaireEntity();

            evolution.PartenaireId = partenaire.Id;

            evolution.TypePartenaireId = int.Parse(ddlTypePartenaire.SelectedValue);

            evolution.DateEvolution = DateTime.Now;



            bu.AjouterEvolutionPartenaire(evolution);

            #endregion



            if (evolution.TypePartenaireId == 1) {
                Response.Redirect("BarEtRestaurants.aspx");
            } else Response.Redirect("Entreprise.aspx");
        }

        protected void ddlVille_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlCodePostal.SelectedValue = ddlVille.SelectedValue.ToString();
        }

        protected void ddlCodePostal_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlVille.SelectedValue = ddlCodePostal.SelectedValue.ToString();
        }
    }
}