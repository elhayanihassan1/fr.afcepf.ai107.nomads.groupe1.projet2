﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Page.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Presentation.Home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="principal" runat="server">
    <div _designerregion="0"> <!-- -->
        <table>
            <tr>
                <td class="auto-text">
                    <table>
                        <tr>
                            <td>
                                <h2>NOMAD'S , C'EST QUOI EXACTEMENT ? </h2>
                                <asp:Label ID="lbltxt1" runat="server" Text="Label"></asp:Label>
                                <h2>LE MASSAGE NOMAD'S, NOTRE SPÉCIALITÉ"</h2>
                                <asp:Label ID="lbltxt2" runat="server" Text="Label"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    <img alt="" class="auto-image" src="images/photo_accueil.png" />
                </td>
            </tr>
        </table>
    </div>
    <hr />
    <table class="auto-style2">
        <tr>
            <td>
                <h2>POURQUOI NOMAD'S ?  </h2>
            </td>
        </tr>
    </table>
    <br />
    <table class="grid-container">
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <h3>VOTRE ACCES AU PLANNING </h3>
                            <p>
                                <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
                            </p>
                        </td>
                        <td>
                            <h3>NOTRE COMMUNAUTE </h3>
                            <p>
                                <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label>
                            </p>
                        </td>
                        <td>
                            <h3>LE DEAL TRANQUILLE</h3>
                            <p>
                                <asp:Label ID="Label3" runat="server" Text="Label"></asp:Label>
                            </p>
                        </td>
                        <td>
                            <h3>LE PETIT PLUS  </h3>
                            <p>
                                <asp:Label ID="Label4" runat="server" Text="Label"></asp:Label>
                            </p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
