﻿using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Presentation
{
    public partial class Intervention : System.Web.UI.Page
    {

        private bool partenaireUniquement;
        private int idPartenaire;
        protected void Page_Load(object sender, EventArgs e)
        {
            InterventionBU bu = new InterventionBU();

            partenaireUniquement = int.TryParse(Request["idPartenaire"], out idPartenaire);
            if (!IsPostBack)
            {
                if (partenaireUniquement)
                {
                    gvIntervention.DataSource =bu.InterventionPartenaire(idPartenaire);
                }

                else
                {
                    gvIntervention.DataSource= bu.ListIntervention();
                }
              
                gvIntervention.DataBind();

            }

            }
 
        protected void btnAjouterIntervention_Click(object sender, EventArgs e)
        {
            // redirection vers la page d'ajout
    
            if (partenaireUniquement)
            {
                Response.Redirect("AjoutIntervention.aspx?idPartenaire=" + idPartenaire);
            }
            else
            {
                Response.Redirect("AjoutIntervention.aspx");
            }
        }
    }
}