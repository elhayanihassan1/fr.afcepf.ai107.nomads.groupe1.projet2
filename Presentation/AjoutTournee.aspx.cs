﻿using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Business;
using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities;
using System;
using System.Data;
using System.Web.UI;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Presentation {
    public partial class AjoutTournee : System.Web.UI.Page {

        private bool partenaireUniquement;
        private int idPartenaire;

        protected void Page_Load(object sender, EventArgs e) {

            partenaireUniquement = int.TryParse(Request["idPartenaire"], out idPartenaire);

            if (!IsPostBack) {

                if (partenaireUniquement) {
                    lblTitre.Text = new PartenaireBU().GetById(idPartenaire).Nom + " : ajouter une tournée";
                } else {
                    lblTitre.Text = "Ajouter une tournée";
                }

                DataTable dataTable;
                DataRow dataRow;

                dataTable = new PartenaireBU().GetListPartenaire();
                dataRow = dataTable.NewRow();
                dataRow["partenaire_nom"] = "nom";
                dataTable.Rows.InsertAt(dataRow, 0);

                ddlPartenaire.DataSource = dataTable;
                ddlPartenaire.DataBind();
                ddlPartenaire.SelectedValue = partenaireUniquement ? Convert.ToString(idPartenaire) : "0";
                ddlPartenaire.Enabled = !partenaireUniquement;

                dataTable = new TypeInterventionBU().Liste;
                dataRow = dataTable.NewRow();
                dataRow["type_intervention_libelle"] = "Type";
                dataTable.Rows.InsertAt(dataRow, 0);

                ddlType.DataSource = dataTable;
                ddlType.DataBind();

                dataTable = new JourSemBU().Liste;
                dataRow = dataTable.NewRow();
                dataRow["jour_semaine_libelle"] = "Jour";
                dataTable.Rows.InsertAt(dataRow, 0);

                ddlJourSem.DataSource = dataTable;
                ddlJourSem.DataBind();

                lblPartenaire.Visible = !partenaireUniquement;
                ddlPartenaire.Visible = !partenaireUniquement;

            }
        }

        protected void btnAjouterTournee_Click(object sender, EventArgs e) {
            int.TryParse(ddlPartenaire.SelectedValue, out int partenaire);
            int.TryParse(ddlJourSem.SelectedValue, out int jour);
            int.TryParse(ddlType.SelectedValue, out int type);
            int.TryParse(ddlCompetence.SelectedValue, out int competence);
            int.TryParse(txtDebutHeures.Text, out int debutHeures);
            int.TryParse(txtDebutMinutes.Text, out int debutMinutes);
            int.TryParse(txtxDureeHeure.Text, out int dureeHeures);
            int.TryParse(txtDureeMinutes.Text, out int dureeMinutes);
            int.TryParse(txtBinomesMin.Text, out int binomesMin);
            int.TryParse(txtBinomesMax.Text, out int binomesMax);
            int.TryParse(txtCoutMads.Text, out int coutMads);

            DateTime? dateFin;
            if (cldDateFin.SelectedDate != DateTime.MinValue) {
                dateFin = cldDateFin.SelectedDate;
            } else {
                dateFin = null;
            }

            new TourneeBU().AjouterTournee(new TourneeEntity {
                PartenaireId = partenaire,
                Nom = txtNom.Text,
                JourSemaineId = jour,
                TypeInterventionId = type,
                CompetenceId = competence,
                HeureDebut = new TimeSpan(debutHeures, debutMinutes, 0),
                Duree = new TimeSpan(dureeHeures, dureeMinutes, 0),
                MinBinomes = binomesMin,
                MaxBinomes = binomesMax,
                CoutMads = coutMads,
                DateFin = dateFin,
                DateCreation = DateTime.Now
            });

            Response.Redirect("Tournees.aspx");
        }

        protected void imgDateFinCalendar_Click(object sender, ImageClickEventArgs e) {
            cldDateFin.Visible = !cldDateFin.Visible;
            if (cldDateFin.Visible) {
                txtDateFin.Text = "";
            }
        }

        protected void cldDateFin_SelectionChanged(object sender, EventArgs e) {
            txtDateFin.Text = cldDateFin.SelectedDate.ToShortDateString();
            cldDateFin.Visible = false;
        }

        protected void ddlType_SelectedIndexChanged(object sender, EventArgs e) {
            if (ddlType.SelectedIndex != 0) {
                DataTable dataTable = new CompetenceBU().Liste(Int32.Parse(ddlType.SelectedValue));
                DataRow dataRow = dataTable.NewRow();

                dataRow["competence_libelle"] = "Compétence";
                dataTable.Rows.InsertAt(dataRow, 0);

                ddlCompetence.DataSource = dataTable;
            } else {
                ddlCompetence.SelectedIndex = 0;
                ddlCompetence.DataSource = null;
            }

            lblCompetence.Visible = ddlType.SelectedIndex != 0;
            ddlCompetence.Visible = ddlType.SelectedIndex != 0;

            ddlCompetence.DataBind();
        }

        protected void btnDemo_Click(object sender, EventArgs e) {
            ddlPartenaire.SelectedIndex = 5;
            txtNom.Text = "Toujours du mercredi";
            ddlJourSem.SelectedIndex = 3;
            ddlType.SelectedIndex = 1;
            if (ddlType.SelectedIndex != 0) {
                DataTable dataTable = new CompetenceBU().Liste(Int32.Parse(ddlType.SelectedValue));
                DataRow dataRow = dataTable.NewRow();

                dataRow["competence_libelle"] = "Compétence";
                dataTable.Rows.InsertAt(dataRow, 0);

                ddlCompetence.DataSource = dataTable;
            } else {
                ddlCompetence.SelectedIndex = 0;
                ddlCompetence.DataSource = null;
            }

            lblCompetence.Visible = ddlType.SelectedIndex != 0;
            ddlCompetence.Visible = ddlType.SelectedIndex != 0;

            ddlCompetence.DataBind();
            ddlCompetence.SelectedIndex = 2;
            txtDebutHeures.Text = "18";
            txtDebutMinutes.Text = "30";
            txtxDureeHeure.Text = "3";
            txtDureeMinutes.Text = "30";
            txtBinomesMin.Text = "1";
            txtBinomesMax.Text = "2";
            txtCoutMads.Text = "10";
            txtDateFin.Text = new DateTime(2020, 6, 30).ToShortDateString();
            cldDateFin.SelectedDate = new DateTime(2020, 6, 30);
        }
    }
}
