﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Page.Master" AutoEventWireup="true" CodeBehind="Intervention.aspx.cs" Inherits="FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Presentation.Intervention" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>



<asp:Content ID="Content2" ContentPlaceHolderID="principal" runat="server">


    <h1 aria-atomic="False">Liste des interventions</h1>

    <br />
    <asp:GridView
        ID="gvIntervention"
        CssClass="cssGvTournees"
        runat="server"
        CellPadding="5"
        CellSpacing="10"
        EmptyDataText="Aucune intervention"
        AutoGenerateColumns="false">
        <AlternatingRowStyle BackColor="#CCFFCC" />
        <Columns>
            <asp:BoundField DataField="partenaire_nom" HeaderText="Partenaire" />
            <asp:BoundField DataField="type_intervention_libelle" HeaderText="Type intervention" />
            <asp:BoundField DataField="competence_libelle" HeaderText="Compétence" />
            <asp:BoundField DataField="intervention_telephone_lieu" HeaderText="Téléphone " />
            <asp:BoundField DataField="intervention_cout_total_prestation" HeaderText="Cout prestation (‎€)" />
            <asp:BoundField DataField="intervention_date_paiement" HeaderText="Date paiement" DataFormatString="{0:dd/MM/yyyy}" />
            <asp:BoundField DataField="intervention_montant_facture" HeaderText="Montant de Facture (‎€)" />
            <asp:BoundField DataField="intervention_date_creation" HeaderText="Date de création" DataFormatString="{0:dd/MM/yyyy}" />
            <asp:HyperLinkField
                DataNavigateUrlFormatString="DétailsIntervention.aspx?id={0}"
                DataNavigateUrlFields="intervention_id"
                HeaderText="Détails"
                Text="Voir" />
     
        </Columns>
        <HeaderStyle BackColor="Black" ForeColor="White" />
    </asp:GridView>
    <hr />

    <asp:Button ID="Button1" runat="server" Text="Ajouter" OnClick="btnAjouterIntervention_Click" BackColor="#009933" BorderStyle="Outset" Font-Bold="True" Font-Size="Medium" ForeColor="White" />


</asp:Content>
