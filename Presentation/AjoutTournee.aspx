﻿<%@ Page
    Title="Nouvelle tournée"
    Language="C#"
    MasterPageFile="~/Page.Master"
    AutoEventWireup="true"
    CodeBehind="AjoutTournee.aspx.cs"
    Inherits="FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Presentation.AjoutTournee" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="principal" runat="server">
    <h1>
        <asp:Label ID="lblTitre" runat="server"></asp:Label>
    </h1>
    <table>
        <tr>
            <td>
                <asp:Label ID="lblPartenaire" runat="server" Text="Partenaire : "></asp:Label>
            </td>
            <td>
                <asp:DropDownList
                    ID="ddlPartenaire"
                    runat="server"
                    DataValueField="partenaire_id"
                    DataTextField="partenaire_nom">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>Nom de la tounée : </td>
            <td>
                <asp:TextBox ID="txtNom" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>Jour : </td>
            <td>
                <asp:DropDownList
                    ID="ddlJourSem"
                    runat="server"
                    DataValueField="jour_semaine_id"
                    DataTextField="jour_semaine_libelle">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" Text="Type : "></asp:Label></td>
            <td>
                <asp:DropDownList
                    ID="ddlType"
                    runat="server"
                    DataValueField="type_intervention_id"
                    DataTextField="type_intervention_libelle"
                    OnSelectedIndexChanged="ddlType_SelectedIndexChanged"
                    AutoPostBack="true" >
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblCompetence" runat="server" Visible="false">Compétence</asp:Label>
            <td>
                <asp:DropDownList
                    ID="ddlCompetence"
                    runat="server"
                    DataValueField="competence_id"
                    DataTextField="competence_libelle"
                    Visible="false" >
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>Heure de début : </td>
            <td>
                <asp:TextBox ID="txtDebutHeures" runat="server"></asp:TextBox>
                <asp:Label ID="lblDebutHeures" runat="server" Text="heures"></asp:Label>
                <asp:TextBox ID="txtDebutMinutes" runat="server"></asp:TextBox>
                <asp:Label ID="lblDebutMinutes" runat="server" Text="minutes"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>Durée : </td>
            <td>
                <asp:TextBox ID="txtxDureeHeure" runat="server"></asp:TextBox>
                <asp:Label ID="lblDureeHeures" runat="server" Text="heures"></asp:Label>
                <asp:TextBox ID="txtDureeMinutes" runat="server"></asp:TextBox>
                <asp:Label ID="lblDureeMinutes" runat="server" Text="minutes"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>Binômes (min) : </td>
            <td>
                <asp:TextBox ID="txtBinomesMin" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>Binômes (max) : </td>
            <td>
                <asp:TextBox ID="txtBinomesMax" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>Coût (mads) : </td>
            <td>
                <asp:TextBox ID="txtCoutMads" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" Text="Date de fin : "></asp:Label></td>
            <td>
                <asp:TextBox ID="txtDateFin" runat="server" Enabled="false"></asp:TextBox>
                <asp:ImageButton
                    ID="imgDateFinCalendar"
                    runat="server"
                    ImageUrl="~/images/Calendar.png"
                    Height="20"
                    Width="20"
                    OnClick="imgDateFinCalendar_Click" />
                <asp:Calendar ID="cldDateFin" runat="server" Visible="false" OnSelectionChanged="cldDateFin_SelectionChanged"></asp:Calendar>
            </td>
        </tr>
    </table>
    <br />
    <asp:Button ID="btnAjouterTournee" runat="server" Text="Ajouter" OnClick="btnAjouterTournee_Click" BackColor="#009933" BorderStyle="Outset" Font-Bold="True" Font-Size="Medium" ForeColor="White" />
    <asp:Button ID="btnDemo" runat="server" Text="Démo" OnClick="btnDemo_Click" />
</asp:Content>
