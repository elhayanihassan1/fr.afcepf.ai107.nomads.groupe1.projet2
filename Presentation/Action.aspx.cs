﻿using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Business;
using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Presentation {
    public partial class Action : System.Web.UI.Page {

        private ActionEntity action;
        private TourneeEntity tournee;
        private PartenaireEntity partenaire;

        protected void Page_Load(object sender, EventArgs e) {

            int.TryParse(Request["idAction"], out int idAction);
            action = new TourneeBU().GetAction(idAction);
            tournee = new TourneeBU().GetById(action.TourneeId);
            partenaire = new PartenaireBU().GetById(tournee.PartenaireId);

            if (!IsPostBack) {
                lblPartenaire.Text = "Partenaire : " + partenaire.Nom;
                lblDebut.Text = "Commence le " + action.Debut.ToLongDateString() + " à " + action.Debut.ToShortTimeString();
                lblFin.Text = "Termine le " + action.Fin.ToLongDateString() + " à " + action.Fin.ToShortTimeString();

                gvMasseursDispos.DataSource = new MasseurBU().MasseursDispos(action, tournee, partenaire);
                gvMasseursDispos.DataBind();

                gvMasseurAffectes.DataSource = new MasseurBU().MasseursAffectes(action);
                gvMasseurAffectes.DataBind();
            }

        }

        protected void gvMasseursDispos_RowCommand(object sender, GridViewCommandEventArgs e) {
            if (e.CommandName == "Inscrire") {
                int.TryParse(e.CommandArgument.ToString(), out int idMasseur);
                new MasseurBU().InscrireMasseur(new AffectationActionEntity() {
                    ActionId = action.Id,
                    MasseurId = idMasseur,
                    DateAffectation = DateTime.Now
                });
                new MotifIndisponibiliteBU().AjouterIndisponibilite(action, idMasseur);
                Response.Redirect(Request.RawUrl);
            }
        }

        protected void gvMasseurAffectes_RowCommand(object sender, GridViewCommandEventArgs e) {
            if (e.CommandName == "Désister") {
                int.TryParse(e.CommandArgument.ToString(), out int idMasseur);
                new MasseurBU().DesistementAction(action.Id, idMasseur);
                Response.Redirect(Request.RawUrl);
            }
        }
    }
}