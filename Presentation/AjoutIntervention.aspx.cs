﻿using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Business;
using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Presentation {
    public partial class AjoutIntervention : System.Web.UI.Page {

        private bool partenaireUniquement;
        private int idPartenaire;

        InterventionEntity intervention = new InterventionEntity();
        CpVilleBU buCp = new CpVilleBU();
        PartenaireBU bu = new PartenaireBU();
        InterventionBU buTi = new InterventionBU();
        protected void Page_Load(object sender, EventArgs e) {

            partenaireUniquement = int.TryParse(Request["idPartenaire"], out idPartenaire);
            List<CpVilleEntity> listCp = new List<CpVilleEntity>();
            DataTable dtCp = buCp.Liste;

            for (int i = 0; i < dtCp.Rows.Count; i++) {
                CpVilleEntity cp = new CpVilleEntity();
                cp.Id = Convert.ToInt32(dtCp.Rows[i]["cp_ville_id"]);
                cp.CodePostal = dtCp.Rows[i]["cp_ville_code_postal"].ToString();
                cp.Ville = dtCp.Rows[i]["cp_ville_ville"].ToString();
                listCp.Add(cp);
            }

            List<TypeInterventionEntity> listTi = new List<TypeInterventionEntity>();
            DataTable dtTi = buTi.GetListTypeIntervention();

            for (int i = 0; i < dtTi.Rows.Count; i++) {
                TypeInterventionEntity ti = new TypeInterventionEntity();
                ti.Id = Convert.ToInt32(dtTi.Rows[i]["type_intervention_id"]);
                ti.Libelle = dtTi.Rows[i]["type_intervention_libelle"].ToString();
                listTi.Add(ti);
            }

            List<PartenaireEntity> listP = new List<PartenaireEntity>();
            DataTable dtP = bu.GetListPartenaire();

            for (int i = 0; i < dtP.Rows.Count; i++) {
                PartenaireEntity P = new PartenaireEntity();
                P.Id = Convert.ToInt32(dtP.Rows[i]["partenaire_id"]);
                P.Nom = dtP.Rows[i]["partenaire_nom"].ToString();
                listP.Add(P);
            }


            if (!IsPostBack) {

                listCp.Insert(0, new CpVilleEntity() {
                    Id = -1,
                    CodePostal = "Code postal",
                    Ville = "Ville"
                });
                ddlCodePostal.DataTextField = "CodePostal";
                ddlCodePostal.DataValueField = "Id";

                ddlCodePostal.DataSource = listCp;
                ddlCodePostal.DataBind();


                ddlVille.DataTextField = "Ville";
                ddlVille.DataValueField = "Id";
                ddlVille.DataSource = listCp;
                ddlVille.DataBind();

                listTi.Insert(0, new TypeInterventionEntity() {
                    Id = -1,
                    Libelle = "Type intervention"
                });
                ddlTypeIntervention.DataTextField = "Libelle";
                ddlTypeIntervention.DataValueField = "Id";
                ddlTypeIntervention.DataSource = listTi;
                ddlTypeIntervention.DataBind();

                listP.Insert(0, new PartenaireEntity() {
                    Id = -1,
                    Nom = "Partenaire"
                });
                ddlPartenaire.DataTextField = "Nom";
                ddlPartenaire.DataValueField = "Id";
                ddlPartenaire.DataSource = listP;
                ddlPartenaire.DataBind();
                ddlPartenaire.SelectedValue = partenaireUniquement ? Convert.ToString(idPartenaire) : "0";
                ddlPartenaire.Enabled = !partenaireUniquement;
            }

        }

        protected void btnEnregistrer_Click(object sender, EventArgs e) {

            if (int.Parse(ddlTypeIntervention.SelectedValue) == 1) {
                PartenaireEntity partenaire = bu.GetDetailPartenaire(int.Parse(ddlPartenaire.SelectedValue));
                intervention.NomLieu = partenaire.Nom;
                intervention.TelephoneLieu = partenaire.Telephone;
                intervention.MailLieu = partenaire.Mail;
                intervention.AdresseLieu = partenaire.AdressePostale;
                intervention.CpVilleId = partenaire.CpVilleId;
            } else {
                intervention.NomLieu = txtNomLieu.Text;
                intervention.AdresseLieu = txtAdressePostale.Text;
                intervention.MailLieu = txtMail.Text;
                intervention.TelephoneLieu = txtTelephone.Text;
                if (ddlCodePostal.SelectedIndex > 0) {
                    intervention.CpVilleId = int.Parse(ddlCodePostal.SelectedValue);
                }

                if (ddlVille.SelectedIndex > 0) {
                    intervention.CpVilleId = int.Parse(ddlVille.SelectedValue);
                }
            }

            if (ddlCompetence.SelectedIndex > 0) {
                intervention.CompetenceId = int.Parse(ddlCompetence.SelectedValue);
            }

            float cout;
            float.TryParse(txtCout.Text, out cout);
            intervention.CoutTotalPrestation = cout;
            float facture;
            float.TryParse(txtCout.Text, out facture);
            intervention.MontantFacture = facture;


            if (cldPaiement.SelectedDate != DateTime.MinValue) {
                intervention.DatePaiement = cldPaiement.SelectedDate;
            } else {
                intervention.DatePaiement = null;
            }

            intervention.DateCreation = DateTime.Now;

            if (ddlPartenaire.SelectedIndex > 0) {
                intervention.PartenaireId = int.Parse(ddlPartenaire.SelectedValue);
            }



            if (ddlTypeIntervention.SelectedIndex > 0) {
                intervention.TypeInterventionId = int.Parse(ddlTypeIntervention.SelectedValue);
            }

            if (ButtonValidationOui.Checked) {
                intervention.ValideBool = ButtonValidationOui.Checked;
                intervention.ValideDate = DateTime.Now;

                buTi.AjouterIntervention(intervention);

                Response.Redirect("Intervention.aspx");
            } else {
                lblMessage.Visible = true;
                lblMessage.CssClass += " success";
                lblMessage.Text = "Valider avant d'enregistrer";
            }

        }



        protected void imgPaiementCalendar_Click(object sender, ImageClickEventArgs e) {
            cldPaiement.Visible = !cldPaiement.Visible;
            if (cldPaiement.Visible) {
                txtDatePaiement.Text = "";
            }

        }

        protected void cldPaiement_SelectionChanged(object sender, EventArgs e) {
            txtDatePaiement.Text = cldPaiement.SelectedDate.ToShortDateString();
            cldPaiement.Visible = false;
        }


        protected void ddlCodePostal_SelectedIndexChanged(object sender, EventArgs e) {
            ddlVille.SelectedValue = ddlCodePostal.SelectedValue.ToString();
        }

        protected void ddlVille_SelectedIndexChanged(object sender, EventArgs e) {
            ddlCodePostal.SelectedValue = ddlVille.SelectedValue.ToString();
        }



        protected void ddlTypeIntervention_SelectedIndexChanged(object sender, EventArgs e) {
            if (ddlTypeIntervention.SelectedIndex != 0) {
                DataTable dataTable = new CompetenceBU().Liste(Int32.Parse(ddlTypeIntervention.SelectedValue));
                DataRow dataRow = dataTable.NewRow();

                dataRow["competence_libelle"] = "Compétence";
                dataTable.Rows.InsertAt(dataRow, 0);

                ddlCompetence.DataSource = dataTable;
            } else {
                ddlCompetence.SelectedIndex = 0;
                ddlCompetence.DataSource = null;
            }

            lblCompetence.Visible = ddlTypeIntervention.SelectedIndex != 0;
            ddlCompetence.Visible = ddlTypeIntervention.SelectedIndex != 0;

            ddlCompetence.DataBind();
        }

        protected void btnAnnuler_Click(object sender, EventArgs e) {
            Response.Redirect("Intervention.aspx");
        }

        protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e) {

            lblNomLieu.Visible = RadioButtonList1.SelectedItem.ToString() == "non";
            txtNomLieu.Visible = RadioButtonList1.SelectedItem.ToString() == "non";
            lblAdressePostale.Visible = RadioButtonList1.SelectedItem.ToString() == "non";
            txtAdressePostale.Visible = RadioButtonList1.SelectedItem.ToString() == "non";
            lblCodePostal.Visible = RadioButtonList1.SelectedItem.ToString() == "non";
            ddlCodePostal.Visible = RadioButtonList1.SelectedItem.ToString() == "non";
            lblVille.Visible = RadioButtonList1.SelectedItem.ToString() == "non";
            ddlVille.Visible = RadioButtonList1.SelectedItem.ToString() == "non";
            lblTelephone.Visible = RadioButtonList1.SelectedItem.ToString() == "non";
            txtTelephone.Visible = RadioButtonList1.SelectedItem.ToString() == "non";
            lblMail.Visible = RadioButtonList1.SelectedItem.ToString() == "non";
            txtMail.Visible = RadioButtonList1.SelectedItem.ToString() == "non";

        }
    }
}