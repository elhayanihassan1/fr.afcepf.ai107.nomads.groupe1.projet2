﻿using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Business;
using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Presentation
{
    public partial class Partenaire : System.Web.UI.Page
    {

        private int idPartenaire;
        protected void Page_Load(object sender, EventArgs e)
        {
            // récupération de l'id Partenaire :

            int.TryParse(Request["id"], out idPartenaire);

            if (!IsPostBack)
            {
                DataTable dataTable;
                DataRow dataRow;

                #region INFORMATIONS GENERALES
                PartenaireEntity partenaire = new PartenaireBU().GetDetailPartenaire(idPartenaire);
                CpVilleEntity cpVille = new MasseurBU().GetCpVille(partenaire.CpVilleId);

                lblNomSaisie.Text = partenaire.Nom;
                lblTelephoneSaisie.Text = partenaire.Telephone;
                lblEmailSaisie.Text = partenaire.Mail;
                lblAdressePostalSaisie.Text = partenaire.AdressePostale;
                lblCodePostalSaisie.Text = cpVille.CodePostal;
                lblVilleSaisie.Text = cpVille.Ville;
                lblNomReferentSaisie.Text = partenaire.NomReferent;
                lblPrenomReferentSaisie.Text = partenaire.PrenomReferent;
                lblDateCreationSaisie.Text = partenaire.DateCreation.ToString("yyyy-MM-dd");
              

                #endregion

                #region DEPART PARTENAIRE

                dataTable = new MotifDepartPartenaireBU().Liste;
                dataRow = dataTable.NewRow();
                dataRow["motif_depart_partenaire_libelle"] = "Motif";
                dataTable.Rows.InsertAt(dataRow, 0);

                ddlDepartMotif.DataSource = dataTable;
                ddlDepartMotif.DataBind();

                gvDepartPartenaire.DataSource = new PartenaireBU().GetDepartPartenaire(idPartenaire);
                gvDepartPartenaire.DataBind();
                #endregion
            }




        }

        protected void btnSupprimer_Click(object sender, EventArgs e)
        {
            PartenaireBU bu = new PartenaireBU();

            #region Supprimer la ligne dans la table EVOLUTION PARTENAIRE qui correspondent à ce partenaire

            bu.SupprimerEvolutionPartenaire(this.idPartenaire);

            #endregion


            #region Supprimer un Partenaire

            // demander la suppression au business
        
            bu.SupprimerPartenaire(this.idPartenaire);

            #endregion


            Response.Redirect("BarEtRestaurants.aspx");
        }

        #region Depart Partenaire
        protected void btnEnregistrerDepart_Click(object sender, EventArgs e)
        {
            int.TryParse(ddlDepartMotif.SelectedValue, out int idMotif);

            DateTime? dateRetour;
            if (cldDateRetour.SelectedDate != DateTime.MinValue)
            {
                dateRetour = cldDateRetour.SelectedDate;
            }
            else
            {
                dateRetour = null;
            }

            new PartenaireBU().AjouterDepartPartenaire( new DepartPartenaireEntity
            {
                PartenaireId = idPartenaire,
                MotifDepartPartenaireId = idMotif,
                DateDepart = cldDateDepart.SelectedDate,
                DateRetour = dateRetour
            });

            Response.Redirect(Request.RawUrl);
        }

        #endregion

        protected void btnModifier_Click(object sender, EventArgs e)
        {
  
            Response.Redirect("EditionPartenaire.aspx?id="+idPartenaire);
        }

      
            

        protected void imgDateCalendarDepart_Click(object sender, ImageClickEventArgs e)
        {
            cldDateDepart.Visible = !cldDateDepart.Visible;
            if (cldDateDepart.Visible) {
                txtDateDepart.Text = "";
            }
        }

        protected void cldDateDepart_SelectionChanged(object sender, EventArgs e)
        {
            txtDateDepart.Text = cldDateDepart.SelectedDate.ToShortDateString();
            cldDateDepart.Visible = false;
        }

        protected void imgDateCalendarRetour_Click(object sender, ImageClickEventArgs e)
        {
            cldDateRetour.Visible = !cldDateRetour.Visible;
            if (cldDateRetour.Visible) {
                txtDateRetour.Text = "";
            }
        }

        protected void cldDateRetour_SelectionChanged(object sender, EventArgs e)
        {
            txtDateRetour.Text = cldDateRetour.SelectedDate.ToShortDateString();
            cldDateRetour.Visible = false;
        }

        protected void btnAjouterDepart_Click(object sender, EventArgs e) => switchVisibilityDepartPartenaire();




        protected void btnAnnulerDepart_Click(object sender, EventArgs e) => switchVisibilityDepartPartenaire();


        protected void gvDepartPartenaire_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Supprimer")
            {
                int.TryParse(e.CommandArgument.ToString(), out int idDepart);
                new PartenaireBU().SupprimerDepartPartenaire(idDepart);
                Response.Redirect(Request.RawUrl);
            }
        }

        private void switchVisibilityDepartPartenaire()
        {
            btnAjouterDepart.Visible = !btnAjouterDepart.Visible;
            btnEnregistrerDepart.Visible = !btnEnregistrerDepart.Visible;
            btnAnnulerDepart.Visible = !btnAnnulerDepart.Visible;

            lbldeparMotif.Visible = !lbldeparMotif.Visible;
            ddlDepartMotif.Visible = !ddlDepartMotif.Visible;
            lblDateDepart.Visible = !lblDateDepart.Visible;
            txtDateDepart.Visible = !txtDateDepart.Visible;
            lblDateRetour.Visible = !lblDateRetour.Visible;
            txtDateRetour.Visible = !txtDateRetour.Visible;
            imgDateCalendarDepart.Visible = !imgDateCalendarDepart.Visible;
            imgDateCalendarRetour.Visible = !imgDateCalendarRetour.Visible;
        }
    }
}