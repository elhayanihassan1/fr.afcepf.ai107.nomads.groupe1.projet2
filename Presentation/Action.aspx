﻿<%@ Page
    Title="Action"
    Language="C#"
    MasterPageFile="~/Page.Master"
    AutoEventWireup="true"
    CodeBehind="Action.aspx.cs"
    Inherits="FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Presentation.Action" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style3 {
            float: left;
            width: auto;
            border: 4px solid #000000;
            border-radius: 6px;
            margin: 5px;
            padding: 5px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="principal" runat="server">

    <table class="auto-style3">
        <tr>
            <td>
                <h1>Informations générales</h1>
                <table>
                    <tr>
                        <td>
                            <asp:Label ID="lblPartenaire" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblDebut" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblFin" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                <asp:GridView
                    ID="gvMasseursDispos"
                    CssClass="cssGvMasseursDispos"
                    CellPadding="5"
                    CellSpacing="10"
                    EmptyDataText="Aucun masseur disponible ou qualifié"
                    AutoGenerateColumns="false"
                    OnRowCommand="gvMasseursDispos_RowCommand"
                    runat="server">
                    <AlternatingRowStyle BackColor="#CCFFCC" />
                    <Columns>
                        <asp:BoundField DataField="masseur_nom" HeaderText="Nom" />
                        <asp:BoundField DataField="masseur_prenom" HeaderText="Prénom" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button
                                    ID="btnAffecterMasseur"
                                    runat="server"
                                    CommandName="Inscrire"
                                    CommandArgument='<%# Bind("masseur_id") %>'
                                    BackColor="Green"
                                    ForeColor="White"
                                    Text="Inscrire" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle BackColor="Black" ForeColor="White" />
                </asp:GridView>
            </td>
            <td>
                <asp:GridView
                    ID="gvMasseurAffectes"
                    CssClass="cssGvMasseurAffectes"
                    CellPadding="5"
                    CellSpacing="10"
                    EmptyDataText="Aucun masseur affecté"
                    AutoGenerateColumns="false"
                    OnRowCommand="gvMasseurAffectes_RowCommand"
                    runat="server">
                    <AlternatingRowStyle BackColor="#CCFFCC" />
                    <Columns>
                        <asp:BoundField DataField="masseur_nom" HeaderText="Nom" />
                        <asp:BoundField DataField="masseur_prenom" HeaderText="Prénom" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button
                                    ID="btnDesisterMasseur"
                                    runat="server"
                                    CommandName="Désister"
                                    CommandArgument='<%# Bind("masseur_id") %>'
                                    BackColor="Red"
                                    ForeColor="White"
                                    Text="Désister" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle BackColor="Black" ForeColor="White" />
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>
