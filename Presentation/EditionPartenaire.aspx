﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Page.Master" AutoEventWireup="true" CodeBehind="EditionPartenaire.aspx.cs" Inherits="FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Presentation.EditionPartenaire" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>



<asp:Content ID="Content2" ContentPlaceHolderID="principal" runat="server">

    <h1>Edite Partenaire    
            <a href="BarEtRestaurants.aspx">< retour Liste Partenaires</a> </h1>
    <br />
    <table class="AjoutPartenaire">
        <tr>
            <td>
                <asp:Label ID="lblNom" runat="server" Text=" Nom : "></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtNom" runat="server"></asp:TextBox>
            </td>
        </tr>

        <tr>
            <td>
                <asp:Label ID="lblTelephone" runat="server" Text="Téléphone : "></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtTelephone" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblEmail" runat="server" Text="Email : "></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblAdressePostal" runat="server" Text="Adresse postal : "></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtAdressePostal" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblNomReferent" runat="server" Text="Nom référent :"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtNomReferent" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblPrenomReferent" runat="server" Text="Prenom référent  :"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtPrenomReferent" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblCodePostal" runat="server" Text="Code postal :"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddlCodePostal" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblVille" runat="server" Text=" Ville :"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddlVille" runat="server">
                </asp:DropDownList>
            </td>
        </tr>


    </table>
    <br />

    <asp:Button ID="btnEnregistrer" runat="server" OnClick="btnEnregistrer_Click" Text="Mettre à jour" BackColor="#009933" BorderStyle="Outset" Font-Bold="True" Font-Size="Medium" ForeColor="White" />



</asp:Content>
