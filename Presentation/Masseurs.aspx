﻿<%@ Page
    Title="Liste des masseurs"
    Language="C#"
    MasterPageFile="~/Page.Master"
    AutoEventWireup="true"
    CodeBehind="Masseurs.aspx.cs"
    Inherits="FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Presentation.Masseurs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="principal" runat="server">
    <h1 aria-atomic="False">Liste des masseurs</h1>
    <asp:Button ID="btnAjouterMasseur" runat="server" Text="Ajouter" OnClick="btnAjouterMasseur_Click" BackColor="#009933" BorderStyle="Outset" Font-Bold="True" Font-Size="Medium" ForeColor="White" />
    <br />
    <br />
    <asp:GridView
        ID="gvMasseurs"
        CssClass="cssGvMasseurs"
        runat="server"
        CellPadding="5"
        CellSpacing="10"
        EmptyDataText="Aucun masseur"
        AutoGenerateColumns="false">
        <AlternatingRowStyle BackColor="#CCFFCC" />
        <Columns>
            <asp:BoundField DataField="masseur_nom" HeaderText="Nom" />
            <asp:BoundField DataField="masseur_prenom" HeaderText="Prénom" />
            <asp:BoundField DataField="genre_abreviation" HeaderText="Sexe" />
            <asp:BoundField DataField="masseur_telephone" HeaderText="Téléphone" />
            <asp:BoundField DataField="masseur_email" HeaderText="Email" />
            <asp:BoundField DataField="masseur_date_naissance" HeaderText="Date de naissance" DataFormatString="{0:dd/MM/yyyy}" />
            <asp:BoundField DataField="masseur_distance_max_km" HeaderText="Distance max" />
            <asp:BoundField DataField="masseur_date_creation" HeaderText="Date d'inscription" DataFormatString="{0:dd/MM/yyyy}" />
            <asp:HyperLinkField
                DataNavigateUrlFormatString="Masseur.aspx?idMasseur={0}"
                DataNavigateUrlFields="masseur_id"
                HeaderText="Détails"
                Text="Voir" />
        </Columns>
        <HeaderStyle BackColor="Black" ForeColor="White" />
    </asp:GridView>
</asp:Content>
