﻿using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Business;
using System;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Presentation {
    public partial class Masseurs : System.Web.UI.Page {
        protected void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                gvMasseurs.DataSource = new MasseurBU().Liste();
                gvMasseurs.DataBind();
            }
        }

        protected void btnAjouterMasseur_Click(object sender, EventArgs e) {
            Response.Redirect("AjoutMasseur.aspx");
        }
    }
}