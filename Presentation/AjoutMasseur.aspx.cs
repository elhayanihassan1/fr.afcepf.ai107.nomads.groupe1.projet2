﻿using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Business;
using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities;
using System;
using System.Data;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Presentation {
    public partial class AjoutMasseur : System.Web.UI.Page {
        protected void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {

                DataTable dataTable;
                DataRow dataRow;

                dataTable = new GenreBU().Liste;
                dataRow = dataTable.NewRow();
                dataRow["genre_libelle"] = "Sexe";
                dataTable.Rows.InsertAt(dataRow, 0);

                ddlGenre.DataSource = dataTable;
                ddlGenre.DataBind();

                dataTable = new CpVilleBU().Liste;
                dataRow = dataTable.NewRow();
                dataRow["cp_ville_code_postal"] = "Code postal";
                dataRow["cp_ville_ville"] = "Ville";
                dataTable.Rows.InsertAt(dataRow, 0);

                ddlCodePostal.DataSource = dataTable;
                ddlCodePostal.DataBind();

                ddlVille.DataSource = dataTable;
                ddlVille.DataBind();

                cldDateNaissance.VisibleDate = DateTime.Now.AddYears(-18);
            }
        }

        protected void ddlCodePostal_SelectedIndexChanged(object sender, EventArgs e) {
            ddlVille.SelectedValue = ddlCodePostal.SelectedValue.ToString();
        }

        protected void ddlVille_SelectedIndexChanged(object sender, EventArgs e) {
            ddlCodePostal.SelectedValue = ddlVille.SelectedValue.ToString();

        }

        protected void btnAjouterMasseur_Click(object sender, EventArgs e) {
            int.TryParse(ddlGenre.SelectedValue, out int genre);
            int.TryParse(ddlCodePostal.SelectedValue, out int cpVille);
            int.TryParse(txtDistanceMax.Text, out int distanceMax);

            new MasseurBU().AjouterMasseur(new MasseurEntity {
                Nom = txtNom.Text,
                Prenom = txtPrenom.Text,
                GenreId = genre,
                Telephone = txtTelephone.Text,
                Email = txtEmail.Text,
                AdressePostale = txtAdressePostale.Text,
                CpVilleId = cpVille,
                DateNaissance =cldDateNaissance.SelectedDate,
                DistanceMaxKm = distanceMax,
                DateCreation = DateTime.Now
            });

            Response.Redirect("Masseurs.aspx");
        }

        protected void imgDateNaissanceCalendar_Click(object sender, System.Web.UI.ImageClickEventArgs e) {
            cldDateNaissance.Visible = !cldDateNaissance.Visible;
            if (cldDateNaissance.Visible) {
                txtDateNaissance.Text = "";
            }
        }

        protected void cldDateNaissance_SelectionChanged(object sender, EventArgs e) {
            txtDateNaissance.Text = cldDateNaissance.SelectedDate.ToShortDateString();
            cldDateNaissance.Visible = false;
        }

        protected void btnDemo_Click(object sender, EventArgs e) {
            txtNom.Text = "Doe";
            txtPrenom.Text = "John";
            ddlGenre.SelectedIndex = 1;
            txtTelephone.Text = "##########";
            txtEmail.Text = "john@doe.duh";
            txtAdressePostale.Text = "18, rue des lilas";
            ddlCodePostal.SelectedIndex = 10;
            ddlVille.SelectedIndex = 10;
            txtDateNaissance.Text = new DateTime(1984, 2, 28).ToShortDateString();
            txtDistanceMax.Text = "7";
        }
    }
}