﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Page.Master" AutoEventWireup="true" CodeBehind="AjoutIntervention.aspx.cs" Inherits="FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Presentation.AjoutIntervention" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="principal" runat="server">

    <h1>Ajout Intervention :   </h1>
    <hr />

    <table class="auto-style3">
        <tr>
            <td class>
                <asp:Label ID="lblPartenaire" runat="server" Text="Nom du partenaire : "></asp:Label>
            </td>
            <td class="auto-style3">
                <asp:DropDownList ID="ddlPartenaire" runat="server"
                    DataValueField="partenaire_id"
                    DataTextField="partenaire_nom">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblLieu" runat="server" Text="L'intervention aura lieu dans vos locaux :"></asp:Label>
            </td>
            <td>

                <asp:RadioButtonList ID="RadioButtonList1" OnSelectedIndexChanged="RadioButtonList1_SelectedIndexChanged" runat="server" AutoPostBack="true" RepeatDirection="Horizontal">
                    <asp:ListItem>oui</asp:ListItem>
                    <asp:ListItem>non</asp:ListItem>
                </asp:RadioButtonList>

            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblTypeIntervention" runat="server" Text="Type intervention : "></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddlTypeIntervention" runat="server"
                    DataValueField="type_intervention_id"
                    DataTextField="type_intervention_libelle"
                    OnSelectedIndexChanged="ddlTypeIntervention_SelectedIndexChanged"
                    AutoPostBack="true">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblCompetence" runat="server" Visible="false" Text="Compétence : "></asp:Label>
            </td>
            <td>
                <asp:DropDownList
                    ID="ddlCompetence"
                    runat="server"
                    DataValueField="competence_id"
                    DataTextField="competence_libelle"
                    Visible="false">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblNomLieu" runat="server" Visible="false" Text="Nom de lieu : " Enabled="true"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtNomLieu" Visible="false" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblAdressePostale" Visible="false" runat="server" Text="Adresse postale :"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtAdressePostale" Visible="false" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblCodePostal" Visible="false" runat="server" Text="Code postal :"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddlCodePostal" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlCodePostal_SelectedIndexChanged" Visible="false">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblVille" Visible="false" runat="server" Text="Ville :"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddlVille" Visible="false" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlVille_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="auto-style5">
                <asp:Label ID="lblTelephone" Visible="false" runat="server" Text="Téléphone :"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtTelephone" Visible="false" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="auto-style5">
                <asp:Label ID="lblMail" runat="server" Visible="false" Text="Email :"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtMail" Visible="false" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="auto-style5">
                <asp:Label ID="lblCoutPrestation" runat="server" Text="Coût de la prestation :"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtCout" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="auto-style5">
                <asp:Label ID="lblDatePaiement" runat="server" Text="Date de paiement :"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtDatePaiement" runat="server"></asp:TextBox>

                <asp:ImageButton
                    ID="imgPaiementCalendar"
                    runat="server"
                    ImageUrl="~/images/Calendar.png"
                    Height="20"
                    Width="20"
                    OnClick="imgPaiementCalendar_Click" />
                <asp:Calendar ID="cldPaiement" runat="server" Visible="false" OnSelectionChanged="cldPaiement_SelectionChanged"></asp:Calendar>
            </td>
        </tr>


        <tr>
            <td class="auto-style5">
                <asp:Label ID="lblMontant" runat="server" Text="Montant de la facture :"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtMontantFacture" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblValidation" runat="server" Text="Validation :"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblValidationOui" runat="server" Text="Oui"></asp:Label>
                <asp:RadioButton ID="ButtonValidationOui" runat="server" />
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
    <br />


    <asp:Button ID="btnEnregistrer" runat="server" OnClick="btnEnregistrer_Click" Text="Enregistrer" BackColor="#009933" BorderStyle="Outset" Font-Bold="True" Font-Overline="False" Font-Size="Medium" Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
    <asp:Button ID="btnAnnuler" runat="server" Text="Annuler" OnClick="btnAnnuler_Click" BackColor="Red" BorderStyle="Outset" Font-Bold="True" Font-Size="Medium" ForeColor="White" />

</asp:Content>
