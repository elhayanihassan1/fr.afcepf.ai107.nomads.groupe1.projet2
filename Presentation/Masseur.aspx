﻿<%@ Page
    Title="Masseur"
    Language="C#"
    MaintainScrollPositionOnPostback="true"
    MasterPageFile="~/Page.Master"
    AutoEventWireup="true"
    CodeBehind="Masseur.aspx.cs"
    Inherits="FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Presentation.WebForm1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style3 {
            float: left;
            width: auto;
            border: 4px solid #000000;
            border-radius: 6px;
            margin: 5px;
            padding: 5px;
        }
    </style>
</asp:Content>  

<asp:Content ID="Content2" ContentPlaceHolderID="principal" runat="server">

    <table>
        <tr>
            <td>
                <table class="auto-style3">
                    <tr>
                        <td>
                            <h1>Informations générales</h1>
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label runat="server">Nom : </asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtNom" runat="server" Enabled="false"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label runat="server">Prénom : </asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtPrenom" runat="server" Enabled="false"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label runat="server" Text="Sexe : "></asp:Label></td>
                                    <td>
                                        <asp:DropDownList
                                            ID="ddlGenre"
                                            runat="server"
                                            DataValueField="genre_id"
                                            DataTextField="genre_libelle"
                                            Enabled="false">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label runat="server">Téléphone : </asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtTelephone" runat="server" Enabled="false"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label runat="server">Email : </asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtEmail" runat="server" Enabled="false"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label runat="server">Adresse : </asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtAdresse" runat="server" Enabled="false"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label runat="server">Code postal : </asp:Label></td>
                                    <td>
                                        <asp:DropDownList
                                            ID="ddlCodePostal"
                                            runat="server"
                                            DataValueField="cp_ville_id"
                                            DataTextField="cp_ville_code_postal"
                                            Enabled="false">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label runat="server">Ville : </asp:Label></td>
                                    <td>
                                        <asp:DropDownList
                                            ID="ddlVille"
                                            runat="server"
                                            DataValueField="cp_ville_id"
                                            DataTextField="cp_ville_ville"
                                            Enabled="false">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label runat="server">Date de naissance : </asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtDateNaissance" runat="server" Enabled="false"></asp:TextBox>
                                        <asp:ImageButton
                                            ID="imgMasseurCalendar"
                                            runat="server"
                                            ImageUrl="~/images/Calendar.png"
                                            Height="20"
                                            Width="20"
                                            OnClick="imgMasseurCalendar_Click"
                                            Visible="false" />
                                        <asp:Calendar ID="cldDateNaissance" runat="server" Visible="false" OnSelectionChanged="cldDateNaissance_SelectionChanged"></asp:Calendar>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label runat="server">Distance max (km) : </asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtDistanceMax" runat="server" Enabled="false"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label runat="server">Date d'inscription : </asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtDateCreation" runat="server" Enabled="false"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <asp:Button ID="btnModifierMasseur" runat="server" Text="Modifier" OnClick="btnModifierMasseur_Click" BackColor="#009933" BorderStyle="Outset" Font-Bold="True" Font-Size="Medium" ForeColor="White" />
                            <asp:Button ID="btnSupprimerMasseur" runat="server" Text="Supprimer" OnClick="btnSupprimerMasseur_Click" BackColor="Red" BorderStyle="Outset" Font-Bold="True" Font-Size="Medium" ForeColor="White" />
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                <table class="auto-style3">
                    <tr>
                        <td>
                            <h1>Indisponibilités :</h1>
                            <asp:GridView
                                ID="gvIndisponibilites"
                                CssClass="cssGvIndisponibilites"
                                runat="server"
                                AutoGenerateColumns="false"
                                CellPadding="5"
                                CellSpacing="10"
                                EmptyDataText="Aucune indisponibilité"
                                OnRowCommand="gvIndisponibilites_RowCommand">
                                <Columns>
                                    <asp:BoundField DataField="motif_indisponibilite_libelle" HeaderText="Motif" />
                                    <asp:BoundField DataField="indisponibilite_debut" HeaderText="Premier jour" DataFormatString="{0:dd/MM/yyyy}" />
                                    <asp:BoundField DataField="indisponibilite_fin" HeaderText="Dernier jour" DataFormatString="{0:dd/MM/yyyy}" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Button
                                                ID="btnSupprimerIndisponibilite"
                                                runat="server"
                                                CommandName="Supprimer"
                                                CommandArgument='<%# Bind("indisponibilite_id") %>'
                                                BackColor="Red"
                                                ForeColor="White"
                                                Text="Supprimer" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle BackColor="Black" ForeColor="White" />
                            </asp:GridView>
                            <br />
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblIndispoMotif" runat="server" Visible="false">Motif : </asp:Label></td>
                                    <td>
                                        <asp:DropDownList
                                            ID="ddlIndispoMotif"
                                            runat="server"
                                            DataValueField="motif_indisponibilite_id"
                                            DataTextField="motif_indisponibilite_libelle"
                                            Visible="false">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblIndisponibiliteDebut" runat="server" Visible="false">Commence le : </asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtIndisponibiliteDebut" runat="server" Enabled="false" Visible="false"></asp:TextBox>
                                        <asp:ImageButton
                                            ID="imgIndisponibiliteCalendarDebut"
                                            runat="server"
                                            ImageUrl="~/images/Calendar.png"
                                            Height="20"
                                            Width="20"
                                            OnClick="imgIndisponibiliteCalendarDebut_Click"
                                            Visible="false" />
                                        <asp:Calendar ID="cldIndisponibiliteDebut" runat="server" OnSelectionChanged="cldIndisponibiliteDebut_SelectionChanged" Visible="false"></asp:Calendar>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblIndisponibiliteFin" runat="server" Visible="false">Termine le : </asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtIndisponibiliteFin" runat="server" Enabled="false" Visible="false"></asp:TextBox>
                                        <asp:ImageButton
                                            ID="imgIndisponibiliteCalendarFin"
                                            runat="server"
                                            ImageUrl="~/images/Calendar.png"
                                            Height="20"
                                            Width="20"
                                            OnClick="imgIndisponibiliteCalendarFin_Click"
                                            Visible="false" />
                                        <asp:Calendar ID="cldIndisponibiliteFin" runat="server" OnSelectionChanged="cldIndisponibiliteFin_SelectionChanged" Visible="false"></asp:Calendar>
                                    </td>
                                </tr>
                            </table>
                            <asp:Button ID="btnAjouterIndisponibilite" runat="server" Text="Ajouter" OnClick="btnAjouterIndisponibilite_Click" BackColor="#009900" BorderStyle="Outset" Font-Bold="True" Font-Size="Medium" ForeColor="White" />
                            <asp:Button ID="btnEnregistrerIndisponibilite" runat="server" Text="Enregistrer" Visible="false" OnClick="btnEnregistrerIndisponibilite_Click" BackColor="Blue" BorderStyle="Outset" Font-Bold="True" Font-Size="Medium" ForeColor="White" />
                            <asp:Button ID="btnAnnulerIndisponibilite" runat="server" Text="Annuler" Visible="false" OnClick="btnAnnulerIndisponibilite_Click" BackColor="Red" BorderStyle="Outset" Font-Bold="True" Font-Size="Medium" ForeColor="White" />
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                <table class="auto-style3">
                    <tr>
                        <td>
                            <h1>Mads :</h1>
                            <asp:GridView
                                ID="gvMads"
                                CssClass="cssGvMads"
                                runat="server"
                                AutoGenerateColumns="false"
                                CellPadding="5"
                                CellSpacing="10"
                                EmptyDataText="Aucune transaction">
                                <Columns>
                                    <asp:BoundField DataField="pack_mads_quantite_mads" HeaderText="Mads / pack" />
                                    <asp:BoundField DataField="pack_mads_prix_unitaire" HeaderText="Prix unitaire" DataFormatString="{0:C2}" />
                                    <asp:BoundField DataField="transaction_pack_mads_quantite" HeaderText="Quantité" />
                                    <asp:TemplateField HeaderText="Total mads" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <%# TotalMads((int)Eval("transaction_pack_mads_quantite"), (int)Eval("pack_mads_quantite_mads")) + " mads" %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Coût total" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <%# Math.Round(PrixTotal((int)Eval("transaction_pack_mads_quantite"), (float)Eval("pack_mads_prix_unitaire")), 2) + " €" %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="transaction_pack_mads_date_transaction" HeaderText="Date d'achat" DataFormatString="{0:dd/MM/yyyy}" />
                                </Columns>
                                <HeaderStyle BackColor="Black" ForeColor="White" />
                            </asp:GridView>
                            <br />
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblTotalMadsTransactions" runat="server" Visible="true"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblCoutTotalTransactions" runat="server" Visible="true"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblPack" runat="server" Visible="false">Mads : </asp:Label></td>
                                    <td>
                                        <asp:DropDownList
                                            ID="ddlPack"
                                            runat="server"
                                            DataValueField="pack_mads_id"
                                            DataTextField="pack_mads_quantite_mads"
                                            OnSelectedIndexChanged="ddlPack_SelectedIndexChanged"
                                            AutoPostBack="true"
                                            Visible="false">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblQuantite" runat="server" Visible="false">Quantité : </asp:Label></td>
                                    <td>
                                        <asp:TextBox
                                            ID="txtQuantite"
                                            runat="server"
                                            TextMode="Number"
                                            Min="0"
                                            Max="20"
                                            Visible="false"
                                            OnTextChanged="txtQuantite_TextChanged"
                                            AutoPostBack="true">0</asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblPrixUnitaire" runat="server" Visible="false">Prix unitaire : </asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtPrixUnitaire" runat="server" Visible="false" Enabled="false"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblTotalMadsTransaction" runat="server" Visible="false" Enabled="false">Total mads: </asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtTotalMadsTransaction" runat="server" Visible="false" Enabled="false"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblPrixTotalTransaction" runat="server" Visible="false" Enabled="false">Prix total: </asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtPrixTotalTransaction" runat="server" Visible="false" Enabled="false"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                            <asp:Button ID="btnAjouterTransaction" runat="server" Text="Ajouter" OnClick="btnAjouterTransaction_Click" BackColor="#009900" BorderStyle="Outset" Font-Bold="True" Font-Size="Medium" ForeColor="White" />
                            <asp:Button ID="btnEngistrerTransaction" runat="server" Text="Enregistrer" Visible="false" OnClick="btnEngistrerTransaction_Click" BackColor="Blue" BorderStyle="Outset" Font-Bold="True" Font-Size="Medium" ForeColor="White" />
                            <asp:Button ID="btnAnnulerTransaction" runat="server" Text="Annuler" Visible="false" OnClick="btnAnnulerTransaction_Click" BackColor="Red" BorderStyle="Outset" Font-Bold="True" Font-Size="Medium" ForeColor="White" />
                            <asp:Button ID="BtnDemo" runat="server" Text="Démo" OnClick="BtnDemo_Click" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table class="auto-style3">
                    <tr>
                        <td>
                            <h1>Préférences :</h1>
                            <asp:GridView
                                ID="gvPreferences"
                                CssClass="cssGvPreferences"
                                runat="server"
                                AutoGenerateColumns="false"
                                CellPadding="5"
                                CellSpacing="10"
                                EmptyDataText="Aucune préférence"
                                OnRowCommand="gvPreferences_RowCommand">
                                <Columns>
                                    <asp:BoundField DataField="jour_semaine_libelle" HeaderText="Jour" />
                                    <asp:BoundField DataField="preference_masseur_heure_debut" HeaderText="Heure" HtmlEncode="false" DataFormatString="{0:hh\:mm}" />
                                    <asp:BoundField DataField="preference_masseur_duree" HeaderText="Durée" HtmlEncode="false" DataFormatString="{0:hh\:mm}" />
                                    <asp:BoundField DataField="preference_masseur_date_fin" HeaderText="Jusqu'au" DataFormatString="{0:dd/MM/yyyy}" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Button
                                                ID="btnSupprimerPreference"
                                                runat="server"
                                                CommandName="Supprimer"
                                                CommandArgument='<%# Bind("preference_masseur_id") %>'
                                                BackColor="Red"
                                                ForeColor="White"
                                                Text="Supprimer" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle BackColor="Black" ForeColor="White" />
                            </asp:GridView>
                            <br />
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblPrefJour" runat="server" Visible="false">Jour : </asp:Label></td>
                                    <td>
                                        <asp:DropDownList
                                            ID="ddlPrefJour"
                                            runat="server"
                                            DataValueField="jour_semaine_id"
                                            DataTextField="jour_semaine_libelle"
                                            Visible="false">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblHeureDebut" runat="server" Visible="false">Début : </asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtDebutHeures" runat="server" Visible="false"></asp:TextBox>
                                        <asp:Label ID="lblDebutHeures" runat="server" Visible="false">heures</asp:Label>
                                        <asp:TextBox ID="txtDebutMinutes" runat="server" Visible="false"></asp:TextBox>
                                        <asp:Label ID="lblDebutMinutes" runat="server" Visible="false">minutes</asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblDuree" runat="server" Visible="false">Durée : </asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtDureeHeures" runat="server" Visible="false"></asp:TextBox>
                                        <asp:Label ID="lblDureeHeures" runat="server" Visible="false">heures</asp:Label>
                                        <asp:TextBox ID="txtDureeMinutes" runat="server" Visible="false"></asp:TextBox>
                                        <asp:Label ID="lblDureeMinutes" runat="server" Visible="false">minutes</asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblDateFin" runat="server" Visible="false">Se termine le : </asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtDateFin" runat="server" Enabled="false" Visible="false"></asp:TextBox>
                                        <asp:ImageButton
                                            ID="imgPreferenceCalendar"
                                            runat="server"
                                            ImageUrl="~/images/Calendar.png"
                                            Height="20"
                                            Width="20"
                                            OnClick="imgPreferenceCalendar_Click"
                                            Visible="false" />
                                        <asp:Calendar ID="cldDateFin" runat="server" OnSelectionChanged="cldDateFin_SelectionChanged" Visible="false"></asp:Calendar>
                                    </td>
                                </tr>
                            </table>
                            <asp:Button ID="btnAjouterPreference" runat="server" Text="Ajouter" OnClick="btnAjouterPreference_Click" BackColor="#009933" BorderStyle="Outset" Font-Bold="True" Font-Size="Medium" ForeColor="White" />
                            <asp:Button ID="btnEnregistrer" runat="server" Text="Enregistrer" Visible="false" OnClick="btnEnregistrer_Click" BackColor="Blue" BorderStyle="Outset" Font-Bold="True" Font-Size="Medium" ForeColor="White" />
                            <asp:Button ID="btnAnnuler" runat="server" Text="Annuler" Visible="false" OnClick="btnAnnuler_Click" BackColor="Red" BorderStyle="Outset" Font-Bold="True" Font-Size="Medium" ForeColor="White" />
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                <table class="auto-style3">
                    <tr>
                        <td>
                            <h1>Compétences :</h1>
                            <asp:GridView
                                ID="gvEvolutionMasseur"
                                CssClass="cssGvEvolutionMasseur"
                                runat="server"
                                AutoGenerateColumns="false"
                                CellPadding="5"
                                CellSpacing="10"
                                EmptyDataText="Aucune compétence"
                                OnRowCommand="gvEvolutionMasseur_RowCommand">
                                <Columns>
                                    <asp:BoundField DataField="type_intervention_libelle" HeaderText="Type" />
                                    <asp:BoundField DataField="competence_libelle" HeaderText="Compétence" />
                                    <asp:BoundField DataField="competence_ordre" HeaderText="Niveau" />
                                    <asp:BoundField DataField="evolution_masseur_date_evolution" HeaderText="Date d'obtention" DataFormatString="{0:dd/MM/yyyy}" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Button
                                                ID="btnSupprimerCompetence"
                                                runat="server"
                                                CommandName="Supprimer"
                                                CommandArgument='<%# Bind("evolution_masseur_id") %>'
                                                BackColor="Red"
                                                ForeColor="White"
                                                Text="Supprimer" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle BackColor="Black" ForeColor="White" />
                            </asp:GridView>
                            <br />
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblTypeIntervention" runat="server" Visible="false">Type : </asp:Label></td>
                                    <td>
                                        <asp:DropDownList
                                            ID="ddlTypeIntervention"
                                            runat="server"
                                            DataValueField="type_intervention_id"
                                            DataTextField="type_intervention_libelle"
                                            OnSelectedIndexChanged="ddlTypeIntervention_SelectedIndexChanged"
                                            AutoPostBack="true"
                                            Visible="false">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblCompetence" runat="server" Visible="false">Compétence : </asp:Label></td>
                                    <td>
                                        <asp:DropDownList
                                            ID="ddlCompetence"
                                            runat="server"
                                            DataValueField="competence_id"
                                            DataTextField="competence_libelle"
                                            Visible="false">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                            <asp:Button ID="btnAjouterCompetence" runat="server" Text="Ajouter" OnClick="btnAjouterCompetence_Click" BackColor="#009900" BorderStyle="Outset" Font-Bold="True" Font-Size="Medium" ForeColor="White" />
                            <asp:Button ID="btnEnregistrerCompetence" runat="server" Text="Enregistrer" Visible="false" OnClick="btnEnregistrerCompetence_Click" BackColor="Blue" BorderStyle="Outset" Font-Bold="True" Font-Size="Medium" ForeColor="White" />
                            <asp:Button ID="btnAnnulerCompetence" runat="server" Text="Annuler" Visible="false" OnClick="btnAnnulerCompetence_Click" BackColor="Red" BorderStyle="Outset" Font-Bold="True" Font-Size="Medium" ForeColor="White" />
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                <table class="auto-style3">
                    <tr>
                        <td>
                            <h1>Départ :</h1>
                            <asp:GridView
                                ID="gvDepart"
                                CssClass="cssGvDepart"
                                runat="server"
                                AutoGenerateColumns="false"
                                CellPadding="5"
                                CellSpacing="10"
                                EmptyDataText="Aucun départ"
                                OnRowCommand="gvDepart_RowCommand">
                                <Columns>
                                    <asp:BoundField DataField="motif_depart_masseur_libelle" HeaderText="Jour" />
                                    <asp:BoundField DataField="depart_masseur_date_depart" HeaderText="Heure" DataFormatString="{0:dd/MM/yyyy}" />
                                    <asp:BoundField DataField="depart_masseur_date_retour" HeaderText="Jusqu'au" DataFormatString="{0:dd/MM/yyyy}" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Button
                                                ID="btnSupprimerDepart"
                                                runat="server"
                                                CommandName="Supprimer"
                                                CommandArgument='<%# Bind("depart_masseur_id") %>'
                                                Text="Supprimer" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle BackColor="Black" ForeColor="White" />
                            </asp:GridView>
                            <br />
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblDepartMotif" runat="server" Visible="false">Motif : </asp:Label></td>
                                    <td>
                                        <asp:DropDownList
                                            ID="ddlDepartMasseur"
                                            runat="server"
                                            DataValueField="motif_depart_masseur_id"
                                            DataTextField="motif_depart_masseur_libelle"
                                            Visible="false">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblDateDepart" runat="server" Visible="false">Commence le : </asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtDateDepart" runat="server" Enabled="false" Visible="false"></asp:TextBox>
                                        <asp:ImageButton
                                            ID="imgDateCalendarDepart"
                                            runat="server"
                                            ImageUrl="~/images/Calendar.png"
                                            Height="20"
                                            Width="20"
                                            OnClick="imgDateCalendarDepart_Click"
                                            Visible="false" />
                                        <asp:Calendar ID="cldDateDepart" runat="server" OnSelectionChanged="cldDateDepart_SelectionChanged" Visible="false"></asp:Calendar>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblDateRetour" runat="server" Visible="false">Termine le : </asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtDateRetour" runat="server" Enabled="false" Visible="false"></asp:TextBox>
                                        <asp:ImageButton
                                            ID="imgDateCalendarRetour"
                                            runat="server"
                                            ImageUrl="~/images/Calendar.png"
                                            Height="20"
                                            Width="20"
                                            OnClick="imgDateCalendarRetour_Click"
                                            Visible="false" />
                                        <asp:Calendar ID="cldDateRetour" runat="server" OnSelectionChanged="cldDateRetour_SelectionChanged" Visible="false"></asp:Calendar>
                                    </td>
                                </tr>
                            </table>
                            <asp:Button ID="btnAjouterDepart" runat="server" Text="Ajouter" OnClick="btnAjouterDepart_Click" BackColor="#009900" BorderStyle="Outset" Font-Bold="True" Font-Size="Medium" ForeColor="White" />
                            <asp:Button ID="btnEnregistrerDepart" runat="server" Text="Enregistrer" Visible="false" OnClick="btnEnregistrerDepart_Click" BackColor="Blue" BorderStyle="Outset" Font-Bold="True" Font-Size="Medium" ForeColor="White" />
                            <asp:Button ID="btnAnnulerDepart" runat="server" Text="Annuler" Visible="false" OnClick="btnAnnulerDepart_Click" BackColor="Red" BorderStyle="Outset" Font-Bold="True" Font-Size="Medium" ForeColor="White" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
