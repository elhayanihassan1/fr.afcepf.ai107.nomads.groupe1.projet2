﻿using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Business;
using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Presentation
{
    public partial class DétailsIntervention : System.Web.UI.Page
    {

        private int idIntervention;
        protected void Page_Load(object sender, EventArgs e)
        {
            // récupération de l'id Intervention :

            int.TryParse(Request["id"], out idIntervention);
            if (!IsPostBack)
            {
                DataTable dataTable;
                DataRow dataRow;

                #region Massage Intervention

                dataTable = new TypeMassageBU().Liste;
                dataRow = dataTable.NewRow();
                dataRow["type_massage_libelle"] = "Type massage";
                dataTable.Rows.InsertAt(dataRow, 0);

                ddlTypeMassage.DataSource = dataTable;
                ddlTypeMassage.DataBind();

                gvMassageIntervention.DataSource = new InterventionBU().GetMassageIntervention(idIntervention);
                gvMassageIntervention.DataBind();
                #endregion
            }

        }

        protected void btnEnregistrerMassageIntervention_Click(object sender, EventArgs e)
        {
            int.TryParse(ddlTypeMassage.SelectedValue, out int idTypeMassage);

            int NombreMasseurs;
            int.TryParse(txtNombreMasseurs.Text, out NombreMasseurs);
           
            new InterventionBU().AjouterMassageIntervention(new MassageInterventionEntity
            {
                InterventionId = idIntervention,
                TypeMassageId = idTypeMassage,
                NombreMasseurs = NombreMasseurs
               
            });

            Response.Redirect(Request.RawUrl);
        }

        protected void gvMassageIntervention_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Supprimer")
            {
                int.TryParse(e.CommandArgument.ToString(), out int idMassage);
                new InterventionBU().SupprimerMassageIntervention(idMassage);
                Response.Redirect(Request.RawUrl);
            }
        }

        protected void btnAjouterMassageIntervention_Click(object sender, EventArgs e) => switchVisibilityMassageIntervention();


        protected void btnAnnulerMassageIntervention_Click(object sender, EventArgs e) => switchVisibilityMassageIntervention();


        private void switchVisibilityMassageIntervention()
        {
            btnAjouterMassageIntervention.Visible = !btnAjouterMassageIntervention.Visible;
            btnEnregistrerMassageIntervention.Visible = !btnEnregistrerMassageIntervention.Visible;
            btnAnnulerMassageIntervention.Visible = !btnAnnulerMassageIntervention.Visible;

            lblNombreMasseurs.Visible = !lblNombreMasseurs.Visible;
            txtNombreMasseurs.Visible = !txtNombreMasseurs.Visible;
            ddlTypeMassage.Visible = !ddlTypeMassage.Visible;
            lblTypeMassage.Visible = !lblTypeMassage.Visible;
       
        }

   
    }
}