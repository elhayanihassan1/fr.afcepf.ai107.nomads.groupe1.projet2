﻿<%@ Page
    Title="Nouveau masseur"
    Language="C#"
    MasterPageFile="~/Page.Master"
    AutoEventWireup="true"
    CodeBehind="AjoutMasseur.aspx.cs"
    Inherits="FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Presentation.AjoutMasseur" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="principal" runat="server">
    <table>
        <tr>
            <td>Nom : </td>
            <td>
                <asp:TextBox ID="txtNom" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td>Prénom : </td>
            <td>
                <asp:TextBox ID="txtPrenom" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td>Sexe : </td>
            <td>
                <asp:DropDownList
                    ID="ddlGenre"
                    runat="server"
                    DataValueField="genre_id"
                    DataTextField="genre_libelle">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>Téléphone : </td>
            <td>
                <asp:TextBox ID="txtTelephone" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>Email : </td>
            <td>
                <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td>Adresse : </td>
            <td>
                <asp:TextBox ID="txtAdressePostale" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td>Code postal : </td>
            <td>
                <asp:DropDownList
                    ID="ddlCodePostal"
                    runat="server"
                    DataValueField="cp_ville_id"
                    DataTextField="cp_ville_code_postal"
                    AutoPostBack="true"
                    OnSelectedIndexChanged="ddlCodePostal_SelectedIndexChanged">
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td>Ville : </td>
            <td>
                <asp:DropDownList
                    ID="ddlVille"
                    runat="server"
                    DataValueField="cp_ville_id"
                    DataTextField="cp_ville_ville"
                    AutoPostBack="true"
                    OnSelectedIndexChanged="ddlVille_SelectedIndexChanged">
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" Text="Date de naissance : "></asp:Label></td>
            <td>
                <asp:TextBox ID="txtDateNaissance" runat="server" Enabled="false"></asp:TextBox>
                <asp:ImageButton
                    ID="imgDateNaissanceCalendar"
                    runat="server"
                    ImageUrl="~/images/Calendar.png"
                    Height="20"
                    Width="20"
                    OnClick="imgDateNaissanceCalendar_Click" />
                <asp:Calendar ID="cldDateNaissance" runat="server" Visible="false" OnSelectionChanged="cldDateNaissance_SelectionChanged"></asp:Calendar>
            </td>
        </tr>
        <tr>
            <td>Distance max : </td>
            <td>
                <asp:TextBox ID="txtDistanceMax" runat="server"></asp:TextBox></td>
        </tr>
    </table>
    <br />
    <asp:Button ID="btnAjouterMasseur" runat="server" Text="Ajouter" OnClick="btnAjouterMasseur_Click" BackColor="#009933" BorderStyle="Outset" Font-Bold="True" Font-Size="Medium" ForeColor="White" />
    <asp:Button ID="btnDemo" runat="server" Text="Demo" OnClick="btnDemo_Click" />
</asp:Content>
