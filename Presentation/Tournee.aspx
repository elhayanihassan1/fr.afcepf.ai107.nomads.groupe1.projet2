﻿<%@ Page
    Title="Tournée"
    Language="C#"
    MasterPageFile="~/Page.Master"
    AutoEventWireup="true"
    CodeBehind="Tournee.aspx.cs"
    Inherits="FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Presentation.Tournee" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style3 {
            float: left;
            width: auto;
            border: 4px solid #000000;
            border-radius: 6px;
            margin: 5px;
            padding: 5px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="principal" runat="server">
    <asp:Label ID="lblHidden" runat="server" Visible="false"></asp:Label>
    <table class="auto-style3">
        <tr>
            <td>
                <h1>Informations générales</h1>
                <table>
                    <tr>
                        <td>
                            <asp:Label runat="server" Text="Partenaire : "></asp:Label></td>
                        <td>
                            <asp:DropDownList
                                ID="ddlPartenaire"
                                runat="server"
                                DataValueField="partenaire_id"
                                DataTextField="partenaire_nom"
                                Enabled="false">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblNomTournee" runat="server" Enabled="false">Nom : </asp:Label></td>
                        <td>
                            <asp:TextBox ID="txtNomTournee" runat="server" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server" Text="Jour : "></asp:Label></td>
                        <td>
                            <asp:DropDownList
                                ID="ddlJourSem"
                                runat="server"
                                DataValueField="jour_semaine_id"
                                DataTextField="jour_semaine_libelle"
                                Enabled="false">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server" Text="Type : "></asp:Label></td>
                        <td>
                            <asp:DropDownList
                                ID="ddlType"
                                runat="server"
                                DataValueField="type_intervention_id"
                                DataTextField="type_intervention_libelle"
                                OnSelectedIndexChanged="ddlType_SelectedIndexChanged"
                                Enabled="false">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblCompetence" runat="server">Compétence</asp:Label>
                        <td>
                            <asp:DropDownList
                                ID="ddlCompetence"
                                runat="server"
                                DataValueField="competence_id"
                                DataTextField="competence_libelle"
                                Enabled="false">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblHeureDebut" runat="server" Enabled="false">Début : </asp:Label></td>
                        <td>
                            <asp:TextBox ID="txtDebutHeures" runat="server" Enabled="false"></asp:TextBox>
                            <asp:Label ID="lblDebutHeures" runat="server" Enabled="false">heures</asp:Label>
                            <asp:TextBox ID="txtDebutMinutes" runat="server" Enabled="false"></asp:TextBox>
                            <asp:Label ID="lblDebutMinutes" runat="server" Enabled="false">minutes</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblDuree" runat="server" Enabled="false">Durée : </asp:Label></td>
                        <td>
                            <asp:TextBox ID="txtDureeHeures" runat="server" Enabled="false"></asp:TextBox>
                            <asp:Label ID="lblDureeHeures" runat="server" Enabled="false">heures</asp:Label>
                            <asp:TextBox ID="txtDureeMinutes" runat="server" Enabled="false"></asp:TextBox>
                            <asp:Label ID="lblDureeMinutes" runat="server" Enabled="false">minutes</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server">Binômes(min) : </asp:Label></td>
                        <td>
                            <asp:TextBox ID="txtMinBinomes" runat="server" Enabled="false"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server">Binômes(max) : </asp:Label></td>
                        <td>
                            <asp:TextBox ID="txtMaxBinomes" runat="server" Enabled="false"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server">Coût (mads) : </asp:Label></td>
                        <td>
                            <asp:TextBox ID="txtCoutMads" runat="server" Enabled="false"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server">Date de fin : </asp:Label></td>
                        <td>
                            <asp:TextBox ID="txtTourneeDateFin" runat="server" Enabled="false"></asp:TextBox>
                            <asp:ImageButton
                                ID="imgTourneeDateFin"
                                runat="server"
                                ImageUrl="~/images/Calendar.png"
                                Height="20"
                                Width="20"
                                OnClick="imgTourneeDateFin_Click"
                                Visible="false" />
                            <asp:Calendar ID="cldTourneeDateFin" runat="server" Visible="false" OnSelectionChanged="cldTourneeDateFin_SelectionChanged"></asp:Calendar>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server">Date de création : </asp:Label></td>
                        <td>
                            <asp:TextBox ID="txtTourneeDateCreation" runat="server" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server">Date d'annulation : </asp:Label></td>
                        <td>
                            <asp:TextBox ID="txtTourneeDateAnnulation" runat="server" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server" Text="Motif annulation : "></asp:Label></td>
                        <td>
                            <asp:DropDownList
                                ID="ddlTourneeMotifAnnulation"
                                runat="server"
                                DataValueField="motif_annulation_tournee_id"
                                DataTextField="motif_annulation_tournee_libelle"
                                Enabled="false">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
                <asp:Button ID="btnModifierTournee" runat="server" Text="Modifier" OnClick="btnModifierTournee_Click" BackColor="#009933" BorderStyle="Outset" Font-Bold="True" Font-Size="Medium" ForeColor="White" />
                <asp:Button ID="btnSupprimerTournee" runat="server" Text="Supprimer" OnClick="btnSupprimerTournee_Click" BackColor="Red" BorderStyle="Outset" Font-Bold="True" Font-Size="Medium" ForeColor="White" />
            </td>
        </tr>
    </table>
    <table class="auto-style3">
        <tr>
            <td>
                <h1>Actions</h1>
                <table>
                    <tr>
                        <td>
                            <asp:Button
                                ID="btnAjouterAction"
                                runat="server"
                                Text="Ajouter action"
                                OnClick="btnAjouterAction_Click"
                                BackColor="#009933"
                                BorderStyle="Outset"
                                Font-Bold="True"
                                Font-Size="Medium"
                                ForeColor="White" />
                        </td>
                        <td>
                            <asp:CheckBox
                                ID="chkShowFinished"
                                runat="server"
                                OnCheckedChanged="chkShowFinished_CheckedChanged"
                                Checked="false"
                                AutoPostBack="true"
                                Text="Afficher les actions achevées" />
                        </td>
                    </tr>
                </table>
                <br />
                <asp:GridView
                    ID="gvActions"
                    CssClass="cssGvActions"
                    runat="server"
                    AutoGenerateColumns="false"
                    CellPadding="5"
                    CellSpacing="10"
                    EmptyDataText="Aucune action"
                    OnRowCommand="gvActions_RowCommand">
                    <AlternatingRowStyle BackColor="#CCFFCC" />
                    <Columns>
                        <asp:BoundField DataField="tournee_nom" HeaderText="Tournée" />
                        <asp:BoundField DataField="action_debut" HeaderText="Débute le" HtmlEncode="false" DataFormatString="{0:dd/MM/yyyy}" />
                        <asp:BoundField DataField="action_debut" HeaderText="à" HtmlEncode="false" DataFormatString="{0:HH:mm}" />
                        <asp:BoundField DataField="action_fin" HeaderText="Termine le" HtmlEncode="false" DataFormatString="{0:dd/MM/yyyy}" />
                        <asp:BoundField DataField="action_fin" HeaderText="à" HtmlEncode="false" DataFormatString="{0:HH:mm}" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button
                                    ID="btnAffecterMasseurs"
                                    runat="server"
                                    CommandName="Affecter"
                                    CommandArgument='<%# Bind("action_id") %>'
                                    BackColor="Green"
                                    ForeColor="White"
                                    Text="Affectations" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button
                                    ID="btnAnnulerAction"
                                    runat="server"
                                    CommandName="Supprimer"
                                    CommandArgument='<%# Bind("action_id") %>'
                                    BackColor="Red"
                                    ForeColor="White"
                                    Text="Annuler" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle BackColor="Black" ForeColor="White" />
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>
