﻿using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Business;
using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Presentation
{
    public partial class Entreprise : System.Web.UI.Page
    {
      
        PartenaireBU bu = new PartenaireBU();
        protected void Page_Load(object sender, EventArgs e)
        {
          

        #region  Rechercher un Partenaire

 
                if (!IsPostBack)
                {


                  
                  gvEntreprise.DataSource = bu.GetListEntreprise();
                  gvEntreprise.DataBind();
                }

         #endregion

        }


        protected void btnAjouter_Click(object sender, EventArgs e)
        {
            // redirection vers la page d'ajout
            Response.Redirect("AjoutPartenaire.aspx");
        }

    

        protected void btnRechercher_Click(object sender, EventArgs e)
        {
            string nom = txtNom.Text;
            string NomRreferent = txtNomReferent.Text;
            string PrenomReferent = txtPrenomReferent.Text;

       

            gvEntreprise.DataSource = bu.RecherchePartenaireE(nom, NomRreferent, PrenomReferent);
            gvEntreprise.DataBind();
        }
    }
}