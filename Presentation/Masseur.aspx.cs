﻿using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Business;
using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities;
using System;
using System.Data;
using System.Web.UI.WebControls;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Presentation {
    public partial class WebForm1 : System.Web.UI.Page {

        private int idMasseur;

        protected void Page_Load(object sender, EventArgs e) {

            if (!int.TryParse(Request["idMasseur"], out idMasseur)) {
                Response.Redirect("404.aspx");
            } else if (!new MasseurBU().MasseurExiste(idMasseur)) {
                Response.Redirect("404.aspx");
            }

            if (!IsPostBack) {

                //Récupération de l'entité masseur
                MasseurEntity masseur = new MasseurBU().GetById(idMasseur);

                DataTable dataTable;
                DataRow dataRow;

                #region INFORMATIONS GENERALES
                //Récupération liste de genre
                dataTable = new GenreBU().Liste;
                dataRow = dataTable.NewRow();
                dataRow["genre_libelle"] = "Sexe";
                dataTable.Rows.InsertAt(dataRow, 0);

                ddlGenre.DataSource = dataTable;
                ddlGenre.DataBind();

                //Récupération liste de codes postaux et villes
                dataTable = new CpVilleBU().Liste;
                dataRow = dataTable.NewRow();
                dataRow["cp_ville_code_postal"] = "Code postal";
                dataRow["cp_ville_ville"] = "Ville";
                dataTable.Rows.InsertAt(dataRow, 0);

                ddlCodePostal.DataSource = dataTable;
                ddlCodePostal.DataBind();

                ddlVille.DataSource = dataTable;
                ddlVille.DataBind();

                //On remplit la liste
                txtNom.Text = masseur.Nom;
                txtPrenom.Text = masseur.Prenom;
                ddlGenre.SelectedValue = masseur.GenreId.ToString();
                txtTelephone.Text = masseur.Telephone;
                txtEmail.Text = masseur.Email;
                txtAdresse.Text = masseur.AdressePostale;
                ddlCodePostal.SelectedValue = masseur.CpVilleId.ToString();
                ddlVille.SelectedValue = masseur.CpVilleId.ToString();
                txtDateNaissance.Text = masseur.DateNaissance.ToShortDateString();
                cldDateNaissance.SelectedDate = masseur.DateNaissance;
                cldDateNaissance.VisibleDate = masseur.DateNaissance;
                txtDistanceMax.Text = masseur.DistanceMaxKm.ToString();
                txtDateCreation.Text = masseur.DateCreation.ToShortDateString();
                #endregion
                #region PREFERENCES
                //Récupération liste Jours Semaine
                dataTable = new JourSemBU().Liste;
                dataRow = dataTable.NewRow();
                dataRow["jour_semaine_libelle"] = "Jour";
                dataTable.Rows.InsertAt(dataRow, 0);

                //Données intégrées à la BDD
                ddlPrefJour.DataSource = dataTable;
                ddlPrefJour.DataBind();

                //On envoie les données du gridview au BU
                gvPreferences.DataSource = new MasseurBU().GetPreferences(idMasseur);
                gvPreferences.DataBind();
                #endregion
                #region INDISPONIBILITES
                //Récupération liste Motifs indisponibilité
                dataTable = new MotifIndisponibiliteBU().Liste;
                dataRow = dataTable.NewRow();
                dataRow["motif_indisponibilite_libelle"] = "Motif";
                dataTable.Rows.InsertAt(dataRow, 0);

                //Données intégrées à la BDD
                ddlIndispoMotif.DataSource = dataTable;
                ddlIndispoMotif.DataBind();

                //On envoie les données du gridview au BU
                gvIndisponibilites.DataSource = new MasseurBU().GetIndisponibilites(idMasseur, true);
                gvIndisponibilites.DataBind();
                #endregion
                #region DEPART MASSEUR
                dataTable = new MotifDepartMasseurBU().ListeDepart;
                dataRow = dataTable.NewRow();
                dataRow["motif_depart_masseur_libelle"] = "Motif";
                dataTable.Rows.InsertAt(dataRow, 0);

                //Données intégrées à la BDD
                ddlDepartMasseur.DataSource = dataTable;
                ddlDepartMasseur.DataBind();

                //On envoie les données du gridview au BU
                gvDepart.DataSource = new MasseurBU().GetDepart(idMasseur);
                gvDepart.DataBind();
                #endregion
                #region COMPETENCES
                //Récupération liste Motifs indisponibilité

                dataTable = new TypeInterventionBU().Liste;
                dataRow = dataTable.NewRow();
                dataRow["type_intervention_libelle"] = "";
                dataTable.Rows.InsertAt(dataRow, 0);

                ddlTypeIntervention.DataSource = dataTable;
                ddlTypeIntervention.DataBind();

                gvEvolutionMasseur.DataSource = new MasseurBU().GetEvolutionsMasseur(idMasseur);
                gvEvolutionMasseur.DataBind();
                #endregion
                #region MADS
                ddlPack.DataSource = new MasseurBU().ListePacks();
                ddlPack.DataBind();

                dataTable = new MasseurBU().GetTransactions(idMasseur);
                gvMads.DataSource = dataTable;
                gvMads.DataBind();

                lblTotalMadsTransactions.Text = new MasseurBU().MadsAchetes(idMasseur).ToString() + " mads achetés";
                lblCoutTotalTransactions.Text = "Coût total : " + new MasseurBU().CountMadsAchetes(idMasseur).ToString() + " €";
                #endregion
            }
        }

        #region INFORMATIONS GENERALES
        protected void btnModifierMasseur_Click(object sender, EventArgs e) {

            string text = (sender as Button).Text;

            if (text == "Modifier") {
                (sender as Button).Text = "Enregistrer";
            } else {

                new MasseurBU().ModifierMasseur(new MasseurEntity {
                    Id = idMasseur,
                    Nom = txtNom.Text,
                    Prenom = txtPrenom.Text,
                    GenreId = int.Parse(ddlGenre.SelectedValue),
                    Telephone = txtTelephone.Text,
                    Email = txtEmail.Text,
                    AdressePostale = txtAdresse.Text,
                    CpVilleId = int.Parse(ddlCodePostal.SelectedValue),
                    DateNaissance = cldDateNaissance.SelectedDate,
                    DistanceMaxKm = int.Parse(txtDistanceMax.Text),
                    DateCreation = DateTime.Parse(txtDateCreation.Text)
                });

                (sender as Button).Text = "Modifier";
            }

            txtNom.Enabled = text == "Modifier";
            txtPrenom.Enabled = text == "Modifier";
            ddlGenre.Enabled = text == "Modifier";
            txtTelephone.Enabled = text == "Modifier";
            txtEmail.Enabled = text == "Modifier";
            txtAdresse.Enabled = text == "Modifier";
            ddlCodePostal.Enabled = text == "Modifier";
            ddlVille.Enabled = text == "Modifier";
            cldDateNaissance.Enabled = text == "Modifier";
            imgMasseurCalendar.Visible = text == "Modifier";
            txtDistanceMax.Enabled = text == "Modifier";
        }

        protected void imgMasseurCalendar_Click(object sender, System.Web.UI.ImageClickEventArgs e) {
            cldDateNaissance.Visible = !cldDateNaissance.Visible;
            if (cldDateNaissance.Visible) {
                txtDateNaissance.Text = "";
            }
        }

        protected void cldDateNaissance_SelectionChanged(object sender, EventArgs e) {
            txtDateNaissance.Text = cldDateNaissance.SelectedDate.ToShortDateString();
            cldDateNaissance.Visible = false;
        }

        protected void btnSupprimerMasseur_Click(object sender, EventArgs e) {
            new MasseurBU().SupprimerMasseur(idMasseur);
            Response.Redirect("Masseurs.aspx");
        }
        #endregion
        #region PREFERENCES
        protected void imgPreferenceCalendar_Click(object sender, System.Web.UI.ImageClickEventArgs e) {
            cldDateFin.Visible = !cldDateFin.Visible;
            if (cldDateFin.Visible) {
                txtDateFin.Text = "";
            }
        }

        protected void cldDateFin_SelectionChanged(object sender, EventArgs e) {
            txtDateFin.Text = cldDateFin.SelectedDate.ToShortDateString();
            cldDateFin.Visible = false;
        }

        protected void gvPreferences_RowCommand(object sender, GridViewCommandEventArgs e) {
            //Récupération id Preference pour supprimer une préférence
            if (e.CommandName == "Supprimer") {
                int.TryParse(e.CommandArgument.ToString(), out int idPreference);
                new MasseurBU().SupprimerPreference(idPreference);
                Response.Redirect(Request.RawUrl);
            }
        }
        //Bouton visible ou pas selon l'action
        private void switchVisibilityPreferences() {
            btnAjouterPreference.Visible = !btnAjouterPreference.Visible;
            btnEnregistrer.Visible = !btnEnregistrer.Visible;
            btnAnnuler.Visible = !btnAnnuler.Visible;

            lblPrefJour.Visible = !lblPrefJour.Visible;
            ddlPrefJour.Visible = !ddlPrefJour.Visible;
            lblHeureDebut.Visible = !lblHeureDebut.Visible;
            txtDebutHeures.Visible = !txtDebutHeures.Visible;
            lblDebutHeures.Visible = !lblDebutHeures.Visible;
            txtDebutMinutes.Visible = !txtDebutMinutes.Visible;
            lblDebutMinutes.Visible = !lblDebutMinutes.Visible;
            lblDuree.Visible = !lblDuree.Visible;
            txtDureeHeures.Visible = !txtDureeHeures.Visible;
            lblDureeHeures.Visible = !lblDureeHeures.Visible;
            txtDureeMinutes.Visible = !txtDureeMinutes.Visible;
            lblDureeMinutes.Visible = !lblDureeMinutes.Visible;
            lblDateFin.Visible = !lblDateFin.Visible;
            txtDateFin.Visible = !txtDateFin.Visible;
            imgPreferenceCalendar.Visible = !imgPreferenceCalendar.Visible;
        }

        protected void btnEnregistrer_Click(object sender, EventArgs e) {
            //Récupération id Jour Semaine
            int.TryParse(ddlPrefJour.SelectedValue, out int idJour);
            int.TryParse(txtDebutHeures.Text, out int debutHeures);
            int.TryParse(txtDebutMinutes.Text, out int debutMinutes);
            int.TryParse(txtDureeHeures.Text, out int dureeHeures);
            int.TryParse(txtDureeMinutes.Text, out int dureeMinutes);

            DateTime? dateFin;
            if (cldDateFin.SelectedDate != DateTime.MinValue) {
                dateFin = cldDateFin.SelectedDate;
            } else {
                dateFin = null;
            }

            new MasseurBU().AjouterPreference(new PreferenceMasseurEntity {
                MasseurId = idMasseur,
                JourSemaineId = idJour,
                HeureDebut = new TimeSpan(debutHeures, debutMinutes, 0),
                Duree = new TimeSpan(dureeHeures, dureeMinutes, 0),
                DateCreation = DateTime.Now,
                DateFin = dateFin
            });

            Response.Redirect(Request.RawUrl);

        }

        protected void btnAjouterPreference_Click(object sender, EventArgs e) => switchVisibilityPreferences();

        protected void btnAnnuler_Click(object sender, EventArgs e) {
            switchVisibilityPreferences();
            if (cldDateFin.Visible) {
                cldDateFin.Visible = false;
            }
        }

        #endregion
        #region INDISPONIBILITES
        protected void btnEnregistrerIndisponibilite_Click(object sender, EventArgs e) {
            int.TryParse(ddlIndispoMotif.SelectedValue, out int idMotif);

            DateTime? dateFin;
            if (cldIndisponibiliteFin.SelectedDate != DateTime.MinValue) {
                dateFin = cldIndisponibiliteFin.SelectedDate;
            } else {
                dateFin = null;
            }

            new MasseurBU().AjouterIndisponibilite(new IndisponibiliteEntity {
                MasseurId = idMasseur,
                Utilisateur = true,
                MotifIndisponibiliteId = idMotif,
                Debut = cldIndisponibiliteDebut.SelectedDate,
                Fin = dateFin
            });

            Response.Redirect(Request.RawUrl);
        }

        protected void btnAjouterIndisponibilite_Click(object sender, EventArgs e) => switchVisibilityIndisponibilites();

        protected void btnAnnulerIndisponibilite_Click(object sender, EventArgs e) {
            switchVisibilityIndisponibilites();
            if (cldIndisponibiliteDebut.Visible) {
                cldIndisponibiliteDebut.Visible = false;
            }
            if (cldIndisponibiliteFin.Visible) {
                cldIndisponibiliteFin.Visible = false;
            }
        }

        protected void cldIndisponibiliteFin_SelectionChanged(object sender, EventArgs e) {
            txtIndisponibiliteFin.Text = cldIndisponibiliteFin.SelectedDate.ToShortDateString();
            cldIndisponibiliteFin.Visible = false;
        }

        protected void imgIndisponibiliteCalendarFin_Click(object sender, System.Web.UI.ImageClickEventArgs e) {
            cldIndisponibiliteFin.Visible = !cldIndisponibiliteFin.Visible;
            if (cldIndisponibiliteFin.Visible) {
                txtIndisponibiliteFin.Text = "";
            }
        }

        protected void cldIndisponibiliteDebut_SelectionChanged(object sender, EventArgs e) {
            txtIndisponibiliteDebut.Text = cldIndisponibiliteDebut.SelectedDate.ToShortDateString();
            cldIndisponibiliteDebut.Visible = false;
        }

        protected void imgIndisponibiliteCalendarDebut_Click(object sender, System.Web.UI.ImageClickEventArgs e) {
            cldIndisponibiliteDebut.Visible = !cldIndisponibiliteDebut.Visible;
            if (cldIndisponibiliteDebut.Visible) {
                txtIndisponibiliteDebut.Text = "";
            }
        }

        protected void gvIndisponibilites_RowCommand(object sender, GridViewCommandEventArgs e) {
            if (e.CommandName == "Supprimer") {
                int.TryParse(e.CommandArgument.ToString(), out int idIndisponibilite);
                new MasseurBU().SupprimerIndisponibilite(idIndisponibilite);
                Response.Redirect(Request.RawUrl);
            }
        }

        private void switchVisibilityIndisponibilites() {
            btnAjouterIndisponibilite.Visible = !btnAjouterIndisponibilite.Visible;
            btnEnregistrerIndisponibilite.Visible = !btnEnregistrerIndisponibilite.Visible;
            btnAnnulerIndisponibilite.Visible = !btnAnnulerIndisponibilite.Visible;

            lblIndispoMotif.Visible = !lblIndispoMotif.Visible;
            ddlIndispoMotif.Visible = !ddlIndispoMotif.Visible;
            lblIndisponibiliteDebut.Visible = !lblIndisponibiliteDebut.Visible;
            txtIndisponibiliteDebut.Visible = !txtIndisponibiliteDebut.Visible;
            lblIndisponibiliteFin.Visible = !lblIndisponibiliteFin.Visible;
            txtIndisponibiliteFin.Visible = !txtIndisponibiliteFin.Visible;
            imgIndisponibiliteCalendarDebut.Visible = !imgIndisponibiliteCalendarDebut.Visible;
            imgIndisponibiliteCalendarFin.Visible = !imgIndisponibiliteCalendarFin.Visible;
        }
        #endregion
        #region DEPART MASSEUR
        protected void btnEnregistrerDepart_Click(object sender, EventArgs e) {
            int.TryParse(ddlDepartMasseur.SelectedValue, out int idMotifDepart);

            DateTime? dateFin;
            if (cldDateRetour.SelectedDate != DateTime.MinValue) {
                dateFin = cldDateRetour.SelectedDate;
            } else {
                dateFin = null;
            }

            new MasseurBU().AjouterDepart(new DepartMasseurEntity {
                MasseurId = idMasseur,
                MotifDepartMasseurId = idMotifDepart,
                DateDepart = cldDateDepart.SelectedDate,
                DateRetour = dateFin
            });

            Response.Redirect(Request.RawUrl);
        }

        protected void btnAjouterDepart_Click(object sender, EventArgs e) => switchVisibilityDepart();

        protected void btnAnnulerDepart_Click(object sender, EventArgs e) {
            switchVisibilityDepart();
            if (cldDateDepart.Visible) {
                cldDateDepart.Visible = false;
            }
        }

        protected void imgDateCalendarRetour_Click(object sender, System.Web.UI.ImageClickEventArgs e) {
            cldDateRetour.Visible = !cldDateRetour.Visible;
            if (cldDateRetour.Visible) {
                txtDateRetour.Text = "";
            }
        }

        protected void cldDateDepart_SelectionChanged(object sender, EventArgs e) {
            txtDateDepart.Text = cldDateDepart.SelectedDate.ToShortDateString();
            cldDateDepart.Visible = false;
        }

        protected void imgDateCalendarDepart_Click(object sender, System.Web.UI.ImageClickEventArgs e) {
            cldDateDepart.Visible = !cldDateDepart.Visible;
            if (cldDateDepart.Visible) {
                txtDateDepart.Text = "";
            }
        }

        protected void cldDateRetour_SelectionChanged(object sender, EventArgs e) {
            txtDateRetour.Text = cldDateRetour.SelectedDate.ToShortDateString();
            cldDateRetour.Visible = false;
        }
        protected void gvDepart_RowCommand(object sender, GridViewCommandEventArgs e) {
            if (e.CommandName == "Supprimer") {
                int.TryParse(e.CommandArgument.ToString(), out int idDepart);
                new MasseurBU().SupprimerDepart(idDepart);
                Response.Redirect(Request.RawUrl);
            }
        }
        private void switchVisibilityDepart() {
            btnAjouterDepart.Visible = !btnAjouterDepart.Visible;
            btnEnregistrerDepart.Visible = !btnEnregistrerDepart.Visible;
            btnAnnulerDepart.Visible = !btnAnnulerDepart.Visible;

            lblDepartMotif.Visible = !lblDepartMotif.Visible;
            ddlDepartMasseur.Visible = !ddlDepartMasseur.Visible;
            lblDateDepart.Visible = !lblDateDepart.Visible;
            txtDateDepart.Visible = !txtDateDepart.Visible;
            lblDateRetour.Visible = !lblDateRetour.Visible;
            txtDateRetour.Visible = !txtDateRetour.Visible;
            imgDateCalendarDepart.Visible = !imgDateCalendarDepart.Visible;
            imgDateCalendarRetour.Visible = !imgDateCalendarRetour.Visible;
        }
        #endregion
        #region COMPETENCES
        protected void btnAjouterCompetence_Click(object sender, EventArgs e) => switchVisibilityCompetences();

        protected void btnEnregistrerCompetence_Click(object sender, EventArgs e) {
            int.TryParse(ddlCompetence.SelectedValue, out int idCompetence);

            new MasseurBU().AjouterEvolutionMasseur(new EvolutionMasseurEntity {
                MasseurId = idMasseur,
                CompetenceId = int.Parse(ddlCompetence.SelectedValue),
                DateEvolution = DateTime.Now
            });

            Response.Redirect(Request.RawUrl);
        }

        protected void btnAnnulerCompetence_Click(object sender, EventArgs e) => switchVisibilityCompetences();

        private void switchVisibilityCompetences() {
            btnAjouterCompetence.Visible = !btnAjouterCompetence.Visible;
            btnEnregistrerCompetence.Visible = !btnEnregistrerCompetence.Visible;
            btnAnnulerCompetence.Visible = !btnAnnulerCompetence.Visible;

            lblTypeIntervention.Visible = !lblTypeIntervention.Visible;
            ddlTypeIntervention.Visible = !ddlTypeIntervention.Visible;
            lblCompetence.Visible = !lblCompetence.Visible;
            ddlCompetence.Visible = !ddlCompetence.Visible;
        }

        protected void ddlTypeIntervention_SelectedIndexChanged(object sender, EventArgs e) {
            if (ddlTypeIntervention.SelectedIndex != 0) {
                DataTable dataTable = new CompetenceBU().Liste(Int32.Parse(ddlTypeIntervention.SelectedValue));
                DataRow dataRow = dataTable.NewRow();

                dataRow["competence_libelle"] = "";
                dataTable.Rows.InsertAt(dataRow, 0);

                ddlCompetence.DataSource = dataTable;
            } else {
                ddlCompetence.SelectedIndex = 0;
                ddlCompetence.DataSource = null;
            }

            lblCompetence.Visible = ddlTypeIntervention.SelectedIndex != 0;
            ddlCompetence.Visible = ddlTypeIntervention.SelectedIndex != 0;

            ddlCompetence.DataBind();
        }
        protected void gvEvolutionMasseur_RowCommand(object sender, GridViewCommandEventArgs e) {
            if (e.CommandName == "Supprimer") {
                int.TryParse(e.CommandArgument.ToString(), out int idEvolutionMasseur);
                new MasseurBU().SupprimerEvolutionMasseur(idEvolutionMasseur);
                Response.Redirect(Request.RawUrl);
            }
        }
        #endregion
        #region MADS
        public static int TotalMads(int nbPacks, int madsParPack) {
            return nbPacks * madsParPack;
        }
        public static double PrixTotal(int nbPacks, double prixUnitaire) {
            return nbPacks * prixUnitaire;
        }
        
        protected void ddlPack_SelectedIndexChanged(object sender, EventArgs e) {
            PackMadsEntity pack = new MasseurBU().PackMads(Int32.Parse(ddlPack.SelectedValue));
            txtPrixUnitaire.Text = pack.PrixUnitaire.ToString();
            txtPrixTotalTransaction.Text = (pack.PrixUnitaire * int.Parse(txtQuantite.Text)).ToString();
        }
        
        protected void txtQuantite_TextChanged(object sender, EventArgs e) {
            PackMadsEntity pack = new MasseurBU().PackMads(Int32.Parse(ddlPack.SelectedValue));
            txtPrixUnitaire.Text = pack.PrixUnitaire.ToString();
            txtPrixTotalTransaction.Text = (pack.PrixUnitaire * int.Parse(txtQuantite.Text)).ToString();
        }

        protected void btnEngistrerTransaction_Click(object sender, EventArgs e) {
            int.TryParse(ddlPack.SelectedValue, out int idPack);

            new MasseurBU().AjouterTransaction(new TransactionPackMadsEntity {
                MasseurId = idMasseur,
                PackMadsId = idPack,
                Quantite = int.Parse(txtQuantite.Text),
                DateTransaction = DateTime.Now
            });

            Response.Redirect(Request.RawUrl);

        }

        protected void btnAjouterTransaction_Click(object sender, EventArgs e) => switchVisibilityMads();

        private void switchVisibilityMads() {
            lblPack.Visible = !lblPack.Visible; 
            ddlPack.Visible = !ddlPack.Visible;

            lblQuantite.Visible = !lblQuantite.Visible;
            txtQuantite.Visible = !txtQuantite.Visible;

            lblPrixUnitaire.Visible = !lblPrixUnitaire.Visible;
            txtPrixUnitaire.Visible = !txtPrixUnitaire.Visible;

            lblPrixTotalTransaction.Visible = !lblPrixTotalTransaction.Visible;
            txtPrixTotalTransaction.Visible = !txtPrixTotalTransaction.Visible;

            btnEngistrerTransaction.Visible = !btnEngistrerTransaction.Visible;
            btnAnnulerTransaction.Visible = !btnAnnulerTransaction.Visible;
            btnAjouterTransaction.Visible = !btnAjouterTransaction.Visible;

        }
        protected void btnAnnulerTransaction_Click(object sender, EventArgs e) => switchVisibilityMads();
        #endregion

        protected void BtnDemo_Click(object sender, EventArgs e) {
            ddlPack.SelectedIndex = 3;
            txtQuantite.Text = "2";
            PackMadsEntity pack = new MasseurBU().PackMads(Int32.Parse(ddlPack.SelectedValue));
            txtPrixUnitaire.Text = pack.PrixUnitaire.ToString();
            txtPrixUnitaire.Text = pack.PrixUnitaire.ToString();
            txtPrixTotalTransaction.Text = (pack.PrixUnitaire * int.Parse(txtQuantite.Text)).ToString();
        }
    }
}