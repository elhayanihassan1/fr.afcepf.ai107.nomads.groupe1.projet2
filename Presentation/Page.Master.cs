﻿using System;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Presentation {
    public partial class Page : System.Web.UI.MasterPage {
        protected void Page_Load(object sender, EventArgs e) {

        }

        protected void imgLogoNomads_Click(object sender, System.Web.UI.ImageClickEventArgs e) {
            Response.Redirect("Home.aspx");
        }
    }
}