﻿<%@ Page
    Title="Liste de tournées"
    Language="C#"
    MasterPageFile="~/Page.Master"
    AutoEventWireup="true"
    CodeBehind="Tournees.aspx.cs"
    Inherits="FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Presentation.Tournees" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="principal" runat="server">
    <h1 aria-atomic="False">Liste des tournées</h1>
    <table>
        <tr>
            <td>
                <asp:Button
                    ID="btnAjouterTournee"
                    runat="server"
                    Text="Ajouter"
                    OnClick="btnAjouterTournee_Click"
                    BackColor="#009933"
                    BorderStyle="Outset"
                    Font-Bold="True"
                    Font-Size="Medium"
                    ForeColor="White" />
            </td>
            <td>
                <asp:CheckBox
                    ID="chkShowFinished"
                    runat="server"
                    OnCheckedChanged="chkShowFinished_CheckedChanged" 
                    Checked="false"
                    AutoPostBack="true"
                    Text="Afficher les tournées achevées" />
            </td>
        </tr>
    </table>
    <br />
    <asp:GridView
        ID="gvTournees"
        CssClass="cssGvTournees"
        runat="server"
        CellPadding="5"
        CellSpacing="10"
        EmptyDataText="Aucune tournée"
        OnRowDataBound="gvTournees_RowDataBound"
        AutoGenerateColumns="false">
        <AlternatingRowStyle BackColor="#CCFFCC" />
        <Columns>
            <asp:BoundField DataField="partenaire_nom" HeaderText="Partenaire" />
            <asp:BoundField DataField="tournee_nom" HeaderText="Nom" />
            <asp:BoundField DataField="jour_semaine_libelle" HeaderText="Jour" />
            <asp:BoundField DataField="type_intervention_libelle" HeaderText="Type" />
            <asp:BoundField DataField="competence_libelle" HeaderText="Compétence" />
            <asp:BoundField DataField="tournee_heure_debut" HeaderText="Heure de début" DataFormatString="{0:hh\:mm}" />
            <asp:BoundField DataField="tournee_duree" HeaderText="Durée" DataFormatString="{0:hh\:mm}" />
            <asp:BoundField DataField="tournee_min_binomes" HeaderText="Binômes (min)" />
            <asp:BoundField DataField="tournee_max_binomes" HeaderText="Binômes (max)" />
            <asp:BoundField DataField="tournee_cout_mads" HeaderText="Coût (mads)" />
            <asp:BoundField DataField="tournee_date_fin" HeaderText="Date de fin" DataFormatString="{0:dd/MM/yyyy}" />
            <asp:BoundField DataField="tournee_date_creation" HeaderText="Date de création" DataFormatString="{0:dd/MM/yyyy}" />
            <asp:HyperLinkField
                DataNavigateUrlFormatString="Tournee.aspx?idTournee={0}"
                DataNavigateUrlFields="tournee_id"
                HeaderText="Détails"
                Text="Voir" />
        </Columns>
        <HeaderStyle BackColor="Black" ForeColor="White" />
    </asp:GridView>
</asp:Content>
