﻿using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Business;
using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Presentation {
    public partial class EditionPartenaire : System.Web.UI.Page {

        private int idPartenaire;
        CpVilleBU buCp = new CpVilleBU();
        PartenaireBU bu = new PartenaireBU();

        protected void Page_Load(object sender, EventArgs e) {


            // 1- je récupère l'id de Partenaire :
            int.TryParse(Request["id"], out idPartenaire);


            // 2- je remplis la liste de CP Ville:

            List<CpVilleEntity> listCp = new List<CpVilleEntity>();
            DataTable dtCp = buCp.Liste;

            for (int i = 0; i < dtCp.Rows.Count; i++) {
                CpVilleEntity cp = new CpVilleEntity();
                cp.Id = Convert.ToInt32(dtCp.Rows[i]["cp_ville_id"]);
                cp.CodePostal = dtCp.Rows[i]["cp_ville_code_postal"].ToString();
                cp.Ville = dtCp.Rows[i]["cp_ville_ville"].ToString();
                listCp.Add(cp);
            }


            if (!IsPostBack) {
                listCp.Insert(0, new CpVilleEntity() {
                    Id = -1,
                    CodePostal = "Aucun",
                    Ville = "Aucun"
                });
                ddlCodePostal.DataTextField = "CodePostal";
                ddlCodePostal.DataValueField = "Id";
                ddlCodePostal.DataSource = listCp;
                ddlCodePostal.DataBind();

                ddlVille.DataTextField = "Ville";
                ddlVille.DataValueField = "Id";
                ddlVille.DataSource = listCp;
                ddlVille.DataBind();



                PartenaireEntity partenaire = bu.GetDetailPartenaire(idPartenaire);
                txtNom.Text = partenaire.Nom;
                txtTelephone.Text = partenaire.Telephone;
                txtEmail.Text = partenaire.Mail;
                txtAdressePostal.Text = partenaire.AdressePostale;
                ddlCodePostal.SelectedValue = partenaire.CpVilleId.ToString();
                ddlVille.SelectedValue = partenaire.CpVilleId.ToString();
                txtNomReferent.Text = partenaire.NomReferent;
                txtPrenomReferent.Text = partenaire.PrenomReferent;

            }
        }

        protected void btnEnregistrer_Click(object sender, EventArgs e) {
            // créer un PartenaireEntity

            PartenaireEntity partenaire = new PartenaireEntity();

            partenaire.Nom = txtNom.Text;
            partenaire.Telephone = txtTelephone.Text;
            partenaire.Mail = txtEmail.Text;
            partenaire.AdressePostale = txtAdressePostal.Text;
            if (ddlCodePostal.SelectedIndex > 0) {
                partenaire.CpVilleId = int.Parse(ddlCodePostal.SelectedValue);
            }

            if (ddlVille.SelectedIndex > 0) {
                partenaire.CpVilleId = int.Parse(ddlVille.SelectedValue);
            }
            partenaire.NomReferent = txtNomReferent.Text;
            partenaire.PrenomReferent = txtPrenomReferent.Text;

            partenaire.DateCreation = DateTime.Now;

            partenaire.Id = idPartenaire;

            // Envoyer au business

            bu.ModifierPartenaire(partenaire);

            Response.Redirect("BarEtRestaurants.aspx");

        }
    }
}