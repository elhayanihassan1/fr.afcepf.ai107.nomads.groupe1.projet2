﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Page.Master" AutoEventWireup="true" CodeBehind="DétailsIntervention.aspx.cs" Inherits="FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Presentation.DétailsIntervention" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>



<asp:Content ID="Content2" ContentPlaceHolderID="principal" runat="server">

    <table class="DetailsMassageIntervention">
        <tr>
            <td>
                <h1>Massage intervention :</h1>
                <asp:GridView ID="gvMassageIntervention" CssClass="cssGvMassageIntervention" runat="server" AutoGenerateColumns="false" OnRowCommand="gvMassageIntervention_RowCommand">
                    <Columns>
                        <asp:BoundField DataField="type_massage_libelle" HeaderText="Type massage" />
                        <asp:BoundField DataField="massage_intervention_nombre_masseurs" HeaderText="Nombre de masseurs" />

                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button
                                    ID="btnSupprimerMassage"
                                    runat="server"
                                    CommandName="Supprimer"
                                    CommandArgument='<%# Bind("massage_intervention_id") %>'
                                    BackColor="Red"
                                    ForeColor="White"
                                    Text="Supprimer" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle BackColor="Black" ForeColor="White" />
                </asp:GridView>
                <br />
                <table>
                    <tr>
                        <td>
                            <asp:Label ID="lblTypeMassage" runat="server" Visible="false"> type du massage : </asp:Label></td>
                        <td>
                            <asp:DropDownList
                                ID="ddlTypeMassage"
                                runat="server"
                                DataValueField="type_massage_id"
                                DataTextField="type_massage_libelle"
                                Visible="false">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblNombreMasseurs" runat="server" Visible="false">Nombre des masseurs : </asp:Label></td>
                        <td>
                            <asp:TextBox ID="txtNombreMasseurs" runat="server" Visible="false"></asp:TextBox>


                        </td>
                    </tr>

                </table>
                <asp:Button ID="btnAjouterMassageIntervention" runat="server" Text="Ajouter" OnClick="btnAjouterMassageIntervention_Click" BackColor="#009900" BorderStyle="Outset" Font-Bold="True" Font-Size="Medium" ForeColor="White" />
                <asp:Button ID="btnEnregistrerMassageIntervention" runat="server" Text="Enregistrer" Visible="false" OnClick="btnEnregistrerMassageIntervention_Click" BackColor="Blue" BorderStyle="Outset" Font-Bold="True" Font-Size="Medium" ForeColor="White" />
                <asp:Button ID="btnAnnulerMassageIntervention" runat="server" Text="Annuler" Visible="false" OnClick="btnAnnulerMassageIntervention_Click" BackColor="Red" BorderStyle="Outset" Font-Bold="True" Font-Size="Medium" ForeColor="White" />
            </td>
        </tr>
    </table>

</asp:Content>
