﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Page.Master" AutoEventWireup="true" CodeBehind="Entreprise.aspx.cs" Inherits="FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Presentation.Entreprise" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="principal" runat="server">

    <h1>Liste des partenaires : Entreprise   </h1>


    <div class="boite">
        Nom :
        <asp:TextBox ID="txtNom" runat="server"></asp:TextBox>
        Nom référent :
        <asp:TextBox ID="txtNomReferent" runat="server"></asp:TextBox>
        Prenom référent
        <asp:TextBox ID="txtPrenomReferent" runat="server"></asp:TextBox>


        <asp:Button ID="btnRechercher" runat="server" OnClick="btnRechercher_Click" Text="Recherche" BackColor="#CCCCCC" BorderStyle="Outset" Font-Bold="True" />


    </div>
    <hr />
    <asp:GridView
        ID="gvEntreprise"
        runat="server"
        CssClass="cssGvEntreprise"
        CellPadding="5"
        CellSpacing="10"
        EmptyDataText="Aucune entreprise"
        AutoGenerateColumns="false">
        <AlternatingRowStyle BackColor="#CCFFCC" />
        <Columns>
            <asp:BoundField DataField="partenaire_nom" HeaderText="Nom" />
            <asp:BoundField DataField="partenaire_mail" HeaderText="Email" />
            <asp:BoundField DataField="partenaire_telephone" HeaderText="Téléphone" />
            <asp:BoundField DataField="partenaire_nom_referent" HeaderText="Nom réferent" />
            <asp:BoundField DataField="partenaire_prenom_referent" HeaderText="Prénom réferent" />
            <asp:HyperLinkField
                Text="Voir détails"
                DataNavigateUrlFormatString="Partenaire.aspx?id={0}"
                DataNavigateUrlFields="partenaire_id" />

            <asp:HyperLinkField
                Text=" Editer"
                DataNavigateUrlFormatString="EditionPartenaire.aspx?id={0}"
                DataNavigateUrlFields="partenaire_id" />

             <asp:HyperLinkField
                Text="Tournées"
                DataNavigateUrlFormatString="Tournees.aspx?idPartenaire={0}"
                DataNavigateUrlFields="partenaire_id" />

            <asp:HyperLinkField
                Text="Interventions"
                DataNavigateUrlFormatString="Intervention.aspx?idPartenaire={0}"
                DataNavigateUrlFields="partenaire_id" />

        </Columns>
        <HeaderStyle BackColor="Black" BorderStyle="Outset" ForeColor="White" />
    </asp:GridView>

    <br />
    <asp:Button ID="btnAjouter" runat="server" OnClick="btnAjouter_Click" Text="Ajouter" BackColor="#009933" BorderStyle="Outset" Font-Bold="True" Font-Size="Medium" ForeColor="White" />




</asp:Content>
