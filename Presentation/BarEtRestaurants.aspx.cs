﻿using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Business;
using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Presentation
{
    public partial class BarEtRestaurants : System.Web.UI.Page
    {
        
        PartenaireBU bu = new PartenaireBU();
        protected void Page_Load(object sender, EventArgs e)
        {
  
            #region  Rechercher un Partenaire


                if (!IsPostBack)
                {


                gvBarEtRestaurants.DataSource = bu.ListBarRestaurants;
                gvBarEtRestaurants.DataBind();

             
                }

            #endregion

        }

 

        protected void btnRechercher_Click(object sender, EventArgs e)
        {
   

            string nom = txtNom.Text;
            if (nom.Length > 0)
            {
                nom = nom.Substring(0, 1).ToUpper() + nom.Substring(1).ToLower();
            }

            string NomRreferent = txtNomReferent.Text;
            if (NomRreferent.Length > 0)
            {
                NomRreferent = NomRreferent.Substring(0, 1).ToUpper() + NomRreferent.Substring(1).ToLower();
            }
            string PrenomReferent = txtPrenomReferent.Text;
            if (PrenomReferent.Length > 0)
            {
                PrenomReferent = PrenomReferent.Substring(0, 1).ToUpper() + PrenomReferent.Substring(1).ToLower();
            }

            gvBarEtRestaurants.DataSource = bu.RecherchePartenaireBR( nom, NomRreferent,PrenomReferent );
            gvBarEtRestaurants.DataBind();

    
        }


        protected void btnaAjouter_Click(object sender, EventArgs e)
        {
            // redirection vers la page d'ajout
            Response.Redirect("AjoutPartenaire.aspx");

        }

        protected void gvBarEtRestaurants_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}