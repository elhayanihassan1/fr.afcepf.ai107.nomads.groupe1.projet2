﻿using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Business;
using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities;
using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Presentation {
    public partial class Tournee : System.Web.UI.Page {

        private TourneeEntity tournee;

        protected void Page_Load(object sender, EventArgs e) {

            if (!int.TryParse(Request["idTournee"], out int idTournee)) {
                Response.Redirect("404.aspx");
            } else if (!new TourneeBU().TourneeExiste(idTournee)) {
                Response.Redirect("404.aspx");
            }
            tournee = new TourneeBU().GetById(idTournee);

            if (!IsPostBack) {

                lblHidden.Text = Request.UrlReferrer.ToString();

                DataTable dataTable;
                DataRow dataRow;

                #region INFORMATIONS GENERALES
                dataTable = new PartenaireBU().GetListPartenaire();
                dataRow = dataTable.NewRow();
                dataRow["partenaire_nom"] = "Lieu";
                dataTable.Rows.InsertAt(dataRow, 0);

                ddlPartenaire.DataSource = dataTable;
                ddlPartenaire.DataBind();

                dataTable = new TypeInterventionBU().Liste;
                dataRow = dataTable.NewRow();
                dataRow["type_intervention_libelle"] = "Type";
                dataTable.Rows.InsertAt(dataRow, 0);

                ddlType.DataSource = dataTable;
                ddlType.DataBind();

                dataTable = new JourSemBU().Liste;
                dataRow = dataTable.NewRow();
                dataRow["jour_semaine_libelle"] = "Jour";
                dataTable.Rows.InsertAt(dataRow, 0);

                ddlJourSem.DataSource = dataTable;
                ddlJourSem.DataBind();

                ddlPartenaire.SelectedValue = tournee.PartenaireId.ToString();
                txtNomTournee.Text = tournee.Nom;
                ddlJourSem.SelectedValue = tournee.JourSemaineId.ToString();
                ddlType.SelectedValue = tournee.TypeInterventionId.ToString();
                ddlCompetence.SelectedValue = tournee.CompetenceId.ToString();
                txtDebutHeures.Text = tournee.HeureDebut.Hours.ToString();
                txtDebutMinutes.Text = tournee.HeureDebut.Minutes.ToString();
                txtDureeHeures.Text = tournee.Duree.Hours.ToString();
                txtDureeMinutes.Text = tournee.Duree.Minutes.ToString();
                txtMinBinomes.Text = tournee.MinBinomes.ToString();
                txtMaxBinomes.Text = tournee.MaxBinomes.ToString();
                txtCoutMads.Text = tournee.CoutMads.ToString();
                if (DateTime.TryParse(tournee.DateFin.ToString(), out DateTime date)) {
                    txtTourneeDateFin.Text = date.ToShortDateString();
                }
                txtTourneeDateCreation.Text = tournee.DateCreation.ToShortDateString();
                if (DateTime.TryParse(tournee.DateAnnulation.ToString(), out date)) {
                    txtTourneeDateAnnulation.Text = date.ToShortDateString();
                }
                ddlTourneeMotifAnnulation.SelectedValue = tournee.MotifAnnulationTourneeId.ToString();
                #endregion
                #region ACTIONS
                gvActions.DataSource = new TourneeBU().GetActions(idTournee);
                gvActions.DataBind();

                btnAjouterAction.Visible = tournee.DateFin == null;
                #endregion
            }

        }

        protected void cldTourneeDateFin_SelectionChanged(object sender, EventArgs e) {
            txtTourneeDateFin.Text = cldTourneeDateFin.SelectedDate.ToShortDateString();
            cldTourneeDateFin.Visible = false;
        }

        protected void imgTourneeDateFin_Click(object sender, ImageClickEventArgs e) {
            cldTourneeDateFin.Visible = !cldTourneeDateFin.Visible;
            if (cldTourneeDateFin.Visible) {
                txtTourneeDateFin.Text = "";
            }
        }

        protected void btnModifierTournee_Click(object sender, EventArgs e) {
            string text = (sender as Button).Text;

            if (text == "Modifier") {
                (sender as Button).Text = "Enregistrer";
            } else {

                tournee.PartenaireId = int.Parse(ddlPartenaire.SelectedValue);
                tournee.Nom = txtNomTournee.Text;
                tournee.JourSemaineId = int.Parse(ddlJourSem.SelectedValue);
                tournee.TypeInterventionId = int.Parse(ddlType.SelectedValue);
                tournee.CompetenceId = int.Parse(ddlCompetence.SelectedValue);
                tournee.HeureDebut = new TimeSpan(int.Parse(txtDebutHeures.Text), int.Parse(txtDebutMinutes.Text), 0);
                tournee.Duree = new TimeSpan(int.Parse(txtDureeHeures.Text), int.Parse(txtDureeMinutes.Text), 0);
                tournee.MinBinomes = int.Parse(txtMinBinomes.Text);
                tournee.MaxBinomes = int.Parse(txtMaxBinomes.Text);
                tournee.CoutMads = int.Parse(txtCoutMads.Text);

                if (DateTime.TryParse(txtTourneeDateFin.Text, out DateTime date)) {
                    tournee.DateFin = date;
                } else {
                    tournee.DateFin = null;
                }

                if (DateTime.TryParse(txtTourneeDateAnnulation.Text, out date)) {
                    tournee.DateAnnulation = date;
                } else {
                    tournee.DateAnnulation = null;
                }

                if (int.TryParse(ddlTourneeMotifAnnulation.SelectedValue, out int value)) {
                    tournee.MotifAnnulationTourneeId = value;
                } else {
                    tournee.MotifAnnulationTourneeId = null;
                }

                new TourneeBU().ModifierTournee(tournee);

                (sender as Button).Text = "Modifier";
            }

            txtNomTournee.Enabled = text == "Modifier";
            ddlPartenaire.Enabled = text == "Modifier";
            ddlJourSem.Enabled = text == "Modifier";
            txtDebutHeures.Enabled = text == "Modifier";
            txtDebutMinutes.Enabled = text == "Modifier";
            txtDureeHeures.Enabled = text == "Modifier";
            txtDureeMinutes.Enabled = text == "Modifier";
            txtMinBinomes.Enabled = text == "Modifier";
            txtMaxBinomes.Enabled = text == "Modifier";
            txtCoutMads.Enabled = text == "Modifier";
            cldTourneeDateFin.Enabled = text == "Modifier";
            imgTourneeDateFin.Visible = text == "Modifier";
        }

        protected void btnSupprimerTournee_Click(object sender, EventArgs e) {
            new TourneeBU().SupprimerTournee(tournee.Id);
            Response.Redirect(lblHidden.Text);
        }

        protected void gvActions_RowCommand(object sender, GridViewCommandEventArgs e) {
            int.TryParse(e.CommandArgument.ToString(), out int idAction);
            if (e.CommandName == "Supprimer") {
                new TourneeBU().SupprimerAction(idAction);
                Response.Redirect(Request.RawUrl);
            } else if (e.CommandName == "Affecter") {
                Response.Redirect("Action.aspx?idAction=" + idAction);
            }
        }

        protected void btnAjouterAction_Click(object sender, EventArgs e) {

            int jourSemaine = new JourSemBU().GetById(tournee.JourSemaineId).CsharpValue;
            int daysToAdd = (jourSemaine - (int)DateTime.Today.AddDays(1).DayOfWeek + 7) % 7;
            DateTime nextAction = DateTime.Today.AddDays(daysToAdd + 1);

            while (new TourneeBU().ActionExiste(
                tournee.Id, nextAction + tournee.HeureDebut,
                nextAction + tournee.HeureDebut + tournee.Duree)) {
                nextAction = nextAction.AddDays(7);
            }

            new TourneeBU().AjouterAction(new ActionEntity() {
                TourneeId = tournee.Id,
                Debut = nextAction + tournee.HeureDebut,
                Fin = nextAction + tournee.HeureDebut + tournee.Duree
            });

            Response.Redirect(Request.RawUrl);
        }

        protected void ddlType_SelectedIndexChanged(object sender, EventArgs e) {

            if (ddlType.SelectedIndex != 0) {
                DataTable dataTable = new CompetenceBU().Liste(Int32.Parse(ddlType.SelectedValue));
                DataRow dataRow = dataTable.NewRow();

                dataRow["competence_libelle"] = "Compétence";
                dataTable.Rows.InsertAt(dataRow, 0);

                ddlCompetence.DataSource = dataTable;
            } else {
                ddlCompetence.SelectedIndex = 0; 
                ddlCompetence.DataSource = null;
            }

            lblCompetence.Visible = ddlType.SelectedIndex != 0;
            ddlCompetence.Visible = ddlType.SelectedIndex != 0;

            ddlCompetence.DataBind();

        }

        protected void chkShowFinished_CheckedChanged(object sender, EventArgs e) {

        }
    }
}