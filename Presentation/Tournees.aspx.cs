﻿using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Business;
using System;
using System.Data;
using System.Globalization;
using System.Web.UI.WebControls;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Presentation {
    public partial class Tournees : System.Web.UI.Page {

        private bool partenaireUniquement;
        private int idPartenaire;
        private DataTable dataTable;
        private DataView dataView;

        protected void Page_Load(object sender, EventArgs e) {

            partenaireUniquement = int.TryParse(Request["idPartenaire"], out idPartenaire);


            if (!IsPostBack) {
                InitGridView();
            }
        }

        protected void btnAjouterTournee_Click(object sender, EventArgs e) {
            if (partenaireUniquement) {
                Response.Redirect("AjoutTournee.aspx?idPartenaire=" + idPartenaire);
            } else {
                Response.Redirect("AjoutTournee.aspx");
            }
        }

        protected void chkShowFinished_CheckedChanged(object sender, EventArgs e) {
            InitGridView();
        }

        protected void gvTournees_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e) {
            //if (e.Row.RowType == DataControlRowType.DataRow) {
            //    TableCell cell = e.Row.Cells[6];
            //    cell.Text = string.Format(@"{0:mm\:ss}", cell.Text);
            //}
        }

        private void InitGridView() {
            if (partenaireUniquement) {
                dataTable = new TourneeBU().TourneesPartenaire(idPartenaire);
            } else {
                dataTable = new TourneeBU().Tournees();
            }
            dataView = new DataView(dataTable);

            if (!chkShowFinished.Checked) {
                dataView.RowFilter = String.Format(CultureInfo.InvariantCulture.DateTimeFormat,
                 "tournee_date_fin >= #{0}#", DateTime.Now) + " OR tournee_date_fin IS NULL";
            }

            gvTournees.DataSource = dataView;
            gvTournees.DataBind();
        }
    }
}