﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Page.Master" AutoEventWireup="true" CodeBehind="Partenaire.aspx.cs" Inherits="FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Presentation.Partenaire" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">



</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="principal" runat="server">

     <h2>    Détails Partenaire  

             <a href="BarEtRestaurants.aspx">< retour Liste Partenaires</a> 

 
      </h2>

      <hr />
          
 
    <br />

    <table class="DetailsPartenaire">

     
        <tr>
            <td>
                 <h1>Informations générales</h1>
                <table>
                     <tr>
         
            <td>
                <asp:Label ID="lblNom" runat="server" Text="Nom :"></asp:Label>
            </td>

            <td >
                <asp:Label ID="lblNomSaisie" runat="server"></asp:Label>
            </td>
        </tr>
     
        <tr>
            <td>
                <asp:Label ID="lblTelephone" runat="server" Text="Telephone :"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblTelephoneSaisie" runat="server"></asp:Label>
            </td>
        </tr>
      
        <tr>
            <td>
                <asp:Label ID="lblEmail" runat="server" Text="Email :"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblEmailSaisie" runat="server"></asp:Label>
            </td>
        </tr>
 
        <tr>
            <td>
                <asp:Label ID="lblAdressePostal" runat="server" Text="Adresse postal :"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblAdressePostalSaisie" runat="server"></asp:Label>
            </td>
        </tr>
    
        <tr>
            <td>
                <asp:Label ID="lblCodePostal" runat="server" Text="Code postal :"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblCodePostalSaisie" runat="server"></asp:Label>
            </td>
        </tr>
     
        <tr>
            <td>
                <asp:Label ID="lblVille" runat="server" Text="Ville :"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblVilleSaisie" runat="server"></asp:Label>
            </td>
        </tr>
     
        <tr>
            <td>
                <asp:Label ID="lblNomReferent" runat="server" Text="Nom référent :"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblNomReferentSaisie" runat="server"></asp:Label>
            </td>
        </tr>
   
        <tr>
            <td>
                <asp:Label ID="lblPrenomReferent" runat="server" Text="Prenom référent :"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblPrenomReferentSaisie" runat="server"></asp:Label>
            </td>
        </tr>

        <tr>
            <td>
                <asp:Label ID="Label19" runat="server" Text="       "></asp:Label>
                <asp:Label ID="lblDateCreation" runat="server" Text="Date  d'inscription :"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblDateCreationSaisie" runat="server"></asp:Label>
            </td>
        </tr>
       </table>
        <br />
         
                <asp:Button ID="btnSupprimer" runat="server" OnClick="btnSupprimer_Click" Text="Supprimer" BackColor="#FF3300" BorderStyle="Outset" BorderWidth="4px" Font-Bold="True" Font-Overline="False" Font-Size="Medium" ForeColor="White" />
              
          
                <asp:Button ID="btnModifier" runat="server" OnClick="btnModifier_Click" Text ="Modifier" BackColor="#009933" BorderStyle="Outset" Font-Bold="True" Font-Size="Medium" ForeColor="White" />

    </td>
        </tr>
    </table>

     <table class="DetailsPartenaire">
        <tr>
            <td>
                <h1>Depart partenaire :</h1>
                <asp:GridView
                    ID="gvDepartPartenaire"
                    CssClass="cssGvIndisponibilites"
                    runat="server"
                    AutoGenerateColumns="false"
                    CellPadding="5"
                    CellSpacing="10"
                    EmptyDataText="Aucun départ"
                    OnRowCommand="gvDepartPartenaire_RowCommand">
                    <Columns>
                        <asp:BoundField DataField="motif_depart_partenaire_libelle" HeaderText="Motif" />
                        <asp:BoundField DataField="depart_partenaire_date_depart" HeaderText="Date depart" DataFormatString="{0:dd/MM/yyyy}" />
                        <asp:BoundField DataField="depart_partenaire_date_retour" HeaderText="Date retour" DataFormatString="{0:dd/MM/yyyy}" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button
                                    ID="btnSupprimerDepartPartenaire"
                                    runat="server"
                                    CommandName="Supprimer"
                                    CommandArgument='<%# Bind("depart_partenaire_id") %>'
                                    Text="Supprimer" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle BackColor="Black" ForeColor="White" />
                </asp:GridView>
                <br />
                <table>
                    <tr>
                        <td>
                            <asp:Label ID="lbldeparMotif" runat="server" Visible="false">Motif : </asp:Label></td>
                        <td>
                            <asp:DropDownList
                                ID="ddlDepartMotif"
                                runat="server"
                                DataValueField="motif_depart_partenaire_id"
                                DataTextField="motif_depart_partenaire_libelle"
                                Visible="false">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblDateDepart" runat="server" Visible="false">Date depart : </asp:Label></td>
                        <td>
                            <asp:TextBox ID="txtDateDepart" runat="server" Enabled="false" Visible="false"></asp:TextBox>
                            <asp:ImageButton
                                ID="imgDateCalendarDepart"
                                runat="server"
                                ImageUrl="~/images/Calendar.png"
                                Height="20"
                                Width="20"
                                OnClick="imgDateCalendarDepart_Click"
                                Visible="false" />
                            <asp:Calendar ID="cldDateDepart" runat="server" OnSelectionChanged="cldDateDepart_SelectionChanged" Visible="false"></asp:Calendar>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblDateRetour" runat="server" Visible="false">Dade retour : : </asp:Label></td>
                        <td>
                            <asp:TextBox ID="txtDateRetour" runat="server" Enabled="false" Visible="false"></asp:TextBox>
                            <asp:ImageButton
                                ID="imgDateCalendarRetour"
                                runat="server"
                                ImageUrl="~/images/Calendar.png"
                                Height="20"
                                Width="20"
                                OnClick="imgDateCalendarRetour_Click"
                                Visible="false" />
                            <asp:Calendar ID="cldDateRetour" runat="server" OnSelectionChanged="cldDateRetour_SelectionChanged" Visible="false"></asp:Calendar>
                        </td>
                    </tr>
                </table>
                <asp:Button ID="btnAjouterDepart" runat="server" Text="Ajouter" OnClick="btnAjouterDepart_Click" BackColor="#009900" BorderStyle="Outset" Font-Bold="True" Font-Size="Medium" ForeColor="White" />
                <asp:Button ID="btnEnregistrerDepart" runat="server" Text="Enregistrer" Visible="false" OnClick="btnEnregistrerDepart_Click" BackColor="Blue" BorderStyle="Outset" Font-Bold="True" Font-Size="Medium" ForeColor="White" />
                <asp:Button ID="btnAnnulerDepart" runat="server" Text="Annuler" Visible="false" OnClick="btnAnnulerDepart_Click" BackColor="Red" BorderStyle="Outset" Font-Bold="True" Font-Size="Medium" ForeColor="White" />
            </td>
        </tr>
    </table>
       
</asp:Content>
