﻿<%@ Page
    Title="Tester requête"
    Language="C#"
    MasterPageFile="~/Page.Master"
    AutoEventWireup="true"
    CodeBehind="TesterRequete.aspx.cs"
    Inherits="FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Presentation.TesterRequete" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="principal" runat="server">
    <h1 aria-atomic="False">Requête test</h1>
    <asp:GridView
        ID="gvTestRequete"
        CssClass="cssGvTournees"
        runat="server"
        CellPadding="5"
        CellSpacing="10"
        EmptyDataText="Aucun résultat"
        AutoGenerateColumns="true">
        <AlternatingRowStyle BackColor="#CCFFCC" />
        <HeaderStyle BackColor="Black" ForeColor="White" />
    </asp:GridView>
</asp:Content>
