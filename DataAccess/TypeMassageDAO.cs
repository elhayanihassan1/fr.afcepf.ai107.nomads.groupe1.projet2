﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.DataAccess
{
    public class TypeMassageDAO : DAO
    {
        public DataTable Liste => GetDataTable("SELECT * FROM " + BDD.TypeMassage.TableName + ";");
    }
}
