﻿using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.DataAccess {
    public class GenreDAO :DAO {

        public DataTable Liste => GetDataTable("SELECT * FROM " + BDD.Genre.TableName + ";");

        public GenreEntity GetById(int idGenre) {

            DataTable dt = GetDataTable(@"SELECT * FROM " + BDD.Genre.TableName +
                " WHERE " + BDD.Genre.Fields.Id + " = " + idGenre + ";");

            if (dt.Rows.Count == 1) {
                return DataRowToEntity(dt.Rows[0]);
            } else {
                return null;
            }
        }

        private GenreEntity DataRowToEntity(DataRow dr) {
            return new GenreEntity {
                Id = (int)dr[BDD.Genre.Fields.Id],
                Libelle = (string)dr[BDD.Genre.Fields.Libelle],
                Abreviation = (string)dr[BDD.Genre.Fields.Abreviation]
            };
        }
    }
}
