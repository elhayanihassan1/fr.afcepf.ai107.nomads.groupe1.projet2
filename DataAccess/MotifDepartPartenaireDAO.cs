﻿using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.DataAccess
{
    public class MotifDepartPartenaireDAO : DAO
    {

        public DataTable Liste => GetDataTable("SELECT * FROM " + BDD.MotifDepartPartenaire.TableName + ";");

        public MotifDepartPartenaireEntity GetById(int idMotif)
        {

            DataTable dt = GetDataTable(@"
                SELECT * FROM " + BDD.MotifDepartPartenaire.TableName +
                " WHERE " + BDD.MotifDepartPartenaire.Fields.Id + " = " + idMotif + ";");

            if (dt.Rows.Count == 1)
            {
                return DataRowToEntity(dt.Rows[0]);
            }
            else
            {
                return null;
            }
        }

        private MotifDepartPartenaireEntity DataRowToEntity(DataRow dr)
        {
            return new MotifDepartPartenaireEntity
            {
                Id = (int)dr[BDD.MotifDepartPartenaire.Fields.Id],
                Libelle = (string)dr[BDD.MotifDepartPartenaire.Fields.Libelle]
            };
        }

    }
}
