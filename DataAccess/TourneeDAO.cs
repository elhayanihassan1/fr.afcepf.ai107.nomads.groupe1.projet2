﻿using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.DataAccess {
    public class TourneeDAO : DAO {

        public DataTable Tournees() {

            return GetDataTable(
                "SELECT * FROM " +
                    BDD.Tournee.TableName + ", " +
                    BDD.Partenaire.TableName + ", " +
                    BDD.JourSemaine.TableName + ", " +
                    BDD.TypeIntervention.TableName + ", " +
                    BDD.Competence.TableName +
                " WHERE " + BDD.Tournee.Fields.PartenaireId + " = " + BDD.Partenaire.Fields.Id +
                " AND " + BDD.Tournee.Fields.JourSemaineId + " = " + BDD.JourSemaine.Fields.Id +
                " AND " + BDD.Tournee.Fields.TypeInterventionId + " = " + BDD.TypeIntervention.Fields.Id +
                " AND " + BDD.Tournee.Fields.CompetenceId + " = " + BDD.Competence.Fields.Id +
                " ORDER BY " + BDD.Partenaire.Fields.Nom + ";");
        }

        public ActionEntity GetAction(int idAction) {
            DataTable dt = GetDataTable(
                "SELECT * FROM " + BDD.Action.TableName +
               " WHERE " + BDD.Action.Fields.Id + " = " + idAction);

            if (dt.Rows.Count == 1) {
                ActionEntity action = new ActionEntity();
                action.Id = idAction;
                action.TourneeId = (int)dt.Rows[0][BDD.Action.Fields.TourneeId];
                action.Debut = (DateTime)dt.Rows[0][BDD.Action.Fields.Debut];
                action.Fin = (DateTime)dt.Rows[0][BDD.Action.Fields.Fin];

                if (dt.Rows[0][BDD.Action.Fields.DateSatisfaction] != DBNull.Value) {
                    action.DateSatisfaction = (DateTime?)dt.Rows[0][BDD.Action.Fields.DateSatisfaction];
                }
                if (dt.Rows[0][BDD.Action.Fields.SatisfactionId] != DBNull.Value) {
                    action.SatisfactionId = (int?)dt.Rows[0][BDD.Action.Fields.SatisfactionId];
                }
                if (dt.Rows[0][BDD.Action.Fields.DateAnnulation] != DBNull.Value) {
                    action.DateAnnulation = (DateTime?)dt.Rows[0][BDD.Action.Fields.DateAnnulation];
                }
                if (dt.Rows[0][BDD.Action.Fields.MotifAnnulationActionId] != DBNull.Value) {
                    action.MotifAnnulationActionId = (int?)dt.Rows[0][BDD.Action.Fields.MotifAnnulationActionId];
                }

                return action;
            } else {
                return null;
            }
        }

        public DataTable TourneesPartenaire(int idPartenaire) {

            return GetDataTable(
                "SELECT * FROM " +
                    BDD.Tournee.TableName + ", " +
                    BDD.Partenaire.TableName + ", " +
                    BDD.JourSemaine.TableName + ", " +
                    BDD.TypeIntervention.TableName + ", " +
                    BDD.Competence.TableName +
                " WHERE " + BDD.Tournee.Fields.PartenaireId + " = " + BDD.Partenaire.Fields.Id +
                " AND " + BDD.Tournee.Fields.JourSemaineId + " = " + BDD.JourSemaine.Fields.Id +
                " AND " + BDD.Tournee.Fields.TypeInterventionId + " = " + BDD.TypeIntervention.Fields.Id +
                " AND " + BDD.Tournee.Fields.CompetenceId + " = " + BDD.Competence.Fields.Id +
                " AND " + BDD.Tournee.Fields.PartenaireId + " = " + idPartenaire +
                " ORDER BY " + BDD.Partenaire.Fields.Nom + ";");
        }

        public void SupprimerAction(int idAction) {
            MySqlCommand cmd = GetCommandeSql(
                "DELETE FROM " + BDD.Action.TableName +
                " WHERE " + BDD.Action.Fields.Id + " = @" + BDD.Action.Fields.Id + ";");

            cmd.Parameters.Add(new MySqlParameter("@" + BDD.Action.Fields.Id, idAction));

            try {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            } catch (Exception exc) {
                throw exc;
            } finally {
                cmd.Connection.Close();
            }
        }

        public DataTable GetActions(int idTournee) => GetDataTable(
            "SELECT * FROM " +
                BDD.Action.TableName + ", " +
                BDD.Tournee.TableName +
            " WHERE " + BDD.Action.Fields.TourneeId + " = " + BDD.Tournee.Fields.Id +
            " AND " + BDD.Action.Fields.TourneeId + " = " + idTournee +
            " ORDER BY " + BDD.Action.Fields.Debut + ";");

        public bool TourneeExiste(int idTournee) => ValueExists(BDD.Tournee.TableName, BDD.Tournee.Fields.Id, idTournee);

        public bool ActionExiste(int idTournee, DateTime debut, DateTime fin) {
            return ValuesExist(BDD.Action.TableName,
                new string[] {
                    BDD.Action.Fields.TourneeId,
                    BDD.Action.Fields.Debut,
                    BDD.Action.Fields.Fin },
                new object[] { idTournee, debut, fin });
        }

        public TourneeEntity GetById(int idTournee) {

            DataTable dt = GetDataTable(
                "SELECT * FROM " + BDD.Tournee.TableName +
                " WHERE " + BDD.Tournee.Fields.Id + " = " + idTournee);

            if (dt.Rows.Count == 1) {
                return DataRowToEntity(dt.Rows[0]);
            } else {
                return null;
            }
        }

        public void DeleteTournee(int idTournee) {

            DeleteActions(idTournee);

            MySqlCommand cmd = GetCommandeSql(
                "DELETE FROM " + BDD.Tournee.TableName +
                " WHERE " + BDD.Tournee.Fields.Id + " = @" + BDD.Tournee.Fields.Id + ";");

            cmd.Parameters.Add(new MySqlParameter("@" + BDD.Tournee.Fields.Id, idTournee));

            try {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            } catch (Exception exc) {
                throw exc;
            } finally {
                cmd.Connection.Close();
            }
        }

        public void DeleteActions(int idTournee) {
            MySqlCommand cmd = GetCommandeSql(
                "DELETE FROM " + BDD.Action.TableName +
                " WHERE " + BDD.Action.Fields.TourneeId + " = @" + BDD.Action.Fields.TourneeId + ";");

            cmd.Parameters.Add(new MySqlParameter("@" + BDD.Action.Fields.TourneeId, idTournee));

            try {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            } catch (Exception exc) {
                throw exc;
            } finally {
                cmd.Connection.Close();
            }
        }

        public void UpdateTournee(TourneeEntity tournee) => UpdateTable(
            BDD.Tournee.TableName, tournee.Id, BDD.Tournee.AllFieldsExceptId, tournee.AllValuesExceptId);

        public void InsertTournee(TourneeEntity tournee) {

            tournee.Id = InsertInto(
                BDD.Tournee.TableName, BDD.Tournee.AllFieldsExceptId, tournee.AllValuesExceptId);

            CreateActions(tournee);
        }

        public int AddAction(ActionEntity action) => InsertInto(
            BDD.Action.TableName, BDD.Action.AllFieldsExceptId, action.AllValuesExceptId);

        private void CreateActions(TourneeEntity tournee) {
            if (tournee.DateFin != null) {
                int jourSemaine = new JourSemDAO().GetById(tournee.JourSemaineId).CsharpValue;
                int daysToAdd = (jourSemaine - (int)DateTime.Today.AddDays(1).DayOfWeek + 7) % 7;
                DateTime nextAction = DateTime.Today.AddDays(daysToAdd + 1);

                while (nextAction <= tournee.DateFin) {
                    InsertInto(BDD.Action.TableName, BDD.Action.AllFieldsExceptId, new ActionEntity() {
                        TourneeId = tournee.Id,
                        Debut = nextAction + tournee.HeureDebut,
                        Fin = nextAction + tournee.HeureDebut + tournee.Duree
                    }.AllValuesExceptId); nextAction = nextAction.AddDays(7);
                }
            }
        }

        private TourneeEntity DataRowToEntity(DataRow dr) {
            TourneeEntity tournee = new TourneeEntity {
                Id = (int)dr[BDD.Tournee.Fields.Id],
                PartenaireId = (int)dr[BDD.Tournee.Fields.PartenaireId],
                Nom = (string)dr[BDD.Tournee.Fields.Nom],
                JourSemaineId = (int)dr[BDD.Tournee.Fields.JourSemaineId],
                HeureDebut = (TimeSpan)dr[BDD.Tournee.Fields.HeureDebut],
                Duree = (TimeSpan)dr[BDD.Tournee.Fields.Duree],
                MinBinomes = (int)dr[BDD.Tournee.Fields.MinBinomes],
                MaxBinomes = (int)dr[BDD.Tournee.Fields.MaxBinomes],
                CoutMads = (int)dr[BDD.Tournee.Fields.CoutMads],
                DateCreation = (DateTime)dr[BDD.Tournee.Fields.DateCreation]
            };
            if (dr[BDD.Tournee.Fields.DateFin] != DBNull.Value) {
                tournee.DateFin = (DateTime?)dr[BDD.Tournee.Fields.DateFin];
            }
            if (dr[BDD.Tournee.Fields.DateAnnulation] != DBNull.Value) {
                tournee.DateAnnulation = (DateTime?)dr[BDD.Tournee.Fields.DateAnnulation];
            }
            if (dr[BDD.Tournee.Fields.MotifAnnulationTourneeId] != DBNull.Value) {
                tournee.MotifAnnulationTourneeId = (int?)dr[BDD.Tournee.Fields.MotifAnnulationTourneeId];
            }
            return tournee;
        }
    }
}
