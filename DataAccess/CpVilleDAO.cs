﻿using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.DataAccess {
    public class CpVilleDAO : DAO {

        public DataTable Liste => GetDataTable("SELECT * FROM " + BDD.CpVille.TableName + ";");

        public CpVilleEntity GetById(int idCpVille) {

            DataTable dt = GetDataTable(@"
                SELECT * FROM " + BDD.CpVille.TableName + 
                " WHERE " + BDD.CpVille.Fields.Id + " = " + idCpVille + ";");

            if (dt.Rows.Count == 1) {
                return DataRowToEntity(dt.Rows[0]);
            } else {
                return null;
            }
        }

        private CpVilleEntity DataRowToEntity(DataRow dr) {
            return new CpVilleEntity {
                Id = (int)dr[BDD.CpVille.Fields.Id],
                CodePostal = (string)dr[BDD.CpVille.Fields.CodePostal],
                Ville = (string)dr[BDD.CpVille.Fields.Ville]
            };
        }
    }
}
