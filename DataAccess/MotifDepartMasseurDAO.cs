﻿using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.DataAccess
{
    public class MotifDepartMasseurDAO : DAO
    {
        public DataTable ListeDepart => GetDataTable("SELECT * FROM " + BDD.MotifDepartMasseur.TableName + ";");

        public MotifDepartMasseurEntity GetById(int idMotif)
        {

            DataTable dt = GetDataTable(@"
                SELECT * FROM " + BDD.MotifDepartMasseur.TableName +
                " WHERE " + BDD.MotifDepartMasseur.Fields.Id + " = " + idMotif + ";");

            if (dt.Rows.Count == 1)
            {
                return DataRowToEntity(dt.Rows[0]);
            }
            else
            {
                return null;
            }
        }

        private MotifDepartMasseurEntity DataRowToEntity(DataRow dr)
        {
            return new MotifDepartMasseurEntity
            {
                Id = (int)dr[BDD.MotifDepartMasseur.Fields.Id],
                Libelle = (string)dr[BDD.MotifDepartMasseur.Fields.Libelle]
            };
        }
        
    }

}
