﻿using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities;
using System.Data;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.DataAccess {
    public class CompetenceDAO : DAO {

        public DataTable Liste(int idTypeIntervention) => GetDataTable(
            "SELECT * FROM " + BDD.Competence.TableName +
            " WHERE " + BDD.Competence.Fields.TypeInterventionId + " = " + idTypeIntervention +
            " ORDER BY " + BDD.Competence.Fields.Ordre + ";");

        public DataTable Liste(int idTypeIntervention, int idMasseur) => GetDataTable(
            "SELECT * FROM " + BDD.Competence.TableName +
            " WHERE " + BDD.Competence.Fields.TypeInterventionId + " = " + idTypeIntervention +
            " ORDER BY " + BDD.Competence.Fields.Ordre + ";");

        public CompetenceEntity GetById(int idCompetence) {

            DataTable dt = GetDataTable(
                "SELECT * FROM " + BDD.Competence.TableName +
                " WHERE " + BDD.Competence.Fields.Id + " = " + idCompetence + ";");

            if (dt.Rows.Count == 1) {
                return DataRowToEntity(dt.Rows[0]);
            } else {
                return null;
            }
        }

        private CompetenceEntity DataRowToEntity(DataRow dr) {
            return new CompetenceEntity {
                Id = (int)dr[BDD.Competence.Fields.Id],
                TypeInterventionId = (int)dr[BDD.Competence.Fields.TypeInterventionId],
                Ordre = (int)dr[BDD.Competence.Fields.Ordre],
                Libelle = (string)dr[BDD.Competence.Fields.Libelle]
            };
        }
    }
}
