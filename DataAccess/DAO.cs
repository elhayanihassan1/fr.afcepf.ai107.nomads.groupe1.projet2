﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.DataAccess {
    public class DAO {
        private const string connectionString = "Server=localhost;Database=nomads;Uid=root;Pwd=root;";

        public MySqlCommand GetCommandeSql(string sql) {
            return new MySqlCommand(sql, new MySqlConnection(connectionString));
        }

        protected DataTable GetDataTable(string sql) {
            DataTable result = new DataTable();
            new MySqlDataAdapter(GetCommandeSql(sql)).Fill(result);
            return result;
        }

        protected DataTable GetDataTable(string tableName, string orderField) {
            DataTable result = new DataTable();
            new MySqlDataAdapter(GetCommandeSql(
                "SELECT * FROM " + tableName + 
                " ORDER BY " + orderField + ";")).Fill(result);
            return result;
        }

        public int InsertInto(string tableName, string[] fields, object[] values) {

            MySqlCommand cmd = GetCommandeSql(@"
                INSERT INTO " + tableName + @" (" + string.Join(", ", fields) + ") " + @"
                VALUES(@" + string.Join(", @", fields) + ");" + "SELECT LAST_INSERT_ID();");

            for (int i = 0; i < fields.Length; i++) {
                cmd.Parameters.Add(new MySqlParameter("@" + fields[i], values[i]));
            }

            try {
                cmd.Connection.Open();
                return Convert.ToInt32(cmd.ExecuteScalar());
            } catch (Exception exc) {
                throw exc;
            } finally {
                cmd.Connection.Close();
            }
        }

        public void UpdateTable(string tableName, int idToUpdate, string[] fieldsToUpdate, object[] valuesToUpdate) {

            /*
             Procédure de mise à jour d'une table de la base.
             Règles de fonctionnement de la fonction : 
                1 - toutes les clés primaires de nos tables sont nommées de la forme nom_table_id,
                    en incrément auto => on n'update donc jamais l'id, donc on ne passe jamais l'id
                    dans les tableaux fieldsToUpdate et valuesToUpdate. L'id est passé en argument idToUpdate
                2 - les tableaux fieldsToUpdate et valuesToUpdate doivent avoir le même nombre d'éléments, 
                    et dans le même ordre : au énième élément de fieldsToUpdate doit correspondre le
                    énième élément de valuesToUpdate
             */

            MySqlCommand cmd = new MySqlCommand {
                Connection = new MySqlConnection(connectionString)
            };

            // Création des paramètres clé / valeur pour éviter les insertions SQL : les paramètres sont nommés @nom_du_champ
            cmd.Parameters.Add(new MySqlParameter("@" + tableName + "_id", idToUpdate));
            for (int i = 0; i < valuesToUpdate.Length; i++) {
                cmd.Parameters.Add(new MySqlParameter("@" + fieldsToUpdate[i], valuesToUpdate[i]));
            }

            // Préparation de la requête : chaque champ est associé au paramètre correspondant (créé au dessus) => nom_du_champ = @nom_du_champ
            for (int i = 0; i < fieldsToUpdate.Length; i++) {
                fieldsToUpdate[i] = fieldsToUpdate[i] + " = @" + fieldsToUpdate[i];
            }
            
            // Ecriture de la requête définitive
            cmd.CommandText = 
                "UPDATE " + tableName + 
                " SET " + string.Join(", ", fieldsToUpdate) + 
                " WHERE " + tableName + "_id = @" + tableName + "_id;";

            // Exécution de la requête
            try {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            } catch (Exception exc) {
                throw exc;
            } finally {
                cmd.Connection.Close();
            }
        }

        public void DeleteFrom(string tableName, int idToDelete) {
            MySqlCommand cmd = GetCommandeSql(
                "DELETE FROM " + tableName +
                " WHERE " + tableName + "_id = @" + tableName + "_id;");

            cmd.Parameters.Add(new MySqlParameter("@" + tableName + "_id", idToDelete));

            try {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            } catch (Exception exc) {
                throw exc;
            } finally {
                cmd.Connection.Close();
            }
        }

        protected bool ValueExists(string tableName, string field, object value) {

            MySqlCommand cmd = GetCommandeSql(
                "SELECT EXISTS(SELECT " + field + " FROM " + tableName + " WHERE " + field + " = " + value + ");");

            try {
                cmd.Connection.Open();
                return Convert.ToInt32(cmd.ExecuteScalar()) == 1;
            } catch (Exception exc) {
                throw exc;
            } finally {
                cmd.Connection.Close();
            }

        }

        protected bool ValuesExist(string tableName, string[] fields, object[] values) {

            MySqlCommand cmd = new MySqlCommand {
                Connection = new MySqlConnection(connectionString)
            };

            for (int i = 0; i < values.Length; i++) {
                cmd.Parameters.Add(new MySqlParameter("@" + fields[i], values[i]));
            }

            for (int i = 0; i < fields.Length; i++) {
                fields[i] = fields[i] + " = @" + fields[i];
            }

            cmd.CommandText = 
               "SELECT EXISTS(SELECT " + string.Join(", ", fields) +
               " FROM " + tableName +
               " WHERE " + string.Join(" AND ", fields) + ");";

            try {
                cmd.Connection.Open();
                return Convert.ToInt32(cmd.ExecuteScalar()) == 1;
            } catch (Exception exc) {
                throw exc;
            } finally {
                cmd.Connection.Close();
            }

        }
    }
}