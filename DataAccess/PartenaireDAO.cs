﻿using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.DataAccess {
    public class PartenaireDAO : DAO {
        #region BARS ET RESTAURANTS

        public DataTable GetListeBarEtRestaurant() {
            return GetDataTable(
                @"SELECT DISTINCT  " +
                    BDD.Partenaire.Fields.Id + ", " +
                    BDD.Partenaire.Fields.Nom + ", " +
                    BDD.Partenaire.Fields.Telephone + ", " +
                    BDD.Partenaire.Fields.Mail + ", " +
                    BDD.Partenaire.Fields.AdressePostale + ", " +
                    BDD.Partenaire.Fields.NomReferent + ", " +
                    BDD.Partenaire.Fields.PrenomReferent + ", " +
                    BDD.Partenaire.Fields.CpVilleId + ", " +
                    BDD.Partenaire.Fields.DateCreation + "  " +
                @"FROM " +
                    BDD.Partenaire.TableName + ", " +
                    BDD.EvolutionPartenaire.TableName + ", " +
                    BDD.TypePartenaire.TableName + "  " +
                @"WHERE " +
                    BDD.Partenaire.Fields.Id + " = " +
                    BDD.EvolutionPartenaire.Fields.PartenaireId + "   " +
                @"AND " +
                    BDD.EvolutionPartenaire.Fields.TypePartenaireId + " = " +
                    1 + ";");
        }






        #endregion

        #region ENTREPRISE

        public DataTable GetListeEntreprise() {
            return GetDataTable(
                @"SELECT DISTINCT  " +
                    BDD.Partenaire.Fields.Id + ", " +
                    BDD.Partenaire.Fields.Nom + ", " +
                    BDD.Partenaire.Fields.Telephone + ", " +
                    BDD.Partenaire.Fields.Mail + ", " +
                    BDD.Partenaire.Fields.AdressePostale + ", " +
                    BDD.Partenaire.Fields.NomReferent + ", " +
                    BDD.Partenaire.Fields.PrenomReferent + ", " +
                    BDD.Partenaire.Fields.CpVilleId + ", " +
                    BDD.Partenaire.Fields.DateCreation + "  " +
                @"FROM " +
                    BDD.Partenaire.TableName + ", " +
                    BDD.EvolutionPartenaire.TableName + ", " +
                    BDD.TypePartenaire.TableName + "  " +
                @"WHERE " +
                    BDD.Partenaire.Fields.Id + " = " +
                    BDD.EvolutionPartenaire.Fields.PartenaireId + "   " +
                @"AND " +
                    BDD.EvolutionPartenaire.Fields.TypePartenaireId + " = " +
                    2 + ";");
        }



        #endregion


        #region Type Partanaire
        public DataTable GetListeTypePartenaire() {
            return GetDataTable(

                @" Select *  
                 From " + BDD.TypePartenaire.TableName + " ; "
                );
        }


        #endregion

        #region Partenaire

        public DataTable GetListePartenaire() {
            return GetDataTable(

                @" Select *  
                 From " + BDD.Partenaire.TableName + " ; "
                );
        }

        #endregion

        #region Détails Partenaire
        public PartenaireEntity GetById(int idPartenaire) {
            DataTable dt = GetDataTable(@"
                SELECT * 
                FROM " + BDD.Partenaire.TableName + "    " +
                "WHERE " + BDD.Partenaire.Fields.Id + " = " + idPartenaire);

            if (dt.Rows.Count == 1) {
                return DataRowToEntity(dt.Rows[0]);
            } else {
                return null;
            }
        }

        private PartenaireEntity DataRowToEntity(DataRow dr) {
            return new PartenaireEntity {
                Id = (int)dr[BDD.Partenaire.Fields.Id],
                Nom = (string)dr[BDD.Partenaire.Fields.Nom],
                Telephone = (string)dr[BDD.Partenaire.Fields.Telephone],
                Mail = (string)dr[BDD.Partenaire.Fields.Mail],
                AdressePostale = (string)dr[BDD.Partenaire.Fields.AdressePostale],
                CpVilleId = (int)dr[BDD.Partenaire.Fields.CpVilleId],
                NomReferent = (string)dr[BDD.Partenaire.Fields.Nom],
                PrenomReferent = (string)dr[BDD.Partenaire.Fields.PrenomReferent],
                DateCreation = (DateTime)dr[BDD.Partenaire.Fields.DateCreation]
            };
        }


        #endregion

        #region Ajout dans la Table evolution partenaire 
        public void InsertEvolutionPartenaire(EvolutionPartenaireEntity evolution) {
            IDbCommand cmd = GetCommandeSql(@"INSERT INTO   " + BDD.EvolutionPartenaire.TableName + "(" +

                   BDD.EvolutionPartenaire.Fields.PartenaireId + ", " +
                   BDD.EvolutionPartenaire.Fields.TypePartenaireId + ", " +
                   BDD.EvolutionPartenaire.Fields.DateEvolution + ") " +

                   @"VALUES 
                    (@idPartenaire, @idTypePartenaire, @dateEvolution)");

            cmd.Parameters.Add(new MySqlParameter("@idPartenaire", evolution.PartenaireId));
            cmd.Parameters.Add(new MySqlParameter("@idTypePartenaire", evolution.TypePartenaireId));
            cmd.Parameters.Add(new MySqlParameter("@dateEvolution", evolution.DateEvolution));

            try {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            } catch (Exception exc) {
                throw exc;
            } finally {
                cmd.Connection.Close();
            }
        }

        #endregion

        #region Ajout Partenaire

        public void Insert(PartenaireEntity partenaire) {
            IDbCommand cmd = GetCommandeSql(@"INSERT INTO  " + BDD.Partenaire.TableName + "(" +
                    BDD.Partenaire.Fields.Nom + ", " +
                    BDD.Partenaire.Fields.Telephone + ", " +
                    BDD.Partenaire.Fields.Mail + ", " +
                    BDD.Partenaire.Fields.AdressePostale + ", " +
                    BDD.Partenaire.Fields.NomReferent + ", " +
                    BDD.Partenaire.Fields.PrenomReferent + ", " +
                    BDD.Partenaire.Fields.CpVilleId + ", " +
                    BDD.Partenaire.Fields.DateCreation + ") " +
                    @"VALUES 
                    (@nom, @telephone, @email, @adressePostal,@nomReferent,@prenomReferent, @idCpVille,@dateInscription);
                    SELECT LAST_INSERT_ID();");

            cmd.Parameters.Add(new MySqlParameter("@nom", partenaire.Nom));
            cmd.Parameters.Add(new MySqlParameter("@telephone", partenaire.Telephone));
            cmd.Parameters.Add(new MySqlParameter("@email", partenaire.Mail));
            cmd.Parameters.Add(new MySqlParameter("@adressePostal", partenaire.AdressePostale));
            cmd.Parameters.Add(new MySqlParameter("@nomReferent", partenaire.NomReferent));
            cmd.Parameters.Add(new MySqlParameter("@prenomReferent", partenaire.PrenomReferent));
            cmd.Parameters.Add(new MySqlParameter("@idCpVille", partenaire.CpVilleId));
            cmd.Parameters.Add(new MySqlParameter("@dateInscription", partenaire.DateCreation));

            try {
                cmd.Connection.Open();
                partenaire.Id = Convert.ToInt32(cmd.ExecuteScalar());

            } catch (Exception exc) {
                throw exc;
            } finally {
                cmd.Connection.Close();
            }
        }
        #endregion

        #region Mettre à jour Partenaire

        public void UpdatePartenaire(PartenaireEntity partenaire) => UpdateTable(
            BDD.Partenaire.TableName, partenaire.Id, BDD.Partenaire.AllFieldsExceptId, partenaire.AllValuesExceptId);

        #endregion

        #region Supprimer Partenaire 

        public void DeletePartenaire(int idPartenaire) {

            MySqlCommand cmd = GetCommandeSql(@"DELETE FROM " + BDD.Partenaire.TableName +

                                               " WHERE " +
                                                 BDD.Partenaire.Fields.Id + "= @id;");

            cmd.Parameters.Add(new MySqlParameter("@id", idPartenaire));

            try {
                cmd.Connection.Open();

                cmd.ExecuteNonQuery();
            } catch (Exception exc) {
                throw exc;
            } finally {
                cmd.Connection.Close();
            }

        }

        #endregion

        #region Supprimer la ligne dans la table EVOLUTION PARTENAIRE qui correspondent à ce partenaire
        public void DeleteEvolutionPartenaire(int idPartenaire) {
            MySqlCommand cmd = GetCommandeSql(@"DELETE FROM " + BDD.EvolutionPartenaire.TableName +

                                              " WHERE " +
                                                BDD.EvolutionPartenaire.Fields.PartenaireId + "= @id;");

            cmd.Parameters.Add(new MySqlParameter("@id", idPartenaire));

            try {
                cmd.Connection.Open();

                cmd.ExecuteNonQuery();
            } catch (Exception exc) {
                throw exc;
            } finally {
                cmd.Connection.Close();
            }

        }

        #endregion

        #region  Rechercher un Partenaire 


        #region  Bars & Restaurants
        public DataTable RechercherBR(string nom, string nomRreferent, string prenomReferent) {
            DataTable result = new DataTable();

            MySqlCommand cmd = GetCommandeSql(@"SELECT DISTINCT 
                                          partenaire_id ,
                                          partenaire_nom , 
		                                  partenaire_telephone, 
                                          partenaire_mail,
                                          partenaire_adresse_postale,
                                          partenaire_cp_ville_id,
                                          partenaire_nom_referent,
                                          partenaire_prenom_referent,
                                          partenaire_date_creation
        
                                          FROM partenaire, evolution_partenaire,type_partenaire 
                                         
                                          WHERE partenaire_nom LIKE @nom_partenaire
                                            AND  partenaire_nom_referent LIKE @nom_referent
                                            AND  partenaire_prenom_referent LIKE @prenom_referent
                                            AND partenaire_id = evolution_partenaire_partenaire_id 
                                            AND evolution_partenaire_type_partenaire_id  =  1
                                          ORDER BY partenaire_nom");

            cmd.Parameters.Add(new MySqlParameter("@nom_partenaire", "%" + nom + "%"));
            cmd.Parameters.Add(new MySqlParameter("@nom_referent", "%" + nomRreferent + "%"));
            cmd.Parameters.Add(new MySqlParameter("@prenom_referent", "%" + prenomReferent + "%"));


            MySqlDataAdapter da = new MySqlDataAdapter();
            da.SelectCommand = cmd;

            // remplir la datatable :
            da.Fill(result);

            return result;
        }

        #endregion

        #region Entreprise

        public DataTable RechercherE(string nom, string nomRreferent, string prenomReferent) {
            DataTable result = new DataTable();

            MySqlCommand cmd = GetCommandeSql(@"SELECT DISTINCT 
                                          partenaire_id ,
                                          partenaire_nom , 
		                                  partenaire_telephone, 
                                          partenaire_mail,
                                          partenaire_adresse_postale,
                                          partenaire_cp_ville_id,
                                          partenaire_nom_referent,
                                          partenaire_prenom_referent,
                                          partenaire_date_creation

                                          FROM partenaire,evolution_partenaire,type_partenaire 
                                         
                                          WHERE partenaire_nom LIKE @nom_partenaire
                                            AND  partenaire_nom_referent LIKE @nom_referent
                                            AND  partenaire_prenom_referent LIKE @prenom_referent        
                                            AND partenaire_id = evolution_partenaire_partenaire_id 
                                            AND evolution_partenaire_type_partenaire_id  =  2
                                          ORDER BY partenaire_nom");

            cmd.Parameters.Add(new MySqlParameter("@nom_partenaire", "%" + nom + "%"));
            cmd.Parameters.Add(new MySqlParameter("@nom_referent", "%" + nomRreferent + "%"));
            cmd.Parameters.Add(new MySqlParameter("@prenom_referent", "%" + prenomReferent + "%"));


            MySqlDataAdapter da = new MySqlDataAdapter();
            da.SelectCommand = cmd;

            // remplir la datatable :
            da.Fill(result);

            return result;
        }

        #endregion

        #endregion

        #region  Depart Partenaire

        public DataTable GetDepartPartenaire(int idPartenaire) => GetDataTable(
             "SELECT * FROM " + BDD.DepartPartenaire.TableName + ", " + BDD.MotifDepartPartenaire.TableName +
             " WHERE " + BDD.DepartPartenaire.Fields.PartenaireId + " = " + idPartenaire +
             " AND " + BDD.DepartPartenaire.Fields.MotifDepartPartenaireId + " = " + BDD.MotifDepartPartenaire.Fields.Id + ";");

        public void InsertDepartPartenaire(DepartPartenaireEntity departPartenaire) => departPartenaire.Id = InsertInto(
            BDD.DepartPartenaire.TableName, BDD.DepartPartenaire.AllFieldsExceptId, departPartenaire.AllValuesExceptId);

        public void DeleteDepartPartenaire(int idDepart) {
            MySqlCommand cmd = GetCommandeSql(
              "DELETE FROM " + BDD.DepartPartenaire.TableName +
              " WHERE " + BDD.DepartPartenaire.Fields.Id + " = @" + BDD.DepartPartenaire.Fields.Id + ";");

            cmd.Parameters.Add(new MySqlParameter("@" + BDD.DepartPartenaire.Fields.Id, idDepart));

            try {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            } catch (Exception exc) {
                throw exc;
            } finally {
                cmd.Connection.Close();
            }
        }


        #endregion
    }
}
