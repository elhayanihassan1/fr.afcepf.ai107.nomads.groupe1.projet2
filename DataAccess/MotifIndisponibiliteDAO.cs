﻿using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.DataAccess {
    public class MotifIndisponibiliteDAO :DAO {
        public DataTable Liste => GetDataTable("SELECT * FROM " + BDD.MotifIndisponibilite.TableName + ";");

        public MotifIndisponibiliteEntity GetById(int idMotif) {

            DataTable dt = GetDataTable(@"
                SELECT * FROM " + BDD.MotifIndisponibilite.TableName +
                " WHERE " + BDD.MotifIndisponibilite.Fields.Id + " = " + idMotif + ";");

            if (dt.Rows.Count == 1) {
                return DataRowToEntity(dt.Rows[0]);
            } else {
                return null;
            }
        }

        public void AjouterIndisponibilite(ActionEntity action, int idMasseur) {
            IndisponibiliteEntity indisponibilite = new IndisponibiliteEntity();
            indisponibilite.Utilisateur = false;
            indisponibilite.MasseurId = idMasseur;
            indisponibilite.Debut = action.Debut;
            indisponibilite.Fin = action.Fin;
            InsertInto(BDD.Indisponibilite.TableName, BDD.Indisponibilite.AllFieldsExceptId, indisponibilite.AllValuesExceptId);
        }

        private MotifIndisponibiliteEntity DataRowToEntity(DataRow dr) {
            return new MotifIndisponibiliteEntity {
                Id = (int)dr[BDD.MotifIndisponibilite.Fields.Id],
                Libelle = (string)dr[BDD.MotifIndisponibilite.Fields.Libelle]
            };
        }
    }
}
