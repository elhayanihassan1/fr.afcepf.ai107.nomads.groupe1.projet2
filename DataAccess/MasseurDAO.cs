﻿using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.DataAccess {

    public class MasseurDAO : DAO {

        public DataTable Liste() => GetDataTable(
            "SELECT * FROM " + BDD.Masseur.TableName + ", " + BDD.Genre.TableName +
            " WHERE " + BDD.Masseur.Fields.GenreId + " = " + BDD.Genre.Fields.Id +
            " ORDER BY " + BDD.Masseur.Fields.Nom + ";");

        public DataTable MasseursDispos(ActionEntity action, TourneeEntity tournee, PartenaireEntity partenaire) => GetDataTable(
            "SELECT * FROM " + BDD.Masseur.TableName + 
            " WHERE " + BDD.Masseur.Fields.Id + " NOT IN(" + 
                "SELECT " + BDD.AffectationAction.Fields.MasseurId + 
                " FROM " + BDD.AffectationAction.TableName + 
                " WHERE " + BDD.AffectationAction.Fields.ActionId + " = " + action.Id + ")" +
            " ORDER BY " + BDD.Masseur.Fields.Nom + ";");

        public DataTable MasseursAffectes(ActionEntity action) => GetDataTable(
            "SELECT * FROM " + BDD.Masseur.TableName + ", " + BDD.AffectationAction.TableName +
            " WHERE " + BDD.Masseur.Fields.Id + " = " + BDD.AffectationAction.Fields.MasseurId +
            " AND " + BDD.AffectationAction.Fields.ActionId + " = " + action.Id +
            " ORDER BY " + BDD.Masseur.Fields.Nom + ";");

        public MasseurEntity GetById(int idMasseur) {

            DataTable dt = GetDataTable(@"
                SELECT * FROM " + BDD.Masseur.TableName +
                " WHERE " + BDD.Masseur.Fields.Id + " = " + idMasseur);

            if (dt.Rows.Count == 1) {
                return DataRowToEntity(dt.Rows[0]);
            } else {
                return null;
            }
        }

        public bool MasseurExiste(int idMasseur) => ValueExists(BDD.Masseur.TableName, BDD.Masseur.Fields.Id, idMasseur);

        public void InscrireMasseur(AffectationActionEntity affectationAction) => InsertInto(
            BDD.AffectationAction.TableName, BDD.AffectationAction.AllFieldsExceptId, affectationAction.AllValuesExceptId);

        public void UpdateMasseur(MasseurEntity masseur) => UpdateTable(
            BDD.Masseur.TableName, masseur.Id, BDD.Masseur.AllFieldsExceptId, masseur.AllValuesExceptId);

        public DataTable GetIndisponibilites(int idMasseur, bool userOnly) => GetDataTable(
            "SELECT * FROM " + BDD.Indisponibilite.TableName + ", " + BDD.MotifIndisponibilite.TableName +
            " WHERE " + BDD.Indisponibilite.Fields.MasseurId + " = " + idMasseur +
            ((userOnly) ? " AND " + BDD.Indisponibilite.Fields.Utilisateur + " = 1" : "") +
            " AND " + BDD.Indisponibilite.Fields.MotifIndisponibiliteId + " = " + BDD.MotifIndisponibilite.Fields.Id + ";");

        public void DesistementAction(int idAction, int idMasseur) {
            MySqlCommand cmd = GetCommandeSql(
                "DELETE FROM " + BDD.AffectationAction.TableName +
                " WHERE " + BDD.AffectationAction.Fields.ActionId + " = @" + BDD.AffectationAction.Fields.ActionId +
                " AND " + BDD.AffectationAction.Fields.MasseurId + " = @" + BDD.AffectationAction.Fields.MasseurId + ";");

            cmd.Parameters.Add(new MySqlParameter("@" + BDD.AffectationAction.Fields.ActionId, idAction));
            cmd.Parameters.Add(new MySqlParameter("@" + BDD.AffectationAction.Fields.MasseurId, idMasseur));

            try {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            } catch (Exception exc) {
                throw exc;
            } finally {
                cmd.Connection.Close();
            };
        }

        public DataTable GetIndisponibilites(int idMasseur) => GetDataTable(
            "SELECT * FROM " + BDD.Indisponibilite.TableName + ", " + BDD.MotifIndisponibilite.TableName +
            " WHERE " + BDD.Indisponibilite.Fields.MasseurId + " = " + idMasseur +
            " AND " + BDD.Indisponibilite.Fields.MotifIndisponibiliteId + " = " + BDD.MotifIndisponibilite.Fields.Id + ";");

        public void DeleteMasseur(int idMasseur) => DeleteFrom(BDD.Masseur.TableName, idMasseur);

        public int InsertEvolutionMasseur(EvolutionMasseurEntity evolutionMasseurEntity) => InsertInto(
            BDD.EvolutionMasseur.TableName, BDD.EvolutionMasseur.AllFieldsExceptId, evolutionMasseurEntity.AllValuesExceptId);

        public void DeleteEvolutionMasseur(int idEvolutionMasseur) => DeleteFrom(BDD.EvolutionMasseur.TableName, idEvolutionMasseur);

        public DataTable ListePacks() => GetDataTable(BDD.PackMads.TableName, BDD.PackMads.Fields.QuantiteMads);

        public DataTable GetTransactions(int idMasseur) {
            return GetDataTable(
                "SELECT * FROM " +
                    BDD.TransactionPackMads.TableName + ", " +
                    BDD.PackMads.TableName + ", " +
                    BDD.Masseur.TableName +
                " WHERE " + BDD.TransactionPackMads.Fields.MasseurId + " = " + BDD.Masseur.Fields.Id +
                " AND " + BDD.TransactionPackMads.Fields.PackMadsId + " = " + BDD.PackMads.Fields.Id +
                " AND " + BDD.TransactionPackMads.Fields.MasseurId + " = " + idMasseur +
                " ORDER BY " + BDD.TransactionPackMads.Fields.DateTransaction + ";");
        }

        public int InsertTransactionPackMadsEntity(TransactionPackMadsEntity transactionPackMadsEntity) => transactionPackMadsEntity.Id = InsertInto(
            BDD.TransactionPackMads.TableName, BDD.TransactionPackMads.AllFieldsExceptId, transactionPackMadsEntity.AllValuesExceptId);
    
        public PackMadsEntity PackMads(int idPack) {
            DataTable dt = GetDataTable(
                "SELECT * FROM " + BDD.PackMads.TableName +
               " WHERE " + BDD.PackMads.Fields.Id + " = " + idPack);

            if (dt.Rows.Count == 1) {
                return new PackMadsEntity() {
                    Id = idPack,
                    QuantiteMads = (int)dt.Rows[0][BDD.PackMads.Fields.QuantiteMads], 
                    PrixUnitaire = (float)dt.Rows[0][BDD.PackMads.Fields.PrixUnitaire]
                };
            } else {
                return null;
            }
        }

        public int MadsAchetes(int idMasseur) {
            MySqlCommand cmd = GetCommandeSql(
                "SELECT SUM(" + BDD.PackMads.Fields.QuantiteMads + " * " + BDD.TransactionPackMads.Fields.Quantite + ")" +
                " FROM " + BDD.PackMads.TableName + ", " + BDD.TransactionPackMads.TableName + 
                " WHERE " + BDD.PackMads.Fields.Id + " = " + BDD.TransactionPackMads.Fields.PackMadsId + 
                " AND " + BDD.TransactionPackMads.Fields.MasseurId + " = " + idMasseur + ";");

            try {
                cmd.Connection.Open();
                return Convert.ToInt32(cmd.ExecuteScalar());
            } catch (Exception) {
                return 0;
            } finally {
                cmd.Connection.Close();
            }
        }

        public float CountMadsAchetes(int idMasseur) {
            MySqlCommand cmd = GetCommandeSql(
                "SELECT SUM(" + BDD.PackMads.Fields.PrixUnitaire + " * " + BDD.TransactionPackMads.Fields.Quantite + ")" +
                " FROM " + BDD.PackMads.TableName + ", " + BDD.TransactionPackMads.TableName +
                " WHERE " + BDD.PackMads.Fields.Id + " = " + BDD.TransactionPackMads.Fields.PackMadsId +
                " AND " + BDD.TransactionPackMads.Fields.MasseurId + " = " + idMasseur + ";");

            try {
                cmd.Connection.Open();
                return Convert.ToInt32(cmd.ExecuteScalar());
            } catch (Exception) {
                return 0;
            } finally {
                cmd.Connection.Close();
            }
        }

        public DataTable GetEvolutionsMasseur(int idMasseur) {
            return GetDataTable(
                "SELECT * FROM " +
                    BDD.EvolutionMasseur.TableName + ", " +
                    BDD.Masseur.TableName + ", " +
                    BDD.Competence.TableName + ", " +
                    BDD.TypeIntervention.TableName +
                " WHERE " + BDD.EvolutionMasseur.Fields.MasseurId + " = " + BDD.Masseur.Fields.Id +
                " AND " + BDD.EvolutionMasseur.Fields.CompetenceId + " = " + BDD.Competence.Fields.Id +
                " AND " + BDD.Competence.Fields.TypeInterventionId + " = " + BDD.TypeIntervention.Fields.Id +
                " AND " + BDD.EvolutionMasseur.Fields.MasseurId + " = " + idMasseur +
                " ORDER BY " + BDD.Competence.Fields.Ordre + ";");
        }

        public void InsertMasseur(MasseurEntity masseur) => masseur.Id = InsertInto(
            BDD.Masseur.TableName, BDD.Masseur.AllFieldsExceptId, masseur.AllValuesExceptId);

        public DataTable GetPreferences(int idMasseur) => GetDataTable(
            "SELECT * FROM " + BDD.PreferenceMasseur.TableName + ", " + BDD.JourSemaine.TableName +
            " WHERE " + BDD.PreferenceMasseur.Fields.MasseurId + " = " + idMasseur +
            " AND " + BDD.PreferenceMasseur.Fields.JourSemaineId + " = " + BDD.JourSemaine.Fields.Id +
            " ORDER BY " + BDD.JourSemaine.Fields.CsharpValue + ";");

        public void DeletePreference(int idPreference) => DeleteFrom(BDD.PreferenceMasseur.TableName, idPreference);

        public void InsertPreference(PreferenceMasseurEntity preference) => preference.Id = InsertInto(
                BDD.PreferenceMasseur.TableName, BDD.PreferenceMasseur.AllFieldsExceptId, preference.AllValuesExceptId);

        public void DeleteIndisponibilite(int idIndisponibilite) => DeleteFrom(BDD.Indisponibilite.TableName, idIndisponibilite);

        public void InsertIndisponibilite(IndisponibiliteEntity indisponibilite) => indisponibilite.Id = InsertInto(
                BDD.Indisponibilite.TableName, BDD.Indisponibilite.AllFieldsExceptId, indisponibilite.AllValuesExceptId);

        private MasseurEntity DataRowToEntity(DataRow dr) {
            return new MasseurEntity {
                Id = (int)dr[BDD.Masseur.Fields.Id],
                Nom = (string)dr[BDD.Masseur.Fields.Nom],
                Prenom = (string)dr[BDD.Masseur.Fields.Prenom],
                GenreId = (int)dr[BDD.Masseur.Fields.GenreId],
                Telephone = (string)dr[BDD.Masseur.Fields.Telephone],
                Email = (string)dr[BDD.Masseur.Fields.Email],
                AdressePostale = (string)dr[BDD.Masseur.Fields.AdressePostale],
                CpVilleId = (int)dr[BDD.Masseur.Fields.CpVilleId],
                DateNaissance = (DateTime)dr[BDD.Masseur.Fields.DateNaissance],
                DistanceMaxKm = (int)dr[BDD.Masseur.Fields.DistanceMaxKm],
                DateCreation = (DateTime)dr[BDD.Masseur.Fields.DateCreation]
            };
        }

        public void InsertDepart(DepartMasseurEntity depart) => depart.Id = InsertInto(
            BDD.DepartMasseur.TableName, BDD.DepartMasseur.AllFieldsExceptId, depart.AllValuesExceptId);

        public DataTable GetDepart(int idMasseur) => GetDataTable(
          "SELECT * FROM " + BDD.DepartMasseur.TableName + ", " + BDD.MotifDepartMasseur.TableName +
             " WHERE " + BDD.DepartMasseur.Fields.MasseurId + " = " + idMasseur +
             " AND " + BDD.DepartMasseur.Fields.MotifDepartMasseurId + " = " + BDD.MotifDepartMasseur.Fields.Id + ";");

        public void DeleteDepart(int idDepart) => DeleteFrom(BDD.DepartMasseur.TableName, idDepart);
    }
}