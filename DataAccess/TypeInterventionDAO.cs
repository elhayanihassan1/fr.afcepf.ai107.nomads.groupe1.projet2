﻿using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities;
using System.Data;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.DataAccess {
    public class TypeInterventionDAO : DAO {
        public DataTable Liste => GetDataTable(
            "SELECT * FROM " + BDD.TypeIntervention.TableName +
            " ORDER BY " + BDD.TypeIntervention.Fields.Libelle + ";");

        public TypeInterventionEntity GetById(int idTypeIntervention) {

            DataTable dt = GetDataTable(
                "SELECT * FROM " + BDD.TypeIntervention.TableName +
                " WHERE " + BDD.TypeIntervention.Fields.Id + " = " + idTypeIntervention + ";");

            if (dt.Rows.Count == 1) {
                return DataRowToEntity(dt.Rows[0]);
            } else {
                return null;
            }
        }

        private TypeInterventionEntity DataRowToEntity(DataRow dr) {
            return new TypeInterventionEntity {
                Id = (int)dr[BDD.TypeIntervention.Fields.Id],
                Libelle = (string)dr[BDD.TypeIntervention.Fields.Libelle]
            };
        }
    }
}
