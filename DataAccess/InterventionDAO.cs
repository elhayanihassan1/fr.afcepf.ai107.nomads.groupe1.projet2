﻿using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.DataAccess
{
    public class InterventionDAO : DAO
    {
        public DataTable GetListeTypeIntervention()
        {
            return GetDataTable(

                @" Select *  
                 From " + BDD.TypeIntervention.TableName + " ; "
                );
        }

        public void Insert(InterventionEntity intervention)
        {
            intervention.Id = InsertInto(
               BDD.Intervention.TableName, BDD.Intervention.AllFieldsExceptId, intervention.AllValuesExceptId);
        }

      


        #region List Intervention


        public DataTable GetListeIntervention()
        {
            return GetDataTable(
            @"SELECT * FROM " +
                BDD.Intervention.TableName + ", " +
                BDD.Partenaire.TableName + ", " +
                BDD.Competence.TableName + ", " +
                BDD.TypeIntervention.TableName +
            " WHERE " + BDD.Intervention.Fields.PartenaireId + " = " + BDD.Partenaire.Fields.Id +
            " AND " + BDD.Intervention.Fields.TypeInterventionId + " = " + BDD.TypeIntervention.Fields.Id +
             " AND " + BDD.Intervention.Fields.CompetenceId + " = " + BDD.Competence.Fields.Id +
            " ORDER BY " + BDD.Partenaire.Fields.Nom + ";");

        }


           public DataTable InterventionPartenaire(int idPartenaire) {
                return GetDataTable(
            @"SELECT * FROM " +
                BDD.Intervention.TableName + ", " +
                BDD.Partenaire.TableName + ", " +
                BDD.Competence.TableName + ", " +
                BDD.TypeIntervention.TableName +
            " WHERE " + BDD.Intervention.Fields.PartenaireId + " = " + BDD.Partenaire.Fields.Id +
            " AND " + BDD.Intervention.Fields.TypeInterventionId + " = " + BDD.TypeIntervention.Fields.Id +
            " AND " + BDD.Intervention.Fields.CompetenceId + " = " + BDD.Competence.Fields.Id +
            " AND " + BDD.Intervention.Fields.PartenaireId + " = " + idPartenaire +
            " ORDER BY " + BDD.Partenaire.Fields.Nom + ";");

            #endregion


        }

        #region Massage Intervention

        public DataTable GetMassage(int idIntervention)
        {

            return GetDataTable(
            "SELECT * FROM " + BDD.MassageIntervention.TableName + ", " + BDD.TypeMassage.TableName +
            " WHERE " + BDD.MassageIntervention.Fields.InterventionId + " = " + idIntervention +
            " AND " + BDD.MassageIntervention.Fields.TypeMassageId + " = " + BDD.TypeMassage.Fields.Id + ";");


        }


        public void InsertMassageIntervention(MassageInterventionEntity massageIntervention)
        {
            massageIntervention.Id = InsertInto(BDD.MassageIntervention.TableName,
           new string[] {
                    BDD.MassageIntervention.Fields.InterventionId,
                    BDD.MassageIntervention.Fields.TypeMassageId,
                    BDD.MassageIntervention.Fields.NombreMasseurs},
           new object[] {
                    massageIntervention.InterventionId,
                    massageIntervention.TypeMassageId,
                    massageIntervention.NombreMasseurs });
        }


        public void DeleteMassageIntervention(int idMassage)
        {
            MySqlCommand cmd = GetCommandeSql(
           "DELETE FROM " + BDD.MassageIntervention.TableName +
                " WHERE " + BDD.MassageIntervention.Fields.Id + " = @" + BDD.MassageIntervention.Fields.Id + ";");

            cmd.Parameters.Add(new MySqlParameter("@" + BDD.MassageIntervention.Fields.Id, idMassage));

            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
        }
        #endregion
    }
}
