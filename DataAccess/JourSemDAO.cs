﻿using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.DataAccess {
    public class JourSemDAO : DAO {
        public DataTable Liste => GetDataTable(BDD.JourSemaine.TableName, BDD.JourSemaine.Fields.Ordre);

        public JourSemaineEntity GetById(int idJour) {

            DataTable dt = GetDataTable(
                "SELECT * FROM " + BDD.JourSemaine.TableName +
                " WHERE " + BDD.JourSemaine.Fields.Id + " = " + idJour + ";");

            if (dt.Rows.Count == 1) {
                return DataRowToEntity(dt.Rows[0]);
            } else {
                return null;
            }
        }

        private JourSemaineEntity DataRowToEntity(DataRow dr) {
            return new JourSemaineEntity {
                Id = (int)dr[BDD.JourSemaine.Fields.Id],
                Ordre = (int)dr[BDD.JourSemaine.Fields.Ordre],
                MysqlValue = (int)dr[BDD.JourSemaine.Fields.MysqlValue],
                CsharpValue = (int)dr[BDD.JourSemaine.Fields.CsharpValue],
                Libelle = (string)dr[BDD.JourSemaine.Fields.Libelle]
            };
        }
    }
}
