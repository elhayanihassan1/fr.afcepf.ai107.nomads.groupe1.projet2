﻿using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.DataAccess;
using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities;
using System.Data;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Business {
    public class CompetenceBU {
        public DataTable Liste(int idTypeIntervention) => new CompetenceDAO().Liste(idTypeIntervention);

        public CompetenceEntity GetById(int idCompetence) => new CompetenceDAO().GetById(idCompetence);
    }
}
