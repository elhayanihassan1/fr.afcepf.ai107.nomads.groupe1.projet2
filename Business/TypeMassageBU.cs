﻿using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Business
{
    public class TypeMassageBU
    {
        public DataTable Liste => new TypeMassageDAO().Liste;
    }
}
