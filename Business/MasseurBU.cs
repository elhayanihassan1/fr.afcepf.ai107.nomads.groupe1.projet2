﻿using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.DataAccess;
using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities;
using System;
using System.Data;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Business {

    public class MasseurBU {

        public DataTable Liste() => new MasseurDAO().Liste();

        public MasseurEntity GetById(int idMasseur) => new MasseurDAO().GetById(idMasseur);

        public GenreEntity GetGenre(int idGenre) => new GenreDAO().GetById(idGenre);

        public bool MasseurExiste(int idMasseur) => new MasseurDAO().MasseurExiste(idMasseur);

        public CpVilleEntity GetCpVille(int idCpVille) => new CpVilleDAO().GetById(idCpVille);

        public void AjouterMasseur(MasseurEntity masseur) => new MasseurDAO().InsertMasseur(masseur);

        public void ModifierMasseur(MasseurEntity masseur) => new MasseurDAO().UpdateMasseur(masseur);

        public void SupprimerMasseur(int idMasseur) => new MasseurDAO().DeleteMasseur(idMasseur);

        public DataTable GetPreferences(int idMasseur) => new MasseurDAO().GetPreferences(idMasseur);

        public void AjouterPreference(PreferenceMasseurEntity preference) => new MasseurDAO().InsertPreference(preference);

        public void SupprimerPreference(int idPreference) => new MasseurDAO().DeletePreference(idPreference);

        public DataTable MasseursDispos(ActionEntity action, TourneeEntity tournee, PartenaireEntity partenaire) 
            => new MasseurDAO().MasseursDispos(action, tournee, partenaire);

        public DataTable MasseursAffectes(ActionEntity action) => new MasseurDAO().MasseursAffectes(action);

        public DataTable GetIndisponibilites(int idMasseur) => new MasseurDAO().GetIndisponibilites(idMasseur);

        public void InscrireMasseur(AffectationActionEntity affectationAction) => new MasseurDAO().InscrireMasseur(affectationAction);

        public DataTable GetIndisponibilites(int idMasseur, bool utilisateurSeulement) => 
            new MasseurDAO().GetIndisponibilites(idMasseur, utilisateurSeulement);

        public void AjouterIndisponibilite(IndisponibiliteEntity indisponibilite) => new MasseurDAO().InsertIndisponibilite(indisponibilite);

        public void SupprimerIndisponibilite(int idIndisponibilite) => new MasseurDAO().DeleteIndisponibilite(idIndisponibilite);

        public void DesistementAction(int idAction, int idMasseur) => new MasseurDAO().DesistementAction(idAction, idMasseur);

        public DataTable GetDepart(int idMasseur) => new MasseurDAO().GetDepart(idMasseur);

        public void AjouterDepart(DepartMasseurEntity departMasseur) => new MasseurDAO().InsertDepart(departMasseur);

        public void SupprimerDepart(int idDepart) => new MasseurDAO().DeleteDepart(idDepart);

        public DataTable GetEvolutionsMasseur(int idMasseur) => new MasseurDAO().GetEvolutionsMasseur(idMasseur);

        public void SupprimerEvolutionMasseur(int idEvolutionMasseur) => new MasseurDAO().DeleteEvolutionMasseur(idEvolutionMasseur);

        public int AjouterEvolutionMasseur(EvolutionMasseurEntity evolutionMasseurEntity) => new MasseurDAO().InsertEvolutionMasseur(evolutionMasseurEntity);

        public DataTable ListePacks() => new MasseurDAO().ListePacks();

        public DataTable GetTransactions(int idMasseur) => new MasseurDAO().GetTransactions(idMasseur);

        public PackMadsEntity PackMads(int idPack) => new MasseurDAO().PackMads(idPack);

        public int AjouterTransaction(TransactionPackMadsEntity transactionPackMadsEntity) => 
            new MasseurDAO().InsertTransactionPackMadsEntity(transactionPackMadsEntity);

        public int MadsAchetes(int idMasseur) => new MasseurDAO().MadsAchetes(idMasseur);

        public float CountMadsAchetes(int idMasseur) => new MasseurDAO().CountMadsAchetes(idMasseur);
    }
}
