﻿using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.DataAccess;
using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Business {

    public class TourneeBU {
        
        public DataTable Tournees() => new TourneeDAO().Tournees();

        public DataTable TourneesPartenaire(int idPartenaire) => new TourneeDAO().TourneesPartenaire(idPartenaire);

        public void AjouterTournee(TourneeEntity tournee) => new TourneeDAO().InsertTournee(tournee);

        public bool TourneeExiste(int idTournee) => new TourneeDAO().TourneeExiste(idTournee);

        public ActionEntity GetAction(int idAction) => new TourneeDAO().GetAction(idAction);

        public TourneeEntity GetById(int idTournee) => new TourneeDAO().GetById(idTournee);

        public void ModifierTournee(TourneeEntity tournee) => new TourneeDAO().UpdateTournee(tournee);

        public void SupprimerTournee(int idTournee) => new TourneeDAO().DeleteTournee(idTournee);

        public DataTable GetActions(int idTournee) => new TourneeDAO().GetActions(idTournee);

        public int AjouterAction(ActionEntity action) => new TourneeDAO().AddAction(action);

        public bool ActionExiste(int idTournee, DateTime debut, DateTime fin) => new TourneeDAO().ActionExiste(idTournee, debut, fin);

        public void SupprimerAction(int idAction) => new TourneeDAO().SupprimerAction(idAction);
    }
}
