﻿using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.DataAccess;
using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Business {

    public class PartenaireBU {

        #region BAR ET RESTAURANTS

        public DataTable ListBarRestaurants => new PartenaireDAO().GetListeBarEtRestaurant();

        #endregion

        #region ENTREPRISE

        public DataTable GetListEntreprise() => new PartenaireDAO().GetListeEntreprise();

        public PartenaireEntity GetById(int idPartenaire) => new PartenaireDAO().GetById(idPartenaire);

        #endregion

        #region Type Partenaire

        public DataTable GetListTypePartenaire() => new PartenaireDAO().GetListeTypePartenaire();

 
        #endregion

        #region Partenaire

        public DataTable GetListPartenaire() => new PartenaireDAO().GetListePartenaire();
        #endregion

        #region   Détails Partenaire

        public PartenaireEntity GetDetailPartenaire(int idPartenaire)
        {
            return new PartenaireDAO().GetById(idPartenaire);
        }

        #endregion


        #region Ajout Partenaire

        public void AjouterPartenaire(PartenaireEntity partenaire)
        {
            PartenaireDAO dao = new PartenaireDAO();
            dao.Insert(partenaire);
        }


        #endregion

        #region Ajout dans la Table evolution partenaire


        public void AjouterEvolutionPartenaire(EvolutionPartenaireEntity evolution)
        {
             new PartenaireDAO().InsertEvolutionPartenaire(evolution);
        }

     
        #endregion

        #region Mettre à Jour Partenaire

        public void ModifierPartenaire(PartenaireEntity partenaire)
        {
             new PartenaireDAO().UpdatePartenaire(partenaire);
        }

        #endregion

        #region Supprimer Partenaire

        public void SupprimerPartenaire(int idPartenaire)
        {
            new PartenaireDAO().DeletePartenaire(idPartenaire);
        }

  
        #endregion

        #region  Supprimer la ligne dans la table EVOLUTION PARTENAIRE qui correspondent à ce partenaire
        public void SupprimerEvolutionPartenaire(int idPartenaire)
        {
            new PartenaireDAO().DeleteEvolutionPartenaire(idPartenaire);
        }
        #endregion

        #region Rechercher un partenaire ( Bar & restaurants )
        public DataTable RecherchePartenaireBR(string nom, string nomRreferent, string prenomReferent)
        {
             

            return new PartenaireDAO().RechercherBR(nom, nomRreferent, prenomReferent);
        }

        #endregion

        #region  Rechercher un partenaire ( Entreprise )
        public DataTable RecherchePartenaireE(string nom, string nomRreferent, string prenomReferent)
        {
            return new PartenaireDAO().RechercherE(nom, nomRreferent, prenomReferent);
        }
        #endregion


        #region  Depart Partenaire

        public DataTable GetDepartPartenaire(int idPartenaire)
        {
             return new PartenaireDAO().GetDepartPartenaire(idPartenaire);
        }

        public void AjouterDepartPartenaire(DepartPartenaireEntity departPartenaire)
        {
            new PartenaireDAO().InsertDepartPartenaire(departPartenaire);
        }

        public void SupprimerDepartPartenaire(int idDepart)
        {
            new PartenaireDAO().DeleteDepartPartenaire(idDepart);
        }


        #endregion

    }
}
