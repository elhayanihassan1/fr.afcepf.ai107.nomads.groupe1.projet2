﻿using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.DataAccess;
using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Business {

    public class InterventionBU
    {



        #region  Type Intervention
    

        public DataTable GetListTypeIntervention() => new InterventionDAO().GetListeTypeIntervention();

  
        #endregion

        #region Ajout intervention
        public void AjouterIntervention(InterventionEntity intervention)
        {
            InterventionDAO dao = new InterventionDAO();
            dao.Insert(intervention);
        }

        #endregion

        #region Liste Intervention

        public DataTable ListIntervention()
        {
           return  new InterventionDAO().GetListeIntervention();
        }

        public DataTable InterventionPartenaire(int idPartenaire) => new InterventionDAO().InterventionPartenaire(idPartenaire);


        #endregion

        #region  Massage Intervention

        public object GetMassageIntervention(int idIntervention)
        {
            return new InterventionDAO().GetMassage(idIntervention);
        }

        public void AjouterMassageIntervention(MassageInterventionEntity massageIntervention)
        {
            new InterventionDAO().InsertMassageIntervention(massageIntervention);
        }

        public void SupprimerMassageIntervention(int idMassage)
        {
            new InterventionDAO().DeleteMassageIntervention(idMassage);
        }




        #endregion
    }
}
