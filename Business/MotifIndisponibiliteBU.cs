﻿using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.DataAccess;
using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Business {
    public class MotifIndisponibiliteBU {
        public DataTable Liste => new MotifIndisponibiliteDAO().Liste;

        public void AjouterIndisponibilite(ActionEntity action, int idMasseur) => new MotifIndisponibiliteDAO().AjouterIndisponibilite(action, idMasseur);
    }
}
