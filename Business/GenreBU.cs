﻿using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.DataAccess;
using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Business {
    public class GenreBU {
        public DataTable Liste => new GenreDAO().Liste;

        public GenreEntity GetById(int idGenre) {
            return new GenreDAO().GetById(idGenre);
        }
    }
}
