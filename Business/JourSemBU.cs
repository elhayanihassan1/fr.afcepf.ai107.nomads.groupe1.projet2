﻿using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.DataAccess;
using FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities;
using System.Data;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Business {
    public class JourSemBU {
        public DataTable Liste => new JourSemDAO().Liste;

        public JourSemaineEntity GetById(int jourSemaineId) => new JourSemDAO().GetById(jourSemaineId);
    }
}
