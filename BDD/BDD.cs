﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.BDD {
    public static class BDD {
        public static class Action {
            public static string TableName => "action";
            public static class Fields {
                public static string Id => TableName + "_id";
                public static string TourneeId => TableName + "_tournee_id";
                public static string Debut => TableName + "_debut";
                public static string Fin => TableName + "_fin";
                public static string DateSatisfaction => TableName + "_date_satisfaction";
                public static string SatisfactionId => TableName + "_satisfaction_id";
                public static string DateAnnulation => TableName + "_date_annulation";
                public static string MotifAnnulationActionId => TableName + "_motif_annulation_action_id";
            }
        }
        public static class AffectationAction {
            public static string TableName => "affectation_action";
            public static class Fields {
                public static string Id => TableName + "_id";
                public static string ActionId => TableName + "_action_id";
                public static string MasseurId => TableName + "_masseur_id";
                public static string DateAffectation => TableName + "_date_affectation";
                public static string GainsEuros => TableName + "_gains_euros";
                public static string NombreMassages => TableName + "_nombre_massages";
                public static string DateSatisfaction => TableName + "_date_satisfaction";
                public static string SatisfactionId => TableName + "_satisfaction_id";
                public static string DateDesistement => TableName + "_date_desistement";
                public static string MotifDesistementActionId => TableName + "_motif_desistement_action_id";
                public static string RemboursementMads => TableName + "_remboursement_mads";
            }
        }
        public static class Competence {
            public static string TableName => "competence";
            public static class Fields {
                public static string Id => TableName + "_id";
                public static string Libelle => TableName + "_libelle";
            }
        }
        public static class CpVille {
            public static string TableName => "cp_ville";
            public static class Fields {
                public static string Id => TableName + "_id";
                public static string CodePostal => TableName + "_code_postal";
                public static string Ville => TableName + "_ville";
            }
        }
        public static class DepartMasseur {
            public static string TableName => "depart_masseur";
            public static class Fields {
                public static string Id => TableName + "_id";
                public static string MasseurId => TableName + "_masseur_id";
                public static string MotifDepartMasseurId => TableName + "_motif_depart_masseur_id";
                public static string DateDepart => TableName + "_date_depart";
                public static string DateRetour => TableName + "_date_retour";
            }
        }
        public static class DepartPartenaire {
            public static string TableName => "depart_partenaire";
            public static class Fields {
                public static string Id => TableName + "_id";
                public static string PartenaireId => TableName + "_partenaire_id";
                public static string MotifDepartPartenaireId => TableName + "_motif_depart_partenaire_id";
                public static string DateDepart => TableName + "_date_depart";
                public static string DateRetour => TableName + "_date_retour";
            }
        }
        public static class EchelleRemboursement {
            public static string TableName => "echelle_remboursement";
            public static class Fields {
                public static string Id => TableName + "_id";
                public static string TourneeId => TableName + "_tournee_id";
                public static string HeuresRestantes => TableName + "_heures_restantes";
                public static string Pourcentage => TableName + "_pourcentage";
                public static string DateCreation => TableName + "_date_creation";
                public static string DateAnnulation => TableName + "_date_annulation";
                public static string MotifAnnulationEchelleRemboursementId => TableName + "_motif_annulation_echelle_remboursement_id";
            }
        }
        public static class EmpruntMateriel {
            public static string TableName => "emprunt_materiel";
            public static class Fields {
                public static string Id => TableName + "_id";
                public static string MaterielNomadsId => TableName + "_materiel_nomads_id";
                public static string InscriptionInterventionId => TableName + "_inscription_intervention_id";
                public static string DateEmprunt => TableName + "_date_emprunt";
                public static string DateRetour => TableName + "_date_retour";
            }
        }
        public static class EtatMateriel {
            public static string TableName => "etat_materiel";
            public static class Fields {
                public static string Id => TableName + "_id";
                public static string Libelle => TableName + "_libelle";
            }
        }
        public static class EvolutionMasseur {
            public static string TableName => "evolution_masseur";
            public static class Fields {
                public static string Id => TableName + "_id";
                public static string MasseurId => TableName + "_masseur_id";
                public static string CompetenceId => TableName + "_competence_id";
                public static string DateEvolution => TableName + "_date_evolution";
            }
        }
        public static class EvolutionMateriel {
            public static string TableName => "evolution_materiel";
            public static class Fields {
                public static string Id => TableName + "_id";
                public static string MaterielNomadsId => TableName + "_materiel_nomads_id";
                public static string EtatMaterielId => TableName + "_etat_materiel_id";
                public static string DateEvolution => TableName + "_date_evolution";
            }
        }
        public static class EvolutionPartenaire {
            public static string TableName => "evolution_partenaire";
            public static class Fields {
                public static string Id => TableName + "_id";
                public static string PartenaireId => TableName + "_partenaire_id";
                public static string TypePartenaireId => TableName + "_type_partenaire_id";
                public static string DateEvolution => TableName + "_date_evolution";
            }
        }
        public static class Formation {
            public static string TableName => "formation";
            public static class Fields {
                public static string Id => TableName + "_id";
                public static string Titre => TableName + "_titre";
                public static string Description => TableName + "_description";
                public static string CompetenceId => TableName + "_competence_id";
                public static string Duree => TableName + "_duree";
                public static string AdressePostale => TableName + "_adresse_postale";
                public static string CpVilleId => TableName + "_cp_ville_id";
                public static string MinMasseurs => TableName + "_min_masseurs";
                public static string MaxMasseurs => TableName + "_max_masseurs";
                public static string DateCreation => TableName + "_date_creation";
                public static string DateAnnulation => TableName + "_date_annulation";
                public static string DateAnnulationFormationId => TableName + "_date_annulation_formation_id";
            }
        }
        public static class Genre {
            public static string TableName => "genre";
            public static class Fields {
                public static string Id => TableName + "_id";
                public static string Libelle => TableName + "_libelle";
                public static string Abreviation => TableName + "_abreviation";
            }
        }
        public static class Indisponibilite {
            public static string TableName => "indisponibilite";
            public static class Fields {
                public static string Id => TableName + "_id";
                public static string MasseurId => TableName + "_masseur_id";
                public static string MotifIndisponibiliteId => TableName + "_motif_indisponibilite_id";
                public static string Debut => TableName + "_debut";
                public static string Fin => TableName + "_fin";
            }
        }
        public static class InscriptionIntervention {
            public static string TableName => "inscription_intervention";
            public static class Fields {
                public static string Id => TableName + "_id";
                public static string InterventionId => TableName + "_intervention_id";
                public static string MasseurId => TableName + "_masseur_id";
                public static string DateCreation => TableName + "_date_creation";
                public static string DateSatisfaction => TableName + "_date_satisfaction";
                public static string SatisfactionId => TableName + "_satisfaction_id";
                public static string DateDesistement => TableName + "_date_desistement";
                public static string MotifDesistementInterventionId => TableName + "_motif_desistement_intervention_id";
            }
        }
        public static class InscriptionSession {
            public static string TableName => "inscription_session";
            public static class Fields {
                public static string Id => TableName + "_id";
                public static string SessionFormationId => TableName + "_session_formation_id";
                public static string MasseurId => TableName + "_masseur_id";
                public static string DateCreation => TableName + "_date_creation";
                public static string FormationValidee => TableName + "_formation_validee";
                public static string DateSatisfaction => TableName + "_date_satisfaction";
                public static string SatisfactionId => TableName + "_satisfaction_id";
                public static string DateDesistement => TableName + "_date_desistement";
                public static string MotifDesistementSessionId => TableName + "_motif_desistement_session_id";
            }
        }
        public static class Intervention {
            public static string TableName => "intervention";
            public static class Fields {
                public static string Id => TableName + "_id";
                public static string PartenaireId => TableName + "_partenaire_id";
                public static string TypeInterventionId => TableName + "_type_intervention_id";
                public static string NomLieu => TableName + "_nom_lieu";
                public static string AdresseLieu => TableName + "_adresse_lieu";
                public static string CpVilleId => TableName + "_cp_ville_id";
                public static string TelephoneLieu => TableName + "_telephone_lieu";
                public static string MailLieu => TableName + "_mail_lieu";
                public static string DateCreation => TableName + "_date_creation";
                public static string CoutTotalPrestation => TableName + "_cout_total_prestation";
                public static string ValideBool => TableName + "_valide_bool";
                public static string ValideDate => TableName + "_valide_date";
                public static string DateSatisfaction => TableName + "_date_satisfaction";
                public static string SatisfactionId => TableName + "_satisfaction_id";
                public static string DateAnnulation => TableName + "_date_annulation";
                public static string MotifAnnulationInterventionId => TableName + "_motif_annulation_intervention_id";
                public static string DatePaiement => TableName + "_date_paiement";
                public static string MontantFacture => TableName + "_montant_facture";
            }
        }
        public static class JourSemaine {
            public static string TableName => "jour_semaine";
            public static class Fields {
                public static string Id => TableName + "_id";
                public static string Ordre => TableName + "_ordre";
                public static string MysqlValue => TableName + "_mysql_value";
                public static string CsharpValue => TableName + "_csharp_value";
                public static string Libelle => TableName + "_libelle";
            }
        }
        public static class MassageIntervention {
            public static string TableName => "massage_intervention";
            public static class Fields {
                public static string Id => TableName + "_id";
                public static string InterventionId => TableName + "_intervention_id";
                public static string TypeMassageId => TableName + "_type_massage_id";
                public static string NombreMasseurs => TableName + "_nombre_masseurs";
            }
        }
        public static class Masseur {
            public static string TableName => "masseur";
            public static class Fields {
                public static string Id => TableName + "_id";
                public static string Nom => TableName + "_nom";
                public static string Prenom => TableName + "_prenom";
                public static string GenreId => TableName + "_genre_id";
                public static string Telephone => TableName + "_telephone";
                public static string Email => TableName + "_email";
                public static string AdressePostale => TableName + "_adresse_postale";
                public static string CpVilleId => TableName + "_cp_ville_id";
                public static string DateNaissance => TableName + "_date_naissance";
                public static string DistanceMaxKm => TableName + "_distance_max_km";
                public static string DateCreation => TableName + "_date_creation";
            }
        }
        public static class MaterielIntervention {
            public static string TableName => "materiel_intervention";
            public static class Fields {
                public static string Id => TableName + "_id";
                public static string InterventionId => TableName + "_intervention_id";
                public static string TypeMaterielId => TableName + "_type_materiel_id";
                public static string Quantite => TableName + "_quantite";
            }
        }
        public static class MaterielMassage {
            public static string TableName => "materiel_massage";
            public static class Fields {
                public static string Id => TableName + "_id";
                public static string TypeMassageId => TableName + "_type_massage_id";
                public static string TypeMaterielId => TableName + "_type_materiel_id";
                public static string Quantite => TableName + "_quantite";
            }
        }
        public static class MaterielMasseur {
            public static string TableName => "materiel_masseur";
            public static class Fields {
                public static string Id => TableName + "_id";
                public static string TypeMaterielId => TableName + "_type_materiel_id";
                public static string MasseurId => TableName + "_masseur_id";
                public static string Quantite => TableName + "_quantite";
            }
        }
        public static class MaterielNomads {
            public static string TableName => "materiel_nomads";
            public static class Fields {
                public static string Id => TableName + "_id";
                public static string TypeMaterielId => TableName + "_type_materiel_id";
                public static string Ajout => TableName + "_ajout";
            }
        }
        public static class MotifAnnulationAction {
            public static string TableName => "motif_annulation_action";
            public static class Fields {
                public static string Id => TableName + "_id";
                public static string Libelle => TableName + "_libelle";
            }
        }
        public static class MotifAnnulationEchelleRemboursement {
            public static string TableName => "motif_annulation_echelle_remboursement";
            public static class Fields {
                public static string Id => TableName + "_id";
                public static string Libelle => TableName + "_libelle";
            }
        }
        public static class MotifAnnulationFormation {
            public static string TableName => "motif_annulation_formation";
            public static class Fields {
                public static string Id => TableName + "_id";
                public static string Libelle => TableName + "_libelle";
            }
        }
        public static class MotifAnnulationIntervention {
            public static string TableName => "motif_annulation_intervention";
            public static class Fields {
                public static string Id => TableName + "_id";
                public static string Libelle => TableName + "_libelle";
            }
        }
        public static class MotifAnnulationSession {
            public static string TableName => "motif_annulation_session";
            public static class Fields {
                public static string Id => TableName + "_id";
                public static string Libelle => TableName + "_libelle";
            }
        }
        public static class MotifAnnulationTournee {
            public static string TableName => "motif_annulation_tournee";
            public static class Fields {
                public static string Id => TableName + "_id";
                public static string Libelle => TableName + "_libelle";
            }
        }
        public static class MotifDepartMasseur {
            public static string TableName => "motif_depart_masseur";
            public static class Fields {
                public static string Id => TableName + "_id";
                public static string Libelle => TableName + "_libelle";
            }
        }
        public static class MotifDepartPartenaire {
            public static string TableName => "motif_depart_partenaire";
            public static class Fields {
                public static string Id => TableName + "_id";
                public static string Libelle => TableName + "_libelle";
            }
        }
        public static class MotifDesistementAction {
            public static string TableName => "motif_desistement_action";
            public static class Fields {
                public static string Id => TableName + "_id";
                public static string Libelle => TableName + "_libelle";
            }
        }
        public static class MotifDesistementFormateur {
            public static string TableName => "motif_desistement_formateur";
            public static class Fields {
                public static string Id => TableName + "_id";
                public static string Libelle => TableName + "_libelle";
            }
        }
        public static class MotifDesistementIntervention {
            public static string TableName => "motif_desistement_intervention";
            public static class Fields {
                public static string Id => TableName + "_id";
                public static string Libelle => TableName + "_libelle";
            }
        }
        public static class MotifDesistementSession {
            public static string TableName => "motif_desistement_session";
            public static class Fields {
                public static string Id => TableName + "_id";
                public static string Libelle => TableName + "_libelle";
            }
        }
        public static class MotifIndisponibilite {
            public static string TableName => "motif_indisponibilite";
            public static class Fields {
                public static string Id => TableName + "_id";
                public static string Libelle => TableName + "_libelle";
            }
        }
        public static class PackMads {
            public static string TableName => "pack_mads";
            public static class Fields {
                public static string Id => TableName + "_id";
                public static string QuantiteMads => TableName + "_quantite_mads";
                public static string PrixUnitaire => TableName + "_prix_unitaire";
            }
        }
        public static class Partenaire {
            public static string TableName => "partenaire";
            public static class Fields {
                public static string Id => TableName + "_id";
                public static string Nom => TableName + "_nom";
                public static string Telephone => TableName + "_telephone";
                public static string Mail => TableName + "_mail";
                public static string AdressePostale => TableName + "_adresse_postale";
                public static string CpVilleId => TableName + "_cp_ville_id";
                public static string NomReferent => TableName + "_nom_referent";
                public static string PrenomReferent => TableName + "_prenom_referent";
                public static string DateCreation => TableName + "_date_creation";
            }
        }
        public static class PreferenceMasseur {
            public static string TableName => "preference_masseur";
            public static class Fields {
                public static string Id => TableName + "_id";
                public static string MasseurId => TableName + "_masseur_id";
                public static string JourSemaineId => TableName + "_jour_semaine_id";
                public static string HeureDebut => TableName + "_heure_debut";
                public static string Duree => TableName + "_duree";
                public static string DateCreation => TableName + "_date_creation";
                public static string DateFin => TableName + "_date_fin";
            }
        }
        public static class Satisfaction {
            public static string TableName => "satisfaction";
            public static class Fields {
                public static string Id => TableName + "_id";
                public static string Ordre => TableName + "_ordre";
                public static string Libelle => TableName + "_libelle";
            }
        }
        public static class SessionFormation {
            public static string TableName => "session_formation";
            public static class Fields {
                public static string Id => TableName + "_id";
                public static string FormationId => TableName + "_formation_id";
                public static string Debut => TableName + "_debut";
                public static string Fin => TableName + "_fin";
                public static string FormateurMasseurId => TableName + "_formateur_masseur_id";
                public static string AffectationFormateurDate => TableName + "_affectation_formateur_date";
                public static string DateDesistementFormateur => TableName + "_date_desistement_formateur";
                public static string MotifDesistementFormateurId => TableName + "_motif_desistement_formateur_id";
                public static string DateAnnulation => TableName + "_date_annulation";
                public static string MotifAnnulationSessionId => TableName + "_motif_annulation_session_id";
            }
        }
        public static class Tournee {
            public static string TableName => "tournee";
            public static class Fields {
                public static string Id => TableName + "_id";
                public static string PartenaireId => TableName + "_partenaire_id";
                public static string JourSemaineId => TableName + "_jour_semaine_id";
                public static string HeureDebut => TableName + "_heure_debut";
                public static string Duree => TableName + "_duree";
                public static string MinBinomes => TableName + "_min_binomes";
                public static string MaxBinomes => TableName + "_max_binomes";
                public static string CoutMads => TableName + "_cout_mads";
                public static string DateFin => TableName + "_date_fin";
                public static string DateCreation => TableName + "_date_creation";
                public static string DateAnnulation => TableName + "_date_annulation";
                public static string MotifAnnulationTourneeId => TableName + "_motif_annulation_tournee_id";
            }
        }
        public static class TransactionPackMads {
            public static string TableName => "transaction_pack_mads";
            public static class Fields {
                public static string Id => TableName + "_id";
                public static string PackMadsId => TableName + "_pack_mads_id";
                public static string MasseurId => TableName + "_masseur_id";
                public static string Quantite => TableName + "_quantite";
                public static string DateTransaction => TableName + "_date_transaction";
            }
        }
        public static class TypeIntervention {
            public static string TableName => "type_intervention";
            public static class Fields {
                public static string Id => TableName + "_id";
                public static string Libelle => TableName + "_libelle";
            }
        }
        public static class TypeMassage {
            public static string TableName => "type_massage";
            public static class Fields {
                public static string Id => TableName + "_id";
                public static string Libelle => TableName + "_libelle";
            }
        }
        public static class TypeMateriel {
            public static string TableName => "type_materiel";
            public static class Fields {
                public static string Id => TableName + "_id";
                public static string Libelle => TableName + "_libelle";
            }
        }
        public static class TypePartenaire {
            public static string TableName => "type_partenaire";
            public static class Fields {
                public static string Id => TableName + "_id";
                public static string Libelle => TableName + "_libelle";
            }
        }
    }
}
