﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities {
    public class EvolutionMaterielEntity {
        public int Id { get; set; }
        public int MaterielNomadsId { get; set; }
        public int EtatMaterielId { get; set; }
        public DateTime DateEvolution { get; set; }
        public object[] AllValuesExceptId => new object[] { MaterielNomadsId, EtatMaterielId, DateEvolution };
    }
}
