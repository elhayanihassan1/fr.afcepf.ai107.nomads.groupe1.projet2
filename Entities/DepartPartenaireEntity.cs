﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities {
    public class DepartPartenaireEntity {
        public int Id { get; set; }
        public int PartenaireId { get; set; }
        public int MotifDepartPartenaireId { get; set; }
        public DateTime DateDepart { get; set; }
        public DateTime? DateRetour { get; set; }
        public object[] AllValuesExceptId => new object[] { PartenaireId, MotifDepartPartenaireId, DateDepart, DateRetour };
    }
}
