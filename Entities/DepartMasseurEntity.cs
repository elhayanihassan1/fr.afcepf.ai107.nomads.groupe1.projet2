﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities {
    public class DepartMasseurEntity {
        public int Id { get; set; }
        public int MasseurId { get; set; }
        public int MotifDepartMasseurId { get; set; }
        public DateTime DateDepart { get; set; }
        public DateTime? DateRetour { get; set; }
        public object[] AllValuesExceptId => new object[] { MasseurId, MotifDepartMasseurId, DateDepart, DateRetour };
    }
}
