﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities {
    public class MaterielNomadsEntity {
        public int Id { get; set; }
        public int TypeMaterielId { get; set; }
        public DateTime Ajout { get; set; }
        public object[] AllValuesExceptId => new object[] { TypeMaterielId, Ajout };
    }
}
