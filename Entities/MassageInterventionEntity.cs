﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities {
    public class MassageInterventionEntity {
        public int Id { get; set; }
        public int InterventionId { get; set; }
        public int TypeMassageId { get; set; }
        public int NombreMasseurs { get; set; }
        public object[] AllValuesExceptId => new object[] { InterventionId, TypeMassageId, NombreMasseurs };
    }
}
