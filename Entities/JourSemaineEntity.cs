﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities {
    public class JourSemaineEntity {
        public int Id { get; set; }
        public int Ordre { get; set; }
        public int MysqlValue { get; set; }
        public int CsharpValue { get; set; }
        public string Libelle { get; set; }
        public object[] AllValuesExceptId => new object[] { Ordre, MysqlValue, CsharpValue, Libelle };
    }
}
