﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities {
    public class AffectationActionEntity {
        public int Id { get; set; }
        public int ActionId { get; set; }
        public int MasseurId { get; set; }
        public DateTime DateAffectation { get; set; }
        public float? GainsEuros { get; set; }
        public int? NombreMassages { get; set; }
        public DateTime? DateSatisfaction { get; set; }
        public int? SatisfactionId { get; set; }
        public DateTime? DateDesistement { get; set; }
        public int? MotifDesistementActionId { get; set; }
        public int? RemboursementMads { get; set; }
        public object[] AllValuesExceptId => new object[] { ActionId, MasseurId, DateAffectation, GainsEuros, NombreMassages, DateSatisfaction, SatisfactionId, DateDesistement, MotifDesistementActionId, RemboursementMads };
    }
}
