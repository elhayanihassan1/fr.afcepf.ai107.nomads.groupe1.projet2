﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities {
    public class EvolutionMasseurEntity {
        public int Id { get; set; }
        public int MasseurId { get; set; }
        public int CompetenceId { get; set; }
        public DateTime DateEvolution { get; set; }
        public object[] AllValuesExceptId => new object[] { MasseurId, CompetenceId, DateEvolution };
    }
}
