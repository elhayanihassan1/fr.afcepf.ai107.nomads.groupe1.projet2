﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities {
    public class TourneeEntity {
        public int Id { get; set; }
        public int PartenaireId { get; set; }
        public string Nom { get; set; }
        public int JourSemaineId { get; set; }
        public int TypeInterventionId { get; set; }
        public int CompetenceId { get; set; }
        public TimeSpan HeureDebut { get; set; }
        public TimeSpan Duree { get; set; }
        public int MinBinomes { get; set; }
        public int MaxBinomes { get; set; }
        public int CoutMads { get; set; }
        public DateTime? DateFin { get; set; }
        public DateTime DateCreation { get; set; }
        public DateTime? DateAnnulation { get; set; }
        public int? MotifAnnulationTourneeId { get; set; }
        public object[] AllValuesExceptId => new object[] { PartenaireId, Nom, JourSemaineId, TypeInterventionId, CompetenceId, HeureDebut, Duree, MinBinomes, MaxBinomes, CoutMads, DateFin, DateCreation, DateAnnulation, MotifAnnulationTourneeId };
    }
}
