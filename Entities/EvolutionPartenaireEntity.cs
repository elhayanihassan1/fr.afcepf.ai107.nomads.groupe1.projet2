﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities {
    public class EvolutionPartenaireEntity {
        public int Id { get; set; }
        public int PartenaireId { get; set; }
        public int TypePartenaireId { get; set; }
        public DateTime DateEvolution { get; set; }
        public object[] AllValuesExceptId => new object[] { PartenaireId, TypePartenaireId, DateEvolution };
    }
}
