﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities {
    public class EchelleRemboursementEntity {
        public int Id { get; set; }
        public int TourneeId { get; set; }
        public TimeSpan HeuresRestantes { get; set; }
        public int Pourcentage { get; set; }
        public DateTime DateCreation { get; set; }
        public DateTime? DateAnnulation { get; set; }
        public int? MotifAnnulationEchelleRemboursementId { get; set; }
        public object[] AllValuesExceptId => new object[] { TourneeId, HeuresRestantes, Pourcentage, DateCreation, DateAnnulation, MotifAnnulationEchelleRemboursementId };
    }
}
