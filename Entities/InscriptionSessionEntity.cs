﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities {
    public class InscriptionSessionEntity {
        public int Id { get; set; }
        public int SessionFormationId { get; set; }
        public int MasseurId { get; set; }
        public DateTime DateCreation { get; set; }
        public bool? FormationValidee { get; set; }
        public DateTime? DateSatisfaction { get; set; }
        public int? SatisfactionId { get; set; }
        public DateTime? DateDesistement { get; set; }
        public int? MotifDesistementSessionId { get; set; }
        public object[] AllValuesExceptId => new object[] { SessionFormationId, MasseurId, DateCreation, FormationValidee, DateSatisfaction, SatisfactionId, DateDesistement, MotifDesistementSessionId };
    }
}
