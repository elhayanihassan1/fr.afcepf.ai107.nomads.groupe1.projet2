﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities {
    public class PreferenceMasseurEntity {
        public int Id { get; set; }
        public int MasseurId { get; set; }
        public int JourSemaineId { get; set; }
        public TimeSpan HeureDebut { get; set; }
        public TimeSpan Duree { get; set; }
        public DateTime DateCreation { get; set; }
        public DateTime? DateFin { get; set; }
        public object[] AllValuesExceptId => new object[] { MasseurId, JourSemaineId, HeureDebut, Duree, DateCreation, DateFin };
    }
}
