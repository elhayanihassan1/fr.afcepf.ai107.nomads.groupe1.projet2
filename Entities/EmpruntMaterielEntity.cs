﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities {
    public class EmpruntMaterielEntity {
        public int Id { get; set; }
        public int MaterielNomadsId { get; set; }
        public int InscriptionInterventionId { get; set; }
        public DateTime DateEmprunt { get; set; }
        public DateTime DateRetour { get; set; }
        public object[] AllValuesExceptId => new object[] { MaterielNomadsId, InscriptionInterventionId, DateEmprunt, DateRetour };
    }
}
