﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities {
    public class SatisfactionEntity {
        public int Id { get; set; }
        public int Ordre { get; set; }
        public string Libelle { get; set; }
        public object[] AllValuesExceptId => new object[] { Ordre, Libelle };
    }
}
