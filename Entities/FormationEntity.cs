﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities {
    public class FormationEntity {
        public int Id { get; set; }
        public string Titre { get; set; }
        public string Description { get; set; }
        public int CompetenceId { get; set; }
        public TimeSpan Duree { get; set; }
        public string AdressePostale { get; set; }
        public int CpVilleId { get; set; }
        public int MinMasseurs { get; set; }
        public int MaxMasseurs { get; set; }
        public DateTime DateCreation { get; set; }
        public DateTime? DateAnnulation { get; set; }
        public int? LationFormationId { get; set; }
        public object[] AllValuesExceptId => new object[] { Titre, Description, CompetenceId, Duree, AdressePostale, CpVilleId, MinMasseurs, MaxMasseurs, DateCreation, DateAnnulation, LationFormationId };
    }
}
