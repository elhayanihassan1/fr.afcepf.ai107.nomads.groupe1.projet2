﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities {
    public class MasseurEntity {
        public int Id { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public int GenreId { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }
        public string AdressePostale { get; set; }
        public int CpVilleId { get; set; }
        public DateTime DateNaissance { get; set; }
        public int DistanceMaxKm { get; set; }
        public DateTime DateCreation { get; set; }
        public object[] AllValuesExceptId => new object[] { Nom, Prenom, GenreId, Telephone, Email, AdressePostale, CpVilleId, DateNaissance, DistanceMaxKm, DateCreation };
    }
}
