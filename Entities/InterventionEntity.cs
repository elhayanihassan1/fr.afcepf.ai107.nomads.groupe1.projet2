﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities {
    public class InterventionEntity {
        public int Id { get; set; }
        public int PartenaireId { get; set; }
        public int TypeInterventionId { get; set; }
        public string NomLieu { get; set; }
        public string AdresseLieu { get; set; }
        public int CpVilleId { get; set; }
        public int CompetenceId { get; set; }
        public string TelephoneLieu { get; set; }
        public string MailLieu { get; set; }
        public DateTime DateCreation { get; set; }
        public float? CoutTotalPrestation { get; set; }
        public bool? ValideBool { get; set; }
        public DateTime? ValideDate { get; set; }
        public DateTime? DateSatisfaction { get; set; }
        public int? SatisfactionId { get; set; }
        public DateTime? DateAnnulation { get; set; }
        public int? MotifAnnulationInterventionId { get; set; }
        public DateTime? DatePaiement { get; set; }
        public float? MontantFacture { get; set; }
 
        public object[] AllValuesExceptId => new object[] { PartenaireId, TypeInterventionId, NomLieu, AdresseLieu, CpVilleId, CompetenceId, TelephoneLieu, MailLieu, DateCreation, CoutTotalPrestation, ValideBool, ValideDate, DateSatisfaction, SatisfactionId, DateAnnulation, MotifAnnulationInterventionId, DatePaiement, MontantFacture };
    }
}
