﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities {
    public class PartenaireEntity {
        public int Id { get; set; }
        public string Nom { get; set; }
        public string Telephone { get; set; }
        public string Mail { get; set; }
        public string AdressePostale { get; set; }
        public int CpVilleId { get; set; }
        public string NomReferent { get; set; }
        public string PrenomReferent { get; set; }
        public DateTime DateCreation { get; set; }
        public object[] AllValuesExceptId => new object[] { Nom, Telephone, Mail, AdressePostale, CpVilleId, NomReferent, PrenomReferent, DateCreation };
    }
}
