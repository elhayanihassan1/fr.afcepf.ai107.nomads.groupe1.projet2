﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities {
    public class SessionFormationEntity {
        public int Id { get; set; }
        public int FormationId { get; set; }
        public DateTime Debut { get; set; }
        public DateTime Fin { get; set; }
        public int? FormateurMasseurId { get; set; }
        public DateTime? AffectationFormateurDate { get; set; }
        public DateTime? DateDesistementFormateur { get; set; }
        public int? MotifDesistementFormateurId { get; set; }
        public DateTime? DateAnnulation { get; set; }
        public int? MotifAnnulationSessionId { get; set; }
        public object[] AllValuesExceptId => new object[] { FormationId, Debut, Fin, FormateurMasseurId, AffectationFormateurDate, DateDesistementFormateur, MotifDesistementFormateurId, DateAnnulation, MotifAnnulationSessionId };
    }
}
