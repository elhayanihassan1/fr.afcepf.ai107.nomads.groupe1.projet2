﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities {
    public class CpVilleEntity {
        public int Id { get; set; }
        public string CodePostal { get; set; }
        public string Ville { get; set; }
        public object[] AllValuesExceptId => new object[] { CodePostal, Ville };
    }
}
