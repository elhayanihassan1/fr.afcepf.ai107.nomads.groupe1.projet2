﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities {
    public class PackMadsEntity {
        public int Id { get; set; }
        public int QuantiteMads { get; set; }
        public float PrixUnitaire { get; set; }
        public object[] AllValuesExceptId => new object[] { QuantiteMads, PrixUnitaire };
    }
}
