﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities {
    public class TransactionPackMadsEntity {
        public int Id { get; set; }
        public int PackMadsId { get; set; }
        public int MasseurId { get; set; }
        public int Quantite { get; set; }
        public DateTime DateTransaction { get; set; }
        public object[] AllValuesExceptId => new object[] { PackMadsId, MasseurId, Quantite, DateTransaction };
    }
}
