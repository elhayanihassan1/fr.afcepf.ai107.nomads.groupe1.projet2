﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities {
    public class ActionEntity {
        public int Id { get; set; }
        public int TourneeId { get; set; }
        public DateTime Debut { get; set; }
        public DateTime Fin { get; set; }
        public DateTime? DateSatisfaction { get; set; }
        public int? SatisfactionId { get; set; }
        public DateTime? DateAnnulation { get; set; }
        public int? MotifAnnulationActionId { get; set; }
        public object[] AllValuesExceptId => new object[] { TourneeId, Debut, Fin, DateSatisfaction, SatisfactionId, DateAnnulation, MotifAnnulationActionId };
    }
}
