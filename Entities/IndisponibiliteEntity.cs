﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FR.AFCEPF.AI107.Nomads.Projet2.Groupe1.Entities {
    public class IndisponibiliteEntity {
        public int Id { get; set; }
        public bool Utilisateur { get; set; }
        public int MasseurId { get; set; }
        public int? MotifIndisponibiliteId { get; set; }
        public DateTime Debut { get; set; }
        public DateTime? Fin { get; set; }
        public object[] AllValuesExceptId => new object[] { Utilisateur, MasseurId, MotifIndisponibiliteId, Debut, Fin };
    }
}
